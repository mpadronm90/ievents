'use strict';

/**
 * @ngdoc function
 * @name proyectoApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the proyectoApp
 */
angular.module('proyectoApp')
  .controller('MainCtrl', function ($scope) {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
    
    $scope.eventSources = [[{title: 'Concierto', start: new Date(2016, 4, 10), end: new Date(2016,4,12)}]];
    
    $scope.$emit('toolbar','Inicio');
  });
