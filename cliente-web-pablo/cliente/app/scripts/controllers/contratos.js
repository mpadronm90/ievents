/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


angular.module('proyectoApp')
  .controller('ContratoCtrl', function ($scope, $mdDialog) {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
    
    $scope.selected=[];
    
    $scope.$emit('toolbar','Contratos');
    
    $scope.crearContrato=function(ev){
        $mdDialog.show({
      controller: editContratoController,
      templateUrl: 'views/contratos/edit_contrato.html',
      parent: angular.element(document.body),
      targetEvent: ev,
      clickOutsideToClose:true,
      fullscreen: true,
      locals:{
          contrato:{}
      }
      
    })
    .then(function(answer) {
      $scope.status = 'You said the information was "' + answer + '".';
    }, function() {
      $scope.status = 'You cancelled the dialog.';
    });
    }
    
    $scope.showContrato=function(contrato, ev){
        $mdDialog.show({
      controller: contratoShowController,
      templateUrl: 'views/contratos/view_contratos.html',
      parent: angular.element(document.body),
      targetEvent: ev,
      clickOutsideToClose:true,
      fullscreen: true,
      locals:{
          contrato: contrato
      }
      
    })
    .then(function(answer) {
      $scope.status = 'You said the information was "' + answer + '".';
    }, function() {
      $scope.status = 'You cancelled the dialog.';
    });
    }
    
    $scope.editContrato=function(contrato, ev){
        $mdDialog.show({
      controller: editContratoController,
      templateUrl: 'views/contratos/edit_contrato.html',
      parent: angular.element(document.body),
      targetEvent: ev,
      clickOutsideToClose:true,
      fullscreen: true,
      locals:{
          contrato: contrato
      }
      
    })
    .then(function(answer) {
      $scope.status = 'You said the information was "' + answer + '".';
    }, function() {
      $scope.status = 'You cancelled the dialog.';
    });
    }
    
    $scope.borrarContrato = function(idContrato,ev) {
    // Appending dialog to document.body to cover sidenav in docs app
    var confirm = $mdDialog.confirm()
          .title('Borrar Contrato')
          .textContent('¿Te gustaría borrar el contrato seleccionado?.')
          .ariaLabel('Lucky day')
          .targetEvent(ev)
          .ok('Si')
          .cancel('No');
    $mdDialog.show(confirm).then(function() {
      $scope.status = 'You decided to get rid of your debt.';
    }, function() {
      $scope.status = 'You decided to keep your debt.';
    });
  };
    
    $scope.contratos=[{
        id: 1,
        nombre: 'Prueba1',
        cliente: 'prueba',
        contrato: {id: 1, nombre: 'contrato1'},
        fechaCreacion: new Date(),
        fechaInicio: new Date(),
        estado: 'estado1',
        direccion: 'prueba',
        precio: 14,
        aforo: 'prueba',
        fechaFinal: 100
        
    },{
        id: 2,
        nombre: 'hola',
        emplazamiento: 'prueba',
        aforo: 'prueba',
        fechaInicial: 'prueba',
        fechaFinal: 'prueba',
        cliente: 'prueba'
    }];
  });
