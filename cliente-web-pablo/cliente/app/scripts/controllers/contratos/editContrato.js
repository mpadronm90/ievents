/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


function editContratoController($scope, $mdDialog, contrato){
    
   $scope.contrato=contrato;
   
   $scope.servicios=[
       {nombre: "servicio1", descripcion: "Esto es una prueba de servicio"},  
       {nombre: "servicio2", descripcion: "Esto es otra prueba de servicio"}
   ];
   
   $scope.patrocinadores=[
       {nombre: "patrocionador1", contacto: "a@a.es"},  
       {nombre: "patrocionador2", contacto: "b@b.es"}
   ];
       
   
   $scope.hide = function() {
    $mdDialog.hide();
  };
  $scope.cancel = function() {
    $mdDialog.cancel();
  };
  $scope.answer = function(answer) {
    $mdDialog.hide(answer);
  };
}
