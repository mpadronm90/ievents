'use strict';

/**
 * @ngdoc function
 * @name proyectoApp.controller:EventoCtrl
 * @description
 * # EventoCtrl
 * Controller of the proyectoApp
 */
angular.module('proyectoApp')
  .controller('EventoCtrl', function ($scope, $mdDialog) {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
    
    $scope.selected=[];
    
    $scope.$emit('toolbar','Eventos');
    
    $scope.crearEvento=function(ev){
        $mdDialog.show({
      controller: editEventController,
      templateUrl: 'views/evento/edit_event.html',
      parent: angular.element(document.body),
      targetEvent: ev,
      clickOutsideToClose:true,
      fullscreen: true,
      locals:{
          evento:{}
      }
      
    })
    .then(function(answer) {
      $scope.status = 'You said the information was "' + answer + '".';
    }, function() {
      $scope.status = 'You cancelled the dialog.';
    });
    }
    
    $scope.showEvento=function(evento, ev){
        $mdDialog.show({
      controller: eventShowController,
      templateUrl: 'views/evento/view_eventos.html',
      parent: angular.element(document.body),
      targetEvent: ev,
      clickOutsideToClose:true,
      fullscreen: true,
      locals:{
          evento: evento
      }
      
    })
    .then(function(answer) {
      $scope.status = 'You said the information was "' + answer + '".';
    }, function() {
      $scope.status = 'You cancelled the dialog.';
    });
    }
    
    $scope.editEvent=function(evento, ev){
        $mdDialog.show({
      controller: editEventController,
      templateUrl: 'views/evento/edit_event.html',
      parent: angular.element(document.body),
      targetEvent: ev,
      clickOutsideToClose:true,
      fullscreen: true,
      locals:{
          evento: evento
      }
      
    })
    .then(function(answer) {
      $scope.status = 'You said the information was "' + answer + '".';
    }, function() {
      $scope.status = 'You cancelled the dialog.';
    });
    }
    
    $scope.borrarEvento = function(idEvento,ev) {
    // Appending dialog to document.body to cover sidenav in docs app
    var confirm = $mdDialog.confirm()
          .title('Borrar Evento')
          .textContent('¿Te gustaría borrar el evento seleccionado?.')
          .ariaLabel('Lucky day')
          .targetEvent(ev)
          .ok('Si')
          .cancel('No');
    $mdDialog.show(confirm).then(function() {
      $scope.status = 'You decided to get rid of your debt.';
    }, function() {
      $scope.status = 'You decided to keep your debt.';
    });
  };
    
    $scope.eventos=[{
        id: 1,
        nombre: 'Prueba1',
        cliente: 'prueba',
        contrato: {id: 1, nombre: 'contrato1'},
        fechaCreacion: new Date(),
        fechaInicio: new Date(),
        estado: 'estado1',
        direccion: 'prueba',
        precio: 14,
        aforo: 'prueba',
        fechaFinal: 100
        
    },{
        id: 2,
        nombre: 'hola',
        emplazamiento: 'prueba',
        aforo: 'prueba',
        fechaInicial: 'prueba',
        fechaFinal: 'prueba',
        cliente: 'prueba'
    }];
  });
