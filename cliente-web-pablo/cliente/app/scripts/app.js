'use strict';

/**
 * @ngdoc overview
 * @name proyectoApp
 * @description
 * # proyectoApp
 *
 * Main module of the application.
 */
angular
  .module('proyectoApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngMaterial',
    'ui.calendar',
    'md.data.table'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
      .when('/eventos', {
        templateUrl: 'views/evento/eventos.html',
        controller: 'EventoCtrl'
      }).when('/contratos', {
        templateUrl: 'views/contratos/contratos.html',
        controller: 'ContratoCtrl'
      })
     .when('/ventaEntradas', {
        templateUrl: 'views/ventaEntradas.html',
        controller: 'VentaEntradasCtrl'
        
      })
      .otherwise({
        redirectTo: '/'
      });
  }).config(function($mdThemingProvider) {
  $mdThemingProvider.theme('default')
    .primaryPalette('blue-grey')
    .accentPalette('blue');
}).filter("capitalize", function(){
    return function(text) {
        if(text != null){
            return text.substring(0,1).toUpperCase()+text.substring(1);
        }
    }
});
