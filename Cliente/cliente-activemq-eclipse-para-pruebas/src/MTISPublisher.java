import java.util.concurrent.CountDownLatch;

import javax.jms.*;

import org.apache.activemq.ActiveMQConnectionFactory;

public class MTISPublisher implements MessageListener {

	public static void main(String []args) throws JMSException  {
		
		try {
			String url = "tcp://localhost:61616";
		    String subject = "ConsultaCliente";
		    CountDownLatch latch = new CountDownLatch(1);
		    
	        ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(url);
	        Connection connection = connectionFactory.createConnection("admin","admin");
	        connection.start();
	        
	        Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
	
	        Destination destination = session.createTopic(subject);
	
	        MessageProducer producer = session.createProducer(destination);
	
	        TextMessage message = session.createTextMessage("{\"idCliente\":\"0\", \"idContrato\":\"1\" }");
	
	        producer.send(message);
	        System.out.println("Enviado: '" + message.getText() + "'");
	        
	        /** CONSUMIDOR PARA RECIBIR MENSAJES **/
	        
	        String subject_respuesta = "RespuestaConsulta";
	        
	        Destination destination_respuesta = session.createTopic(subject_respuesta);
	
	        MessageConsumer consumer = session.createConsumer(destination_respuesta);
	
	        consumer.setMessageListener(new MTISPublisher());
	        latch.await();
	        consumer.close();
	        connection.close();	        
		}
		catch(Exception e) { }
	}
	
    @Override
    public void onMessage(Message message) {
        try {	     	        
            if (message instanceof TextMessage) {
	            TextMessage textMessage = (TextMessage) message;
	            System.out.println("Recibido: '" + textMessage.getText() + "'");
	        }
        } catch (JMSException e) {
            System.out.println("Got a JMS Exception!");
        }
    }
}
