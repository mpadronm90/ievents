﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Apache.NMS;
using Apache.NMS.Util;
using Apache.NMS.ActiveMQ.Commands;
using System.Threading;
using Newtonsoft.Json;

namespace ClientePatrocinador
{
    class Program
    {
        protected static AutoResetEvent semaphore = new AutoResetEvent(false);
        protected static ITextMessage message = null;
        protected static TimeSpan receiveTimeout = TimeSpan.FromSeconds(10);

        public static void Main(string[] args)
        {
            Console.WriteLine("Introduce tu id de patrocinador:");
            int patrocinador = int.Parse(Console.ReadLine());

            Uri connecturi = new Uri("tcp://localhost:61616");

            Console.WriteLine("Conectando a " + connecturi);

            // NOTE: ensure the nmsprovider-activemq.config file exists in the executable folder.
            IConnectionFactory factory = new NMSConnectionFactory(connecturi);

            using (IConnection connection = factory.CreateConnection("admin", "admin"))
            using (ISession session = connection.CreateSession())
            {

                IDestination destination = session.GetTopic("EnviarPropuesta");
                IDestination contestacion = session.GetTopic("ResponderPropuesta");

                Console.WriteLine("Usando destino: " + destination);

                // Create a consumer and producer
                using (IMessageConsumer consumer = session.CreateConsumer(destination))
                using (IMessageProducer producer = session.CreateProducer(contestacion))
                {
                    // Start the connection so that messages will be processed.
                    connection.Start();
                    producer.DeliveryMode = MsgDeliveryMode.Persistent;
                    producer.RequestTimeout = receiveTimeout;

                    consumer.Listener += new MessageListener(OnMessage);

                    int i = 1; // Para que no se rompa el bucle
                    do
                    {

                        // Wait for the message
                        semaphore.WaitOne((int)receiveTimeout.TotalMilliseconds, true);

                        if (message != null)
                        {
                            //Console.WriteLine("Mensaje recibido con contenido: " + message.Text);
                            // Parsear JSON
                            dynamic data = JsonConvert.DeserializeObject(message.Text);
                            int idEvento = data.idEvento;
                            int idContrato = data.idContrato;
                            int idPatrocinador = data.idPatrocinador;

                            // Notificar de los cambios si están dirigidos al cliente
                            if (idPatrocinador == patrocinador)
                            {
                                /*** RESPUESTA EN FORMATO JSON ***/

                                //WebReference.EventoSkeleton consumidor = new WebReference.EventoSkeleton();
                                //WebReference.DatosEvento datosEvento = new WebReference.DatosEvento();
                                //datosEvento.idEvento = idEvento;
                                //datosEvento.idEventoSpecified = true;
                                //WebReference.DatosEventoResponse respuesta = consumidor.datosEvento(datosEvento);

                                Console.WriteLine("Propuesta de patrocinio recibida: idPatrocinio:" + idContrato.ToString());
                                //Console.WriteLine("Descripción: " + respuesta.@out.descripcion);
                                //Console.WriteLine("Precio de la entrada: " + respuesta.@out.precioEntrada.ToString() + " euros");
                                //Console.WriteLine("Inicio del evento: " + respuesta.@out.fechaInicio.ToString());
                                //Console.WriteLine("Fin del evento: " + respuesta.@out.fechaFin.ToString());
                                Console.WriteLine("¿Deseas aceptar la propuesta?");
                                Console.WriteLine("1-Sí / Otra cosa-No");

                                int aceptado;
                                try
                                {
                                    aceptado = int.Parse(Console.ReadLine());
                                }
                                catch (Exception ex)
                                {
                                    aceptado = 0;
                                }

                                String peticionJson = "";

                                if (aceptado == 1)
                                {
                                    peticionJson = "{ \"idEvento\":\"" + idEvento.ToString() + "\", \"idPatrocinador\":\"" + idPatrocinador.ToString() + "\", \"idContrato\":\"" + idContrato.ToString() + "\", \"aceptado\":\"true\" }";
                                }
                                else
                                {
                                    peticionJson = "{ \"idEvento\":\"" + idEvento.ToString() + "\", \"idPatrocinador\":\"" + idPatrocinador.ToString() + "\", \"idContrato\":\"" + idContrato.ToString() + "\", \"aceptado\":\"false\" }";
                                }

                                producer.Send(peticionJson);
                            }

                        }
                    } while (i != 0);
                }

            }
        }

        protected static void OnMessage(IMessage receivedMsg)
        {
            message = receivedMsg as ITextMessage;
            semaphore.Set();
        }
    }
}
