
/**
 * UtilitySkeleton.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.3  Built on : Jun 27, 2015 (11:17:49 BST)
 */
package iEvents.skeleton;

import java.sql.Date;
import java.sql.DriverManager;
import java.sql.ResultSet;

import iEvents.servicios.*;

/**
 * UtilitySkeleton java skeleton for the axisService
 */
public class UtilitySkeleton {

	/**
	 * Auto generated method signature
	 * 
	 * @param listarNombreEmplazamientosPosibles
	 * @return listarNombreEmplazamientosPosiblesResponse
	 */

	public ListarNombreEmplazamientosPosiblesResponse listarNombreEmplazamientosPosibles(
			ListarNombreEmplazamientosPosibles listarNombreEmplazamientosPosibles) {
		ListarNombreEmplazamientosPosiblesResponse devolver = new ListarNombreEmplazamientosPosiblesResponse();
		try {
			// Conectamos a la bd
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			java.sql.Connection con = DriverManager.getConnection("jdbc:mysql://localhost/ievents", "root", "");

			java.sql.Statement st = con.createStatement();

			// Cogemos las fechas del contrato pasado
			String sql = "SELECT fechaInicioEvento, fechaFinEvento, aforo FROM contrato WHERE idContrato = "
					+ listarNombreEmplazamientosPosibles.getIdContrato();

			ResultSet rs = st.executeQuery(sql);

			if (rs.next()) {
				Date fechaInicioEvento = rs.getDate("fechaInicioEvento");
				Date fechaFinEvento = rs.getDate("fechaFinEvento");
				int aforo = rs.getInt("aforo");
				sql = "SELECT nombre FROM emplazamiento WHERE idEmplazamiento IN" + " (SELECT idEmplazamiento"
						+ " FROM emplazamiento" + " WHERE idEmplazamiento NOT IN"
						+ " (SELECT Emplazamiento_idEmplazamiento" + " FROM contrato c"
						+ " WHERE Emplazamiento_idEmplazamiento IS NOT NULL " + "AND (c.fechaInicioEvento <= \""
						+ fechaInicioEvento + "\" " + "OR c.fechaInicioEvento >= \"" + fechaFinEvento + "\""
						+ ") AND (c.fechaFinEvento >= \"" + fechaFinEvento + "\" " + "OR c.fechaFinEvento <= \""
						+ fechaFinEvento + "\"))" + " AND Aforo >= " + aforo + ")";

				ResultSet rt = st.executeQuery(sql);

				while (rt.next()) {
					devolver.addResultado(rt.getString("nombre"));
				}
			}

			con.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return devolver;
	}

	/**
	 * Auto generated method signature
	 * 
	 * @param listarNombrePatrocinadoresPosibles
	 * @return listarNombrePatrocinadoresPosiblesResponse
	 */

	public ListarNombrePatrocinadoresPosiblesResponse listarNombrePatrocinadoresPosibles(
			ListarNombrePatrocinadoresPosibles listarNombrePatrocinadoresPosibles) {
		ListarNombrePatrocinadoresPosiblesResponse devolver = new ListarNombrePatrocinadoresPosiblesResponse();
		try {
			// Conectamos a la bd
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			java.sql.Connection con = DriverManager.getConnection("jdbc:mysql://localhost/ievents", "root", "");

			java.sql.Statement st = con.createStatement();
			String sql = "SELECT nombre FROM patrocinador";

			ResultSet rs = st.executeQuery(sql);
			while (rs.next()) {
				devolver.addResultado(rs.getString("nombre"));
			}
			con.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return devolver;
	}

	/**
	 * Auto generated method signature
	 * 
	 * @param listarServiciosContrato
	 * @return listarServiciosContratoResponse
	 */

	public ListarServiciosContratoResponse listarServiciosContrato(ListarServiciosContrato listarServiciosContrato) {
		ListarServiciosContratoResponse devolver = new ListarServiciosContratoResponse();
		try {
			// Conectamos a la bd
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			java.sql.Connection con = DriverManager.getConnection("jdbc:mysql://localhost/ievents", "root", "");

			java.sql.Statement st = con.createStatement();
			String sql = "SELECT idServicio, nombre, descripcion " + "FROM servicio "
					+ "INNER JOIN servicio_has_contrato s ON servicio.idServicio=s.Servicio_idServicio "
					+ "AND s.Contrato_idContrato = " + listarServiciosContrato.getIdContrato();

			ResultSet rs = st.executeQuery(sql);
			while (rs.next()) {
				Servicio serv = new Servicio();
				serv.setIdServicio(rs.getInt("idServicio"));
				serv.setNombre(rs.getString("nombre"));
				serv.setDescripcion(rs.getString("descripcion"));
				devolver.addResultado(serv);
			}
			con.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return devolver;
	}

	/**
	 * Auto generated method signature
	 * 
	 * @param listarNombreServiciosPosibles
	 * @return listarNombreServiciosPosiblesResponse
	 */

	public ListarNombreServiciosPosiblesResponse listarNombreServiciosPosibles(
			ListarNombreServiciosPosibles listarNombreServiciosPosibles) {
		ListarNombreServiciosPosiblesResponse devolver = new ListarNombreServiciosPosiblesResponse();
		try {
			// Conectamos a la bd
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			java.sql.Connection con = DriverManager.getConnection("jdbc:mysql://localhost/ievents", "root", "");

			java.sql.Statement st = con.createStatement();

			String sql = "SELECT s.idServicio, s.descripcion, s.nombre " + "FROM servicio s, contrato v "
					+ "WHERE v.idContrato = " + listarNombreServiciosPosibles.getIdContrato()
					+ " AND s.idServicio NOT IN " + "(SELECT Servicio_idServicio " + "FROM servicio_has_contrato co "
					+ "INNER JOIN contrato c ON co.Contrato_idContrato = c.idContrato "
					+ "WHERE (c.fechaInicioEvento <= v.fechaInicioEvento AND c.fechaFinEvento >= v.fechaFinEvento) OR "
					+ "(c.fechaInicioEvento <= v.fechaInicioEvento AND c.fechaInicioEvento >= v.fechaFinEvento AND c.fechaFinEvento <= v.fechaFinEvento) OR "
					+ "(c.fechaInicioEvento >= v.fechaInicioEvento AND c.fechaFinEvento <= v.fechaFinEvento) OR "
					+ "(c.fechaInicioEvento >= v.fechaInicioEvento AND c.fechaFinEvento <= v.fechaInicioEvento AND c.fechaFinEvento >= v.fechaFinEvento))";
			ResultSet rs = st.executeQuery(sql);
			while (rs.next()) {
				devolver.addResultados(rs.getString("nombre")+" - "+rs.getString("descripcion"));
			}
			con.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return devolver;
	}

	/**
	 * Auto generated method signature
	 * 
	 * @param datosCliente
	 * @return datosClienteResponse
	 */

	public DatosClienteResponse datosCliente(DatosCliente datosCliente) {
		DatosClienteResponse devolver = new DatosClienteResponse();

		int idCliente = datosCliente.getIdCliente();

		try {
			// Conectamos a la bd
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			java.sql.Connection con = DriverManager.getConnection("jdbc:mysql://localhost/ievents", "root", "");

			java.sql.Statement st = con.createStatement();

			// Buscamos los servicios que hay que contratar para el evento
			String sql = "SELECT idCliente, nombre, apellido1, apellido2, email, dni FROM cliente WHERE idCliente = "
					+ idCliente;

			ResultSet rs = st.executeQuery(sql);

			if (rs.next()) { // Para cada servicio
				// Recogemos los valores de la db
				Cliente cli = new Cliente();
				cli.setId(rs.getInt("idCliente"));
				cli.setNombre(rs.getString("nombre"));
				cli.setApellido1(rs.getString("apellido1"));
				cli.setApellido2(rs.getString("apellido2"));
				cli.setEmail(rs.getString("email"));
				cli.setDni(rs.getString("dni"));
				// A�adimos el patrocinado al array
				devolver.setRespuesta(cli);
			}

			con.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return devolver;
	}

	/**
	 * Auto generated method signature
	 * 
	 * @return listarContratosResponse
	 */

	public ListarContratosResponse listarContratos() {
		ListarContratosResponse devolver = new ListarContratosResponse();

		try {
			// Conectamos a la bd
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			java.sql.Connection con = DriverManager.getConnection("jdbc:mysql://localhost/ievents", "root", "");

			java.sql.Statement st = con.createStatement();

			// Buscamos los servicios que hay que contratar para el evento
			String sql = "SELECT idContrato, contrato.descripcion, fechaCreacionContrato, fechaInicioEvento, fechaFinEvento, contrato.aforo, urlImagen, "
					+ "emplazamiento.nombre as emplazamiento, " + "estadocontrato.nombre as estado, "
					+ "cliente.nombre, cliente.apellido1, cliente.apellido2, cliente.email  "
					+ "FROM contrato INNER JOIN cliente ON contrato.Cliente_idCliente = cliente.idCliente "
					+ "LEFT JOIN emplazamiento ON contrato.Emplazamiento_idEmplazamiento = emplazamiento.idEmplazamiento "
					+ "INNER JOIN estadocontrato ON contrato.EstadoContrato_idEstadoContrato = estadocontrato.idEstadoContrato";

			ResultSet rs = st.executeQuery(sql);

			while (rs.next()) { // Para cada servicio
				// Recogemos los valores de la db
				Contrato contrato = new Contrato();
				contrato.setIdContrato(rs.getInt("idContrato"));
				contrato.setDescripcion(rs.getString("descripcion"));
				if(rs.getString("emplazamiento") != null){
					contrato.setDireccEmpl(rs.getString("emplazamiento"));
				}
				else{
					contrato.setDireccEmpl("Sin emplazamiento");
				}
				contrato.setAforo(rs.getInt("aforo"));
				contrato.setFechaIni(rs.getDate("fechaInicioEvento"));
				contrato.setFechaFin(rs.getDate("fechaFinEvento"));
				contrato.setNombreCliente(
						rs.getString("nombre") + " " + rs.getString("apellido1") + " "+ rs.getString("apellido2"));
				contrato.setEmailCliente(rs.getString("email"));
				contrato.setEstado(rs.getString("estado"));
				contrato.setFechaCreacion(rs.getDate("fechaCreacionContrato"));
				contrato.setUrlImagen(rs.getString("urlImagen"));
				// A�adimos el patrocinado al array
				devolver.addRespuesta(contrato);
			}

			con.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return devolver;
	}

	/**
	 * Auto generated method signature
	 * 
	 * @return listarNombreEstadosContratoResponse
	 */

	public ListarNombreEstadosContratoResponse listarNombreEstadosContrato() {
		ListarNombreEstadosContratoResponse devolver = new ListarNombreEstadosContratoResponse();

		try {
			// Conectamos a la bd
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			java.sql.Connection con = DriverManager.getConnection("jdbc:mysql://localhost/ievents", "root", "");

			java.sql.Statement st = con.createStatement();

			// Buscamos los servicios que hay que contratar para el evento
			String sql = "SELECT nombre FROM estadocontrato";

			ResultSet rs = st.executeQuery(sql);

			while (rs.next()) { // Para cada servicio
				devolver.addResultado(rs.getString("nombre"));
			}

			con.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return devolver;
	}

	/**
	 * Auto generated method signature
	 * 
	 * @param datosContrato
	 * @return datosContratoResponse
	 */

	public DatosContratoResponse datosContrato(DatosContrato datosContrato) {
		DatosContratoResponse devolver = new DatosContratoResponse();

		try {
			// Conectamos a la bd
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			java.sql.Connection con = DriverManager.getConnection("jdbc:mysql://localhost/ievents", "root", "");

			java.sql.Statement st = con.createStatement();

			// Buscamos los servicios que hay que contratar para el evento
			String sql = "SELECT idContrato, contrato.descripcion, fechaCreacionContrato, fechaInicioEvento, fechaFinEvento, contrato.aforo, urlImagen, "
					+ "emplazamiento.nombre as emplazamiento, " + "estadocontrato.nombre as estado, "
					+ "cliente.nombre, cliente.apellido1, cliente.apellido2, cliente.email  "
					+ "FROM contrato INNER JOIN cliente ON contrato.Cliente_idCliente = cliente.idCliente "
					+ "LEFT JOIN emplazamiento ON contrato.Emplazamiento_idEmplazamiento = emplazamiento.idEmplazamiento "
					+ "INNER JOIN estadocontrato ON contrato.EstadoContrato_idEstadoContrato = estadocontrato.idEstadoContrato "
					+ "WHERE contrato.idContrato=" + datosContrato.getIdContrato();

			ResultSet rs = st.executeQuery(sql);

			if (rs.next()) { // Para cada servicio
				// Recogemos los valores de la db
				Contrato contrato = new Contrato();
				contrato.setIdContrato(rs.getInt("idContrato"));
				contrato.setDescripcion(rs.getString("descripcion"));
				contrato.setDireccEmpl(rs.getString("emplazamiento"));
				contrato.setAforo(rs.getInt("aforo"));
				contrato.setFechaIni(rs.getDate("fechaInicioEvento"));
				contrato.setFechaFin(rs.getDate("fechaFinEvento"));
				contrato.setNombreCliente(
						rs.getString("nombre") + " " + rs.getString("apellido1") + " "+ rs.getString("apellido2"));
				contrato.setEmailCliente(rs.getString("email"));
				contrato.setEstado(rs.getString("estado"));
				contrato.setFechaCreacion(rs.getDate("fechaCreacionContrato"));
				contrato.setUrlImagen(rs.getString("urlImagen"));
				// A�adimos el patrocinado al array
				devolver.setResultado(contrato);
			}

			con.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return devolver;
	}

	/**
	 * Auto generated method signature
	 * 
	 * @param listarContratosCliente
	 * @return listarContratosClienteResponse
	 */

	public ListarContratosClienteResponse listarContratosCliente(ListarContratosCliente listarContratosCliente) {
		ListarContratosClienteResponse devolver = new ListarContratosClienteResponse();

		try {
			// Conectamos a la bd
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			java.sql.Connection con = DriverManager.getConnection("jdbc:mysql://localhost/ievents", "root", "");

			java.sql.Statement st = con.createStatement();

			// Buscamos los servicios que hay que contratar para el evento
			String sql = "SELECT idContrato, contrato.descripcion, fechaCreacionContrato, fechaInicioEvento, fechaFinEvento, contrato.aforo, urlImagen, "
					+ "emplazamiento.nombre as emplazamiento, " + "estadocontrato.nombre as estado, "
					+ "cliente.nombre, cliente.apellido1, cliente.apellido2, cliente.email  "
					+ "FROM contrato INNER JOIN cliente ON contrato.Cliente_idCliente = cliente.idCliente "
					+ "LEFT JOIN emplazamiento ON contrato.Emplazamiento_idEmplazamiento = emplazamiento.idEmplazamiento "
					+ "INNER JOIN estadocontrato ON contrato.EstadoContrato_idEstadoContrato = estadocontrato.idEstadoContrato "
					+ "WHERE contrato.Cliente_idCliente=" + listarContratosCliente.getIdCliente();

			ResultSet rs = st.executeQuery(sql);

			while (rs.next()) { // Para cada servicio
				// Recogemos los valores de la db
				Contrato contrato = new Contrato();
				contrato.setIdContrato(rs.getInt("idContrato"));
				contrato.setDescripcion(rs.getString("descripcion"));
				if(rs.getString("emplazamiento") != null)
					contrato.setDireccEmpl(rs.getString("emplazamiento"));
				else
					contrato.setDireccEmpl("Sin emplazamiento");
				contrato.setAforo(rs.getInt("aforo"));
				contrato.setFechaIni(rs.getDate("fechaInicioEvento"));
				contrato.setFechaFin(rs.getDate("fechaFinEvento"));
				contrato.setNombreCliente(
						rs.getString("nombre") + " " + rs.getString("apellido1") + " "+ rs.getString("apellido2"));
				contrato.setEmailCliente(rs.getString("email"));
				contrato.setEstado(rs.getString("estado"));
				contrato.setFechaCreacion(rs.getDate("fechaCreacionContrato"));
				contrato.setUrlImagen(rs.getString("urlImagen"));
				// A�adimos el patrocinado al array
				devolver.addContratos(contrato);
			}

			con.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return devolver;
	}

	/**
	 * Auto generated method signature
	 * 
	 * @return listarEventosResponse
	 */

	public ListarEventosResponse listarEventos() {
		ListarEventosResponse devolver = new ListarEventosResponse();

		try {
			// Conectamos a la bd
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			java.sql.Connection con = DriverManager.getConnection("jdbc:mysql://localhost/ievents", "root", "");

			java.sql.Statement st = con.createStatement();

			String sql = "SELECT evento.idEvento, evento.aforo, evento.fechaInicio, evento.fechaCierre, contrato.descripcion, emplazamiento.Direccion, cliente.nombre, cliente.apellido1, cliente.apellido2 "
					+ "FROM `evento` INNER JOIN `contrato` ON evento.Contrato_idContrato = contrato.idContrato "
					+ "INNER JOIN emplazamiento ON contrato.Emplazamiento_idEmplazamiento = emplazamiento.idEmplazamiento "
					+ "INNER JOIN cliente ON contrato.Cliente_idCliente = cliente.idCliente";

			ResultSet rs = st.executeQuery(sql); // Ejecutamos la consulta
			while (rs.next()) {
				Evento tmp = new Evento();
				tmp.setIdEvento(rs.getInt("idEvento"));
				tmp.setNombreCliente(rs.getString("nombre") + " " + rs.getString("apellido1") + " "+ rs.getString("apellido2"));
				tmp.setFechaIni(rs.getDate("fechaInicio"));
				tmp.setFechaFin(rs.getDate("fechaCierre"));
				tmp.setAforo(rs.getInt("aforo"));
				tmp.setDescripcion(rs.getString("descripcion"));
				tmp.setDireccEmpl(rs.getString("direccion"));
				devolver.addRespuesta(tmp);
			}
			con.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return devolver;
	}

	/**
	 * Auto generated method signature
	 * 
	 * @return listarClientesResponse
	 */

	public ListarClientesResponse listarClientes() {
		ListarClientesResponse devolver = new ListarClientesResponse();
		try {
			// Conectamos a la bd
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			java.sql.Connection con = DriverManager.getConnection("jdbc:mysql://localhost/ievents", "root", "");

			java.sql.Statement st = con.createStatement();

			String sql = "SELECT idCliente, nombre, apellido1, apellido2, email, dni FROM cliente";

			ResultSet rs = st.executeQuery(sql);

			while (rs.next()) {
				Cliente cli = new Cliente();

				cli.setId(rs.getInt("idCliente"));
				cli.setApellido1(rs.getString("apellido1"));
				cli.setApellido2(rs.getString("apellido2"));
				cli.setNombre(rs.getString("nombre"));
				cli.setEmail(rs.getString("email"));
				cli.setDni(rs.getString("dni"));

				devolver.addClientes(cli);
			}

			con.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return devolver;
	}

	/**
	 * Auto generated method signature
	 * 
	 * @param listarPatrocinadoresPreguntados
	 * @return listarPatrocinadoresPreguntadosResponse
	 */

	public ListarPatrocinadoresPreguntadosResponse listarPatrocinadoresPreguntados(
			ListarPatrocinadoresPreguntados listarPatrocinadoresPreguntados) {

		ListarPatrocinadoresPreguntadosResponse devolver = new ListarPatrocinadoresPreguntadosResponse();

		try {
			// Conectamos a la bd
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			java.sql.Connection con = DriverManager.getConnection("jdbc:mysql://localhost/ievents", "root", "");

			java.sql.Statement st = con.createStatement();

			String sql = "SELECT patrocinador.idPatrocinador, nombre, Contacto, email "
					+ "FROM patrocinador INNER JOIN contrato_has_patrocinador ON patrocinador.idPatrocinador = contrato_has_patrocinador.Patrocinador_idPatrocinador "
					+ "WHERE contrato_has_patrocinador.Contrato_idContrato = "
					+ listarPatrocinadoresPreguntados.getIdContrato();

			ResultSet rs = st.executeQuery(sql);

			while (rs.next()) {
				Patrocinador pat = new Patrocinador();
				pat.setIdPatrocinador(rs.getInt("idPatrocinador"));
				pat.setNombre(rs.getString("nombre"));
				pat.setContacto(rs.getString("contacto"));
				pat.setEmail(rs.getString("email"));
				devolver.addResultado(pat);
			}

			con.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return devolver;
	}

	/**
	 * Auto generated method signature
	 * 
	 * @param listarPatrocinadoresContratados
	 * @return listarPatrocinadoresContratadosResponse
	 */

	public ListarPatrocinadoresContratadosResponse listarPatrocinadoresContratados(
			ListarPatrocinadoresContratados listarPatrocinadoresContratados) {
		ListarPatrocinadoresContratadosResponse devolver = new ListarPatrocinadoresContratadosResponse();

		try {
			// Conectamos a la bd
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			java.sql.Connection con = DriverManager.getConnection("jdbc:mysql://localhost/ievents", "root", "");

			java.sql.Statement st = con.createStatement();

			String sql = "SELECT patrocinador.idPatrocinador, nombre, Contacto, email "
					+ "FROM patrocinador INNER JOIN evento_has_patrocinador ON patrocinador.idPatrocinador = evento_has_patrocinador.Patrocinador_idPatrocinador "
					+ "WHERE evento_has_patrocinador.Evento_idEvento = "
					+ listarPatrocinadoresContratados.getIdEvento();

			ResultSet rs = st.executeQuery(sql);

			while (rs.next()) {
				Patrocinador pat = new Patrocinador();
				pat.setIdPatrocinador(rs.getInt("idPatrocinador"));
				pat.setNombre(rs.getString("nombre"));
				pat.setContacto(rs.getString("contacto"));
				pat.setEmail(rs.getString("email"));
				devolver.addResultado(pat);
			}

			con.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return devolver;
	}

}
