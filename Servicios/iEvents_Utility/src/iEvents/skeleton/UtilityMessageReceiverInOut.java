
/**
 * UtilityMessageReceiverInOut.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.3  Built on : Jun 27, 2015 (11:17:49 BST)
 */
        package iEvents.skeleton;

        /**
        *  UtilityMessageReceiverInOut message receiver
        */

        public class UtilityMessageReceiverInOut extends org.apache.axis2.receivers.AbstractInOutMessageReceiver{


        public void invokeBusinessLogic(org.apache.axis2.context.MessageContext msgContext, org.apache.axis2.context.MessageContext newMsgContext)
        throws org.apache.axis2.AxisFault{

        try {

        // get the implementation class for the Web Service
        Object obj = getTheImplementationObject(msgContext);

        UtilitySkeleton skel = (UtilitySkeleton)obj;
        //Out Envelop
        org.apache.axiom.soap.SOAPEnvelope envelope = null;
        //Find the axisOperation that has been set by the Dispatch phase.
        org.apache.axis2.description.AxisOperation op = msgContext.getOperationContext().getAxisOperation();
        if (op == null) {
        throw new org.apache.axis2.AxisFault("Operation is not located, if this is doclit style the SOAP-ACTION should specified via the SOAP Action to use the RawXMLProvider");
        }

        java.lang.String methodName;
        if((op.getName() != null) && ((methodName = org.apache.axis2.util.JavaUtils.xmlNameToJavaIdentifier(op.getName().getLocalPart())) != null)){


        

            if("listarNombreEmplazamientosPosibles".equals(methodName)){
                
                iEvents.servicios.ListarNombreEmplazamientosPosiblesResponse listarNombreEmplazamientosPosiblesResponse57 = null;
	                        iEvents.servicios.ListarNombreEmplazamientosPosibles wrappedParam =
                                                             (iEvents.servicios.ListarNombreEmplazamientosPosibles)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    iEvents.servicios.ListarNombreEmplazamientosPosibles.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               listarNombreEmplazamientosPosiblesResponse57 =
                                                   
                                                   
                                                         skel.listarNombreEmplazamientosPosibles(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), listarNombreEmplazamientosPosiblesResponse57, false, new javax.xml.namespace.QName("http://www.example.org/Utility/",
                                                    "listarNombreEmplazamientosPosibles"));
                                    } else 

            if("listarNombrePatrocinadoresPosibles".equals(methodName)){
                
                iEvents.servicios.ListarNombrePatrocinadoresPosiblesResponse listarNombrePatrocinadoresPosiblesResponse59 = null;
	                        iEvents.servicios.ListarNombrePatrocinadoresPosibles wrappedParam =
                                                             (iEvents.servicios.ListarNombrePatrocinadoresPosibles)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    iEvents.servicios.ListarNombrePatrocinadoresPosibles.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               listarNombrePatrocinadoresPosiblesResponse59 =
                                                   
                                                   
                                                         skel.listarNombrePatrocinadoresPosibles(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), listarNombrePatrocinadoresPosiblesResponse59, false, new javax.xml.namespace.QName("http://www.example.org/Utility/",
                                                    "listarNombrePatrocinadoresPosibles"));
                                    } else 

            if("datosCliente".equals(methodName)){
                
                iEvents.servicios.DatosClienteResponse datosClienteResponse61 = null;
	                        iEvents.servicios.DatosCliente wrappedParam =
                                                             (iEvents.servicios.DatosCliente)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    iEvents.servicios.DatosCliente.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               datosClienteResponse61 =
                                                   
                                                   
                                                         skel.datosCliente(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), datosClienteResponse61, false, new javax.xml.namespace.QName("http://www.example.org/Utility/",
                                                    "datosCliente"));
                                    } else 

            if("listarContratos".equals(methodName)){
                
                iEvents.servicios.ListarContratosResponse listarContratosResponse63 = null;
	                        listarContratosResponse63 =
                                                     
                                                 skel.listarContratos()
                                                ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), listarContratosResponse63, false, new javax.xml.namespace.QName("http://www.example.org/Utility/",
                                                    "listarContratos"));
                                    } else 

            if("listarNombreEstadosContrato".equals(methodName)){
                
                iEvents.servicios.ListarNombreEstadosContratoResponse listarNombreEstadosContratoResponse65 = null;
	                        listarNombreEstadosContratoResponse65 =
                                                     
                                                 skel.listarNombreEstadosContrato()
                                                ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), listarNombreEstadosContratoResponse65, false, new javax.xml.namespace.QName("http://www.example.org/Utility/",
                                                    "listarNombreEstadosContrato"));
                                    } else 

            if("datosContrato".equals(methodName)){
                
                iEvents.servicios.DatosContratoResponse datosContratoResponse67 = null;
	                        iEvents.servicios.DatosContrato wrappedParam =
                                                             (iEvents.servicios.DatosContrato)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    iEvents.servicios.DatosContrato.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               datosContratoResponse67 =
                                                   
                                                   
                                                         skel.datosContrato(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), datosContratoResponse67, false, new javax.xml.namespace.QName("http://www.example.org/Utility/",
                                                    "datosContrato"));
                                    } else 

            if("listarContratosCliente".equals(methodName)){
                
                iEvents.servicios.ListarContratosClienteResponse listarContratosClienteResponse69 = null;
	                        iEvents.servicios.ListarContratosCliente wrappedParam =
                                                             (iEvents.servicios.ListarContratosCliente)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    iEvents.servicios.ListarContratosCliente.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               listarContratosClienteResponse69 =
                                                   
                                                   
                                                         skel.listarContratosCliente(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), listarContratosClienteResponse69, false, new javax.xml.namespace.QName("http://www.example.org/Utility/",
                                                    "listarContratosCliente"));
                                    } else 

            if("listarNombreServiciosPosibles".equals(methodName)){
                
                iEvents.servicios.ListarNombreServiciosPosiblesResponse listarNombreServiciosPosiblesResponse71 = null;
	                        iEvents.servicios.ListarNombreServiciosPosibles wrappedParam =
                                                             (iEvents.servicios.ListarNombreServiciosPosibles)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    iEvents.servicios.ListarNombreServiciosPosibles.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               listarNombreServiciosPosiblesResponse71 =
                                                   
                                                   
                                                         skel.listarNombreServiciosPosibles(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), listarNombreServiciosPosiblesResponse71, false, new javax.xml.namespace.QName("http://www.example.org/Utility/",
                                                    "listarNombreServiciosPosibles"));
                                    } else 

            if("listarEventos".equals(methodName)){
                
                iEvents.servicios.ListarEventosResponse listarEventosResponse73 = null;
	                        listarEventosResponse73 =
                                                     
                                                 skel.listarEventos()
                                                ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), listarEventosResponse73, false, new javax.xml.namespace.QName("http://www.example.org/Utility/",
                                                    "listarEventos"));
                                    } else 

            if("listarServiciosContrato".equals(methodName)){
                
                iEvents.servicios.ListarServiciosContratoResponse listarServiciosContratoResponse75 = null;
	                        iEvents.servicios.ListarServiciosContrato wrappedParam =
                                                             (iEvents.servicios.ListarServiciosContrato)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    iEvents.servicios.ListarServiciosContrato.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               listarServiciosContratoResponse75 =
                                                   
                                                   
                                                         skel.listarServiciosContrato(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), listarServiciosContratoResponse75, false, new javax.xml.namespace.QName("http://www.example.org/Utility/",
                                                    "listarServiciosContrato"));
                                    } else 

            if("listarClientes".equals(methodName)){
                
                iEvents.servicios.ListarClientesResponse listarClientesResponse77 = null;
	                        listarClientesResponse77 =
                                                     
                                                 skel.listarClientes()
                                                ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), listarClientesResponse77, false, new javax.xml.namespace.QName("http://www.example.org/Utility/",
                                                    "listarClientes"));
                                    } else 

            if("listarPatrocinadoresPreguntados".equals(methodName)){
                
                iEvents.servicios.ListarPatrocinadoresPreguntadosResponse listarPatrocinadoresPreguntadosResponse79 = null;
	                        iEvents.servicios.ListarPatrocinadoresPreguntados wrappedParam =
                                                             (iEvents.servicios.ListarPatrocinadoresPreguntados)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    iEvents.servicios.ListarPatrocinadoresPreguntados.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               listarPatrocinadoresPreguntadosResponse79 =
                                                   
                                                   
                                                         skel.listarPatrocinadoresPreguntados(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), listarPatrocinadoresPreguntadosResponse79, false, new javax.xml.namespace.QName("http://www.example.org/Utility/",
                                                    "listarPatrocinadoresPreguntados"));
                                    } else 

            if("listarPatrocinadoresContratados".equals(methodName)){
                
                iEvents.servicios.ListarPatrocinadoresContratadosResponse listarPatrocinadoresContratadosResponse81 = null;
	                        iEvents.servicios.ListarPatrocinadoresContratados wrappedParam =
                                                             (iEvents.servicios.ListarPatrocinadoresContratados)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    iEvents.servicios.ListarPatrocinadoresContratados.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               listarPatrocinadoresContratadosResponse81 =
                                                   
                                                   
                                                         skel.listarPatrocinadoresContratados(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), listarPatrocinadoresContratadosResponse81, false, new javax.xml.namespace.QName("http://www.example.org/Utility/",
                                                    "listarPatrocinadoresContratados"));
                                    
            } else {
              throw new java.lang.RuntimeException("method not found");
            }
        

        newMsgContext.setEnvelope(envelope);
        }
        }
        catch (java.lang.Exception e) {
        throw org.apache.axis2.AxisFault.makeFault(e);
        }
        }
        
        //
            private  org.apache.axiom.om.OMElement  toOM(iEvents.servicios.ListarNombreEmplazamientosPosibles param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(iEvents.servicios.ListarNombreEmplazamientosPosibles.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(iEvents.servicios.ListarNombreEmplazamientosPosiblesResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(iEvents.servicios.ListarNombreEmplazamientosPosiblesResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(iEvents.servicios.ListarNombrePatrocinadoresPosibles param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(iEvents.servicios.ListarNombrePatrocinadoresPosibles.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(iEvents.servicios.ListarNombrePatrocinadoresPosiblesResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(iEvents.servicios.ListarNombrePatrocinadoresPosiblesResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(iEvents.servicios.DatosCliente param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(iEvents.servicios.DatosCliente.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(iEvents.servicios.DatosClienteResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(iEvents.servicios.DatosClienteResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(iEvents.servicios.ListarContratosResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(iEvents.servicios.ListarContratosResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(iEvents.servicios.ListarNombreEstadosContratoResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(iEvents.servicios.ListarNombreEstadosContratoResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(iEvents.servicios.DatosContrato param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(iEvents.servicios.DatosContrato.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(iEvents.servicios.DatosContratoResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(iEvents.servicios.DatosContratoResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(iEvents.servicios.ListarContratosCliente param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(iEvents.servicios.ListarContratosCliente.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(iEvents.servicios.ListarContratosClienteResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(iEvents.servicios.ListarContratosClienteResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(iEvents.servicios.ListarNombreServiciosPosibles param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(iEvents.servicios.ListarNombreServiciosPosibles.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(iEvents.servicios.ListarNombreServiciosPosiblesResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(iEvents.servicios.ListarNombreServiciosPosiblesResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(iEvents.servicios.ListarEventosResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(iEvents.servicios.ListarEventosResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(iEvents.servicios.ListarServiciosContrato param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(iEvents.servicios.ListarServiciosContrato.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(iEvents.servicios.ListarServiciosContratoResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(iEvents.servicios.ListarServiciosContratoResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(iEvents.servicios.ListarClientesResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(iEvents.servicios.ListarClientesResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(iEvents.servicios.ListarPatrocinadoresPreguntados param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(iEvents.servicios.ListarPatrocinadoresPreguntados.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(iEvents.servicios.ListarPatrocinadoresPreguntadosResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(iEvents.servicios.ListarPatrocinadoresPreguntadosResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(iEvents.servicios.ListarPatrocinadoresContratados param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(iEvents.servicios.ListarPatrocinadoresContratados.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(iEvents.servicios.ListarPatrocinadoresContratadosResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(iEvents.servicios.ListarPatrocinadoresContratadosResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, iEvents.servicios.ListarNombreEmplazamientosPosiblesResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(iEvents.servicios.ListarNombreEmplazamientosPosiblesResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private iEvents.servicios.ListarNombreEmplazamientosPosiblesResponse wraplistarNombreEmplazamientosPosibles(){
                                iEvents.servicios.ListarNombreEmplazamientosPosiblesResponse wrappedElement = new iEvents.servicios.ListarNombreEmplazamientosPosiblesResponse();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, iEvents.servicios.ListarNombrePatrocinadoresPosiblesResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(iEvents.servicios.ListarNombrePatrocinadoresPosiblesResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private iEvents.servicios.ListarNombrePatrocinadoresPosiblesResponse wraplistarNombrePatrocinadoresPosibles(){
                                iEvents.servicios.ListarNombrePatrocinadoresPosiblesResponse wrappedElement = new iEvents.servicios.ListarNombrePatrocinadoresPosiblesResponse();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, iEvents.servicios.DatosClienteResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(iEvents.servicios.DatosClienteResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private iEvents.servicios.DatosClienteResponse wrapdatosCliente(){
                                iEvents.servicios.DatosClienteResponse wrappedElement = new iEvents.servicios.DatosClienteResponse();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, iEvents.servicios.ListarContratosResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(iEvents.servicios.ListarContratosResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private iEvents.servicios.ListarContratosResponse wraplistarContratos(){
                                iEvents.servicios.ListarContratosResponse wrappedElement = new iEvents.servicios.ListarContratosResponse();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, iEvents.servicios.ListarNombreEstadosContratoResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(iEvents.servicios.ListarNombreEstadosContratoResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private iEvents.servicios.ListarNombreEstadosContratoResponse wraplistarNombreEstadosContrato(){
                                iEvents.servicios.ListarNombreEstadosContratoResponse wrappedElement = new iEvents.servicios.ListarNombreEstadosContratoResponse();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, iEvents.servicios.DatosContratoResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(iEvents.servicios.DatosContratoResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private iEvents.servicios.DatosContratoResponse wrapdatosContrato(){
                                iEvents.servicios.DatosContratoResponse wrappedElement = new iEvents.servicios.DatosContratoResponse();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, iEvents.servicios.ListarContratosClienteResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(iEvents.servicios.ListarContratosClienteResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private iEvents.servicios.ListarContratosClienteResponse wraplistarContratosCliente(){
                                iEvents.servicios.ListarContratosClienteResponse wrappedElement = new iEvents.servicios.ListarContratosClienteResponse();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, iEvents.servicios.ListarNombreServiciosPosiblesResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(iEvents.servicios.ListarNombreServiciosPosiblesResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private iEvents.servicios.ListarNombreServiciosPosiblesResponse wraplistarNombreServiciosPosibles(){
                                iEvents.servicios.ListarNombreServiciosPosiblesResponse wrappedElement = new iEvents.servicios.ListarNombreServiciosPosiblesResponse();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, iEvents.servicios.ListarEventosResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(iEvents.servicios.ListarEventosResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private iEvents.servicios.ListarEventosResponse wraplistarEventos(){
                                iEvents.servicios.ListarEventosResponse wrappedElement = new iEvents.servicios.ListarEventosResponse();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, iEvents.servicios.ListarServiciosContratoResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(iEvents.servicios.ListarServiciosContratoResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private iEvents.servicios.ListarServiciosContratoResponse wraplistarServiciosContrato(){
                                iEvents.servicios.ListarServiciosContratoResponse wrappedElement = new iEvents.servicios.ListarServiciosContratoResponse();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, iEvents.servicios.ListarClientesResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(iEvents.servicios.ListarClientesResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private iEvents.servicios.ListarClientesResponse wraplistarClientes(){
                                iEvents.servicios.ListarClientesResponse wrappedElement = new iEvents.servicios.ListarClientesResponse();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, iEvents.servicios.ListarPatrocinadoresPreguntadosResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(iEvents.servicios.ListarPatrocinadoresPreguntadosResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private iEvents.servicios.ListarPatrocinadoresPreguntadosResponse wraplistarPatrocinadoresPreguntados(){
                                iEvents.servicios.ListarPatrocinadoresPreguntadosResponse wrappedElement = new iEvents.servicios.ListarPatrocinadoresPreguntadosResponse();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, iEvents.servicios.ListarPatrocinadoresContratadosResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(iEvents.servicios.ListarPatrocinadoresContratadosResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private iEvents.servicios.ListarPatrocinadoresContratadosResponse wraplistarPatrocinadoresContratados(){
                                iEvents.servicios.ListarPatrocinadoresContratadosResponse wrappedElement = new iEvents.servicios.ListarPatrocinadoresContratadosResponse();
                                return wrappedElement;
                         }
                    


        /**
        *  get the default envelope
        */
        private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory){
        return factory.getDefaultEnvelope();
        }


        private  java.lang.Object fromOM(
        org.apache.axiom.om.OMElement param,
        java.lang.Class type,
        java.util.Map extraNamespaces) throws org.apache.axis2.AxisFault{

        try {
        
                if (iEvents.servicios.DatosCliente.class.equals(type)){
                
                        return iEvents.servicios.DatosCliente.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (iEvents.servicios.DatosClienteResponse.class.equals(type)){
                
                        return iEvents.servicios.DatosClienteResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (iEvents.servicios.DatosContrato.class.equals(type)){
                
                        return iEvents.servicios.DatosContrato.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (iEvents.servicios.DatosContratoResponse.class.equals(type)){
                
                        return iEvents.servicios.DatosContratoResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (iEvents.servicios.ListarClientesResponse.class.equals(type)){
                
                        return iEvents.servicios.ListarClientesResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (iEvents.servicios.ListarContratosCliente.class.equals(type)){
                
                        return iEvents.servicios.ListarContratosCliente.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (iEvents.servicios.ListarContratosClienteResponse.class.equals(type)){
                
                        return iEvents.servicios.ListarContratosClienteResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (iEvents.servicios.ListarContratosResponse.class.equals(type)){
                
                        return iEvents.servicios.ListarContratosResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (iEvents.servicios.ListarEventosResponse.class.equals(type)){
                
                        return iEvents.servicios.ListarEventosResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (iEvents.servicios.ListarNombreEmplazamientosPosibles.class.equals(type)){
                
                        return iEvents.servicios.ListarNombreEmplazamientosPosibles.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (iEvents.servicios.ListarNombreEmplazamientosPosiblesResponse.class.equals(type)){
                
                        return iEvents.servicios.ListarNombreEmplazamientosPosiblesResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (iEvents.servicios.ListarNombreEstadosContratoResponse.class.equals(type)){
                
                        return iEvents.servicios.ListarNombreEstadosContratoResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (iEvents.servicios.ListarNombrePatrocinadoresPosibles.class.equals(type)){
                
                        return iEvents.servicios.ListarNombrePatrocinadoresPosibles.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (iEvents.servicios.ListarNombrePatrocinadoresPosiblesResponse.class.equals(type)){
                
                        return iEvents.servicios.ListarNombrePatrocinadoresPosiblesResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (iEvents.servicios.ListarNombreServiciosPosibles.class.equals(type)){
                
                        return iEvents.servicios.ListarNombreServiciosPosibles.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (iEvents.servicios.ListarNombreServiciosPosiblesResponse.class.equals(type)){
                
                        return iEvents.servicios.ListarNombreServiciosPosiblesResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (iEvents.servicios.ListarPatrocinadoresContratados.class.equals(type)){
                
                        return iEvents.servicios.ListarPatrocinadoresContratados.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (iEvents.servicios.ListarPatrocinadoresContratadosResponse.class.equals(type)){
                
                        return iEvents.servicios.ListarPatrocinadoresContratadosResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (iEvents.servicios.ListarPatrocinadoresPreguntados.class.equals(type)){
                
                        return iEvents.servicios.ListarPatrocinadoresPreguntados.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (iEvents.servicios.ListarPatrocinadoresPreguntadosResponse.class.equals(type)){
                
                        return iEvents.servicios.ListarPatrocinadoresPreguntadosResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (iEvents.servicios.ListarServiciosContrato.class.equals(type)){
                
                        return iEvents.servicios.ListarServiciosContrato.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (iEvents.servicios.ListarServiciosContratoResponse.class.equals(type)){
                
                        return iEvents.servicios.ListarServiciosContratoResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
        } catch (java.lang.Exception e) {
        throw org.apache.axis2.AxisFault.makeFault(e);
        }
           return null;
        }



    

        /**
        *  A utility method that copies the namepaces from the SOAPEnvelope
        */
        private java.util.Map getEnvelopeNamespaces(org.apache.axiom.soap.SOAPEnvelope env){
        java.util.Map returnMap = new java.util.HashMap();
        java.util.Iterator namespaceIterator = env.getAllDeclaredNamespaces();
        while (namespaceIterator.hasNext()) {
        org.apache.axiom.om.OMNamespace ns = (org.apache.axiom.om.OMNamespace) namespaceIterator.next();
        returnMap.put(ns.getPrefix(),ns.getNamespaceURI());
        }
        return returnMap;
        }

        private org.apache.axis2.AxisFault createAxisFault(java.lang.Exception e) {
        org.apache.axis2.AxisFault f;
        Throwable cause = e.getCause();
        if (cause != null) {
            f = new org.apache.axis2.AxisFault(e.getMessage(), cause);
        } else {
            f = new org.apache.axis2.AxisFault(e.getMessage());
        }

        return f;
    }

        }//end of class
    