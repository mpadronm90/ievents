
/**
 * EmplazamientoSkeleton.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.3  Built on : Jun 27, 2015 (11:17:49 BST)
 */
package iEvents.skeleton;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import iEvents.servicios.ComprobarDisponibilidad;
import iEvents.servicios.ComprobarDisponibilidadResponse;
import iEvents.servicios.Reservar;
import iEvents.servicios.ReservarResponse;

/**
 *  EmplazamientoSkeleton java skeleton for the axisService
 */
public class EmplazamientoSkeleton{
	
    /**
     * Asignamos al contrato el emplazamiento.
     * 
     * @param reservar Recibimos la id del emplazamiento y del contrato.
     * @return reservarResponse devuelve true si todo va bien y false si no.
     */
	@SuppressWarnings("finally")
	public ReservarResponse reservar ( Reservar reservar )
	{
		ReservarResponse response = new ReservarResponse();
		response.setResult(false);
		int respuesta = 0;
		
		try{
			Class.forName("com.mysql.jdbc.Driver");
			java.sql.Connection conexion = DriverManager.getConnection("jdbc:mysql://localhost/ievents", "root", "");
			
			Statement statement = conexion.createStatement();
			
			//Insertamos en la tabla la nueva relaci�n.
			respuesta = statement.executeUpdate("UPDATE contrato c SET Emplazamiento_idEmplazamiento = " + reservar.getIdEmplazamiento()
								+ " WHERE c.idContrato = " + reservar.getIdContrato());

			conexion.close();
			
			if(respuesta > 0 )
				response.setResult(true);
			
		} catch( Exception e ) {
			System.out.println("Error: "+e.toString());
			
		}finally{
			return response;
		}
	}
     
    /**
     * Comprueba que existe un local (pasado por parametro) disponible con las condiciones adecuadas para el evento
     * - que no tengan un eveno asociado entre las fechas indicadas
     * - que su aforo sea igual o superior al proporcionado como necesario
     * 
	 * @param comprobarDisponibilidad 
	 * @return comprobarDisponibilidadResponse 
     */
    @SuppressWarnings("finally")
	public ComprobarDisponibilidadResponse comprobarDisponibilidad ( ComprobarDisponibilidad comprobarDisponibilidad )
    {
    	ComprobarDisponibilidadResponse response = new ComprobarDisponibilidadResponse();
		response.setResult(false);
		
		try{
			Class.forName("com.mysql.jdbc.Driver");
			java.sql.Connection conexion = DriverManager.getConnection("jdbc:mysql://localhost/ievents", "root", "");
			
			Statement statement = conexion.createStatement();
    	
			String consulta = "SELECT idEmplazamiento"
							+ " FROM emplazamiento"
							+ " WHERE "+ comprobarDisponibilidad.getIdEmplazamiento() +" IN"
									+ " (SELECT idEmplazamiento"
									+ " FROM emplazamiento"
									+ " WHERE idEmplazamiento NOT IN"
										+ " (SELECT Emplazamiento_idEmplazamiento"
										+ " FROM contrato c"
										+ " WHERE Emplazamiento_idEmplazamiento IS NOT NULL "
										+ "AND (c.fechaInicioEvento <= \""+ comprobarDisponibilidad.getFechaInicial() +"\" "
												+ "OR c.fechaInicioEvento >= \""+ comprobarDisponibilidad.getFechaFinal() +"\""
										+ ") AND (c.fechaFinEvento >= \""+ comprobarDisponibilidad.getFechaFinal() +"\" "
												+ "OR c.fechaFinEvento <= \""+ comprobarDisponibilidad.getFechaInicial() +"\"))"
									+ " AND Aforo >= "+ comprobarDisponibilidad.getAforoNecesario()+")";

			ResultSet rs = statement.executeQuery(consulta);
			
			if( rs.next() ){
				response.setResult(true);
			}
			
			conexion.close();
			
		} catch( Exception e ) {
			System.out.println("Error: "+e.toString());
			
		}finally{
			return response;
		}
    }
 
}
