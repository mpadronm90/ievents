
/**
 * ExtensionMapper.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.3  Built on : Jun 27, 2015 (11:18:31 BST)
 */

        
            package iEvents.servicios.Patrocinador;
        
            /**
            *  ExtensionMapper class
            */
            @SuppressWarnings({"unchecked","unused"})
        
        public  class ExtensionMapper{

          public static java.lang.Object getTypeObject(java.lang.String namespaceURI,
                                                       java.lang.String typeName,
                                                       javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{

              
                  if (
                  "http://www.example.org/patrocionador/".equals(namespaceURI) &&
                  "Contrato".equals(typeName)){
                   
                            return  iEvents.servicios.Patrocinador.Contrato.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://www.example.org/patrocionador/".equals(namespaceURI) &&
                  "Evento".equals(typeName)){
                   
                            return  iEvents.servicios.Patrocinador.Evento.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://www.example.org/patrocionador/".equals(namespaceURI) &&
                  "Patrocinador".equals(typeName)){
                   
                            return  iEvents.servicios.Patrocinador.Patrocinador.Factory.parse(reader);
                        

                  }

              
             throw new org.apache.axis2.databinding.ADBException("Unsupported type " + namespaceURI + " " + typeName);
          }

        }
    