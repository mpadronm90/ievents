
/**
 * ExtensionMapper.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.3  Built on : Jun 27, 2015 (11:18:31 BST)
 */

        
            package iEvents.servicios.Evento;
        
            /**
            *  ExtensionMapper class
            */
            @SuppressWarnings({"unchecked","unused"})
        
        public  class ExtensionMapper{

          public static java.lang.Object getTypeObject(java.lang.String namespaceURI,
                                                       java.lang.String typeName,
                                                       javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{

              
                  if (
                  "http://www.example.org/Evento/".equals(namespaceURI) &&
                  "TypeServicio".equals(typeName)){
                   
                            return  iEvents.servicios.Evento.TypeServicio.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://www.example.org/Evento/".equals(namespaceURI) &&
                  "TypeEventDTO".equals(typeName)){
                   
                            return  iEvents.servicios.Evento.TypeEventDTO.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://www.example.org/Evento/".equals(namespaceURI) &&
                  "EventoType".equals(typeName)){
                   
                            return  iEvents.servicios.Evento.EventoType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://www.example.org/Evento/".equals(namespaceURI) &&
                  "TypeCambiarEstado".equals(typeName)){
                   
                            return  iEvents.servicios.Evento.TypeCambiarEstado.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://www.example.org/Evento/".equals(namespaceURI) &&
                  "TypePatrocinador".equals(typeName)){
                   
                            return  iEvents.servicios.Evento.TypePatrocinador.Factory.parse(reader);
                        

                  }

              
             throw new org.apache.axis2.databinding.ADBException("Unsupported type " + namespaceURI + " " + typeName);
          }

        }
    