
/**
 * ContratoSkeleton.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.3  Built on : Jun 27, 2015 (11:17:49 BST)
 */
package iEvents.skeleton;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.util.Date;

import iEvents.servicios.Contrato.*;

/**
 * ContratoSkeleton java skeleton for the axisService
 */
public class ContratoSkeleton {

	/**
	 * Auto generated method signature
	 * 
	 * @param cambiarEstado
	 * @return cambiarEstadoResponse
	 */

	public CambiarEstadoResponse cambiarEstado(
			CambiarEstado cambiarEstado) {
		CambiarEstadoResponse devolver = new CambiarEstadoResponse();

		int idContrato = cambiarEstado.getIdContrato();
		int nuevoEstado = cambiarEstado.getNuevoEstado();
		// Suponemos que hay error como mensaje de salida
		// Si no es así lo cambiamos después del update
		devolver.setOut(false);

		try {
			// Conectamos a la bd
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			java.sql.Connection con = DriverManager.getConnection("jdbc:mysql://localhost/ievents", "root", "");

			java.sql.Statement st = con.createStatement();

			// Actualizamos el estado del contrato
			String sql = "UPDATE contrato SET EstadoContrato_idEstadoContrato='" + nuevoEstado + "' WHERE idContrato='"
					+ idContrato + "'";
			if (st.executeUpdate(sql) > 0) { // Si se ha modificado la fila
				devolver.setOut(true);
			}
			con.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return devolver;
	}

	/**
	 * Auto generated method signature
	 * 
	 * @param crear
	 * @return crearResponse
	 */

	public CrearContratoResponse crearContrato(CrearContrato crear) {
		CrearContratoResponse devolver = new CrearContratoResponse();
		// Lo inicializamos a error al crear el contrato, si se crea
		// correctamente lo cambiamos luego
		devolver.setOut(0);
		int idContrato = 1; // Por si es el primero que vamos a crear

		// Cogemos los par�metros para trabajar m�s comodamente
		int idCliente = crear.getIdCliente();

		java.sql.Date fechaini = new java.sql.Date(crear.getFechaInicioEvento().getTime());
		java.sql.Date fechafin = new java.sql.Date(crear.getFechaFinEvento().getTime());
		String descripcion = crear.getDescripcion();
		int aforo = crear.getAforo();
		java.sql.Date fechaContrato = new java.sql.Date(new Date().getTime());
		try {
			// Conectamos a la bd
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			java.sql.Connection con = DriverManager.getConnection("jdbc:mysql://localhost/ievents", "root", "");

			java.sql.Statement st = con.createStatement();

			// Buscamos el �ltimo id de la db
			String sql = "SELECT idContrato FROM contrato ORDER BY idContrato DESC LIMIT 0,1";
			ResultSet rs = st.executeQuery(sql);

			if (rs.next()) { // Movemos el cursor a la fila seleccionada
				int nextId = rs.getInt("idContrato"); // Cogemos el ultimo id
				idContrato = ++nextId; // le sumamos uno
			}
			// Preparamos la consulta insert. Solo queda nulo el idEmplazamiento
			// que todav�a se desconoce
			sql = "INSERT INTO `contrato`(`idContrato`, `fechaCreacionContrato`, `fechaInicioEvento`,`fechaFinEvento`, `descripcion`, `Cliente_idCliente`, `EstadoContrato_idEstadoContrato`, `Aforo`, `urlImagen`) VALUES ('"
					+ idContrato + "','" + fechaContrato.toString() + "','" + fechaini.toString() + "','"
					+ fechafin.toString() + "','" + descripcion + "','" + idCliente + "','" + 1 + "','" + aforo + "','"+crear.getUrlimagen()+"')";
			// Se ejecuta el insert y se comprueba el resultado
			if (st.executeUpdate(sql) > 0) { // Si se ha modificado la fila,
												// devolvemos el id del contrato
												// recien creado
				devolver.setOut(idContrato);
			}

			con.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return devolver;
	}

}
