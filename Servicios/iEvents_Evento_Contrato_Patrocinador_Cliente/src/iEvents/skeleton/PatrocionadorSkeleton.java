
/**
 * PatrocionadorSkeleton.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.3  Built on : Jun 27, 2015 (11:17:49 BST)
 */
package iEvents.skeleton;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

import iEvents.servicios.Patrocinador.*;

/**
 *  PatrocionadorSkeleton java skeleton for the axisService
 */
public class PatrocionadorSkeleton{
    
	/**
	 * Devuelve una lista de id's de los posibles patrocinadores a contratar
	 * 
	 * @param obtenerPatrocinadoresPosibles
	 * @return obtenerPatrocinadoresPosiblesResponse
	 */
	public ObtenerPatrocinadoresPosiblesResponse obtenerPatrocinadoresPosibles(
			ObtenerPatrocinadoresPosibles obtenerPatrocinadoresPosibles) {
		// Preparamos el objeto de salida
		ObtenerPatrocinadoresPosiblesResponse devolver = new ObtenerPatrocinadoresPosiblesResponse();

		// Cogemos los par�metros para trabajar m�s c�modamente
		int idContrato = obtenerPatrocinadoresPosibles.getContrato().getIdContrato();
		try {
			// Conectamos a la bd
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			java.sql.Connection con = DriverManager.getConnection("jdbc:mysql://localhost/ievents", "root", "");

			java.sql.Statement st = con.createStatement();

			// Buscamos todos los posibles patrocinadores en la tabla
			// contrato_has_patrocinador
			String sql = "SELECT idPatrocinador, nombre, Contacto " + "FROM patrocinador " + "WHERE idPatrocinador IN "
					+ "(SELECT Patrocinador_idPatrocinador FROM contrato_has_patrocinador WHERE Contrato_idContrato="
					+ idContrato + ")";

			ResultSet rs = st.executeQuery(sql);

			while (rs.next()) { // Recorremos todas las tuplas asignando el id
								// al array que vamos a devolver
				Patrocinador tmp = new Patrocinador();
				tmp.setIdPatrocinador(rs.getInt("idPatrocinador"));
				tmp.setNombre(rs.getString("nombre"));
				tmp.setContacto(rs.getString("Contacto"));
				devolver.addPatrocinador(tmp);
			}
			con.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return devolver;
	}

	/**
	 * Contrata un patrocinador (lo borra de contrato_has_patrocinador y lo
	 * inserta en evento_has_patrocinador)
	 * 
	 * @param contratar
	 * @return contratarResponse
	 */
	public ContratarResponse contratar(Contratar contratar) {
		ContratarResponse devolver = new ContratarResponse();
		devolver.setOut(false);
		// Cogemos los par�metros para trabajar m�s c�modamente
		int idPatrocinador = contratar.getIdPatrocinador();
		int idEvento = contratar.getEvento().getIdEvento();
		int idContrato = contratar.getEvento().getIdContrato();

		try {
			// Conectamos a la bd
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			java.sql.Connection con = DriverManager.getConnection("jdbc:mysql://localhost/ievents", "root", "");

			try {
				java.sql.Statement st = con.createStatement();
				con.setAutoCommit(false); // Desactivamos la confirmaci�n
											// autom�tica para que no se
											// ejecuten en la db las consultas

				// Insertamos la relaci�n en evento_has_patrocinador
				String sql = "INSERT INTO evento_has_patrocinador VALUES (" + idEvento + "," + idContrato + ","
						+ idPatrocinador + ")";
				String sql1 = "DELETE FROM contrato_has_patrocinador WHERE Contrato_idContrato=" + idContrato
						+ " AND Patrocinador_idPatrocinador=" + idPatrocinador;
				
				System.out.println(sql + "\r\n" + sql1);
				int inserts = st.executeUpdate(sql);
				int deletes = st.executeUpdate(sql1);
				System.out.println("INSERTS: " + inserts + " Deletes: " + deletes);
				
				if (inserts > 0 && deletes > 0) { // Ejecutamos las 2 consultas una despu�s de otra y miramos si se han ejecutado correctamente
					System.out.println("ENTRO");
					devolver.setOut(true);
				} else { // Si una de las 2 consultas no ha ido bien, lanzamos
							// excepci�n para ejecutar un rollback y que los
							// datos en la db sigan consistentes
					throw new SQLException("No se ha podido realizar la consulta");
				}
				con.commit();
			} catch (SQLException ex) {
				con.rollback(); // Ejecutamos rollback para dejar los datos como
								// estaban
				con.setAutoCommit(true); // Lo volvemos a activar
				devolver.setOut(false);
			}

			con.close();
		} catch (Exception e) {

			e.printStackTrace();
		}

		return devolver;
	}

	/**
	 * Inserta una relaci�n (contrato_has_patrocinador)
	 * 
	 * @param insertarPatrocinadorPosible
	 * @return insertarPatrocinadorPosibleResponse
	 */
	public InsertarPatrocinadorPosibleResponse insertarPatrocinadorPosible(
			InsertarPatrocinadorPosible insertarPatrocinadorPosible) {
		InsertarPatrocinadorPosibleResponse devolver = new InsertarPatrocinadorPosibleResponse();
		devolver.setOut(false);
		// Cogemos los par�metros para trabajar m�s c�modamente
		int idContrato = insertarPatrocinadorPosible.getIdContrato();
		int idPatrocinador = insertarPatrocinadorPosible.getIdPatrocinador();
		try {
			// Conectamos a la bd
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			java.sql.Connection con = DriverManager.getConnection("jdbc:mysql://localhost/ievents", "root", "");

			java.sql.Statement st = con.createStatement();

			String sql = "INSERT INTO contrato_has_patrocinador VALUES (" + idContrato + "," + idPatrocinador + ")";

			if (st.executeUpdate(sql) > 0) {
				devolver.setOut(true);
			}
		} catch (Exception ex) {

		}

		return devolver;
	}
     
    /**
     * Auto generated method signature
     * 
     * @param eliminarPatrocinadorPosible 
     * @return eliminarPatrocinadorPosibleResponse 
     */
	public EliminarPatrocinadorPosibleResponse eliminarPatrocinadorPosible ( EliminarPatrocinadorPosible eliminarPatrocinadorPosible )
    {
		EliminarPatrocinadorPosibleResponse devolver = new EliminarPatrocinadorPosibleResponse();

		// Cogemos los par�metros para trabajar m�s comodamente
		int idContrato = eliminarPatrocinadorPosible.getIdContrato();
		int idpatrocinador = eliminarPatrocinadorPosible.getIdPatrocinador();

		// Suponemos que hay error como mensaje de salida
		// Si no es as� lo cambiamos despu�s del update
		devolver.setOut(false);
		try {
			// Conectamos a la bd
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			java.sql.Connection con = DriverManager.getConnection("jdbc:mysql://localhost/ievents", "root", "");

			java.sql.Statement st = con.createStatement();

			// Borramos el patrocinador a contratar (ya est� contratado)
			String sql = "DELETE FROM contrato_has_patrocinador WHERE Contrato_idContrato='" + idContrato
					+ "' and Patrocinador_idPatrocinador='" + idpatrocinador + "'";
			if (st.executeUpdate(sql) > 0) { // Si se ha eliminado
				devolver.setOut(true);
			}
			con.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return devolver;
    }
 
}
