
/**
 * PatrocionadorMessageReceiverInOut.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.3  Built on : Jun 27, 2015 (11:17:49 BST)
 */
        package iEvents.skeleton;

        /**
        *  PatrocionadorMessageReceiverInOut message receiver
        */

        public class PatrocionadorMessageReceiverInOut extends org.apache.axis2.receivers.AbstractInOutMessageReceiver{


        public void invokeBusinessLogic(org.apache.axis2.context.MessageContext msgContext, org.apache.axis2.context.MessageContext newMsgContext)
        throws org.apache.axis2.AxisFault{

        try {

        // get the implementation class for the Web Service
        Object obj = getTheImplementationObject(msgContext);

        PatrocionadorSkeleton skel = (PatrocionadorSkeleton)obj;
        //Out Envelop
        org.apache.axiom.soap.SOAPEnvelope envelope = null;
        //Find the axisOperation that has been set by the Dispatch phase.
        org.apache.axis2.description.AxisOperation op = msgContext.getOperationContext().getAxisOperation();
        if (op == null) {
        throw new org.apache.axis2.AxisFault("Operation is not located, if this is doclit style the SOAP-ACTION should specified via the SOAP Action to use the RawXMLProvider");
        }

        java.lang.String methodName;
        if((op.getName() != null) && ((methodName = org.apache.axis2.util.JavaUtils.xmlNameToJavaIdentifier(op.getName().getLocalPart())) != null)){


        

            if("eliminarPatrocinadorPosible".equals(methodName)){
                
                iEvents.servicios.Patrocinador.EliminarPatrocinadorPosibleResponse eliminarPatrocinadorPosibleResponse17 = null;
	                        iEvents.servicios.Patrocinador.EliminarPatrocinadorPosible wrappedParam =
                                                             (iEvents.servicios.Patrocinador.EliminarPatrocinadorPosible)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    iEvents.servicios.Patrocinador.EliminarPatrocinadorPosible.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               eliminarPatrocinadorPosibleResponse17 =
                                                   
                                                   
                                                         skel.eliminarPatrocinadorPosible(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), eliminarPatrocinadorPosibleResponse17, false, new javax.xml.namespace.QName("http://www.example.org/patrocionador/",
                                                    "eliminarPatrocinadorPosible"));
                                    } else 

            if("obtenerPatrocinadoresPosibles".equals(methodName)){
                
                iEvents.servicios.Patrocinador.ObtenerPatrocinadoresPosiblesResponse obtenerPatrocinadoresPosiblesResponse19 = null;
	                        iEvents.servicios.Patrocinador.ObtenerPatrocinadoresPosibles wrappedParam =
                                                             (iEvents.servicios.Patrocinador.ObtenerPatrocinadoresPosibles)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    iEvents.servicios.Patrocinador.ObtenerPatrocinadoresPosibles.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               obtenerPatrocinadoresPosiblesResponse19 =
                                                   
                                                   
                                                         skel.obtenerPatrocinadoresPosibles(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), obtenerPatrocinadoresPosiblesResponse19, false, new javax.xml.namespace.QName("http://www.example.org/patrocionador/",
                                                    "obtenerPatrocinadoresPosibles"));
                                    } else 

            if("contratar".equals(methodName)){
                
                iEvents.servicios.Patrocinador.ContratarResponse contratarResponse21 = null;
	                        iEvents.servicios.Patrocinador.Contratar wrappedParam =
                                                             (iEvents.servicios.Patrocinador.Contratar)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    iEvents.servicios.Patrocinador.Contratar.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               contratarResponse21 =
                                                   
                                                   
                                                         skel.contratar(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), contratarResponse21, false, new javax.xml.namespace.QName("http://www.example.org/patrocionador/",
                                                    "contratar"));
                                    } else 

            if("insertarPatrocinadorPosible".equals(methodName)){
                
                iEvents.servicios.Patrocinador.InsertarPatrocinadorPosibleResponse insertarPatrocinadorPosibleResponse23 = null;
	                        iEvents.servicios.Patrocinador.InsertarPatrocinadorPosible wrappedParam =
                                                             (iEvents.servicios.Patrocinador.InsertarPatrocinadorPosible)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    iEvents.servicios.Patrocinador.InsertarPatrocinadorPosible.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               insertarPatrocinadorPosibleResponse23 =
                                                   
                                                   
                                                         skel.insertarPatrocinadorPosible(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), insertarPatrocinadorPosibleResponse23, false, new javax.xml.namespace.QName("http://www.example.org/patrocionador/",
                                                    "insertarPatrocinadorPosible"));
                                    
            } else {
              throw new java.lang.RuntimeException("method not found");
            }
        

        newMsgContext.setEnvelope(envelope);
        }
        }
        catch (java.lang.Exception e) {
        throw org.apache.axis2.AxisFault.makeFault(e);
        }
        }
        
        //
            private  org.apache.axiom.om.OMElement  toOM(iEvents.servicios.Patrocinador.EliminarPatrocinadorPosible param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(iEvents.servicios.Patrocinador.EliminarPatrocinadorPosible.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(iEvents.servicios.Patrocinador.EliminarPatrocinadorPosibleResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(iEvents.servicios.Patrocinador.EliminarPatrocinadorPosibleResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(iEvents.servicios.Patrocinador.ObtenerPatrocinadoresPosibles param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(iEvents.servicios.Patrocinador.ObtenerPatrocinadoresPosibles.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(iEvents.servicios.Patrocinador.ObtenerPatrocinadoresPosiblesResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(iEvents.servicios.Patrocinador.ObtenerPatrocinadoresPosiblesResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(iEvents.servicios.Patrocinador.Contratar param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(iEvents.servicios.Patrocinador.Contratar.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(iEvents.servicios.Patrocinador.ContratarResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(iEvents.servicios.Patrocinador.ContratarResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(iEvents.servicios.Patrocinador.InsertarPatrocinadorPosible param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(iEvents.servicios.Patrocinador.InsertarPatrocinadorPosible.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(iEvents.servicios.Patrocinador.InsertarPatrocinadorPosibleResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(iEvents.servicios.Patrocinador.InsertarPatrocinadorPosibleResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, iEvents.servicios.Patrocinador.EliminarPatrocinadorPosibleResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(iEvents.servicios.Patrocinador.EliminarPatrocinadorPosibleResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private iEvents.servicios.Patrocinador.EliminarPatrocinadorPosibleResponse wrapeliminarPatrocinadorPosible(){
                                iEvents.servicios.Patrocinador.EliminarPatrocinadorPosibleResponse wrappedElement = new iEvents.servicios.Patrocinador.EliminarPatrocinadorPosibleResponse();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, iEvents.servicios.Patrocinador.ObtenerPatrocinadoresPosiblesResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(iEvents.servicios.Patrocinador.ObtenerPatrocinadoresPosiblesResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private iEvents.servicios.Patrocinador.ObtenerPatrocinadoresPosiblesResponse wrapobtenerPatrocinadoresPosibles(){
                                iEvents.servicios.Patrocinador.ObtenerPatrocinadoresPosiblesResponse wrappedElement = new iEvents.servicios.Patrocinador.ObtenerPatrocinadoresPosiblesResponse();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, iEvents.servicios.Patrocinador.ContratarResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(iEvents.servicios.Patrocinador.ContratarResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private iEvents.servicios.Patrocinador.ContratarResponse wrapcontratar(){
                                iEvents.servicios.Patrocinador.ContratarResponse wrappedElement = new iEvents.servicios.Patrocinador.ContratarResponse();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, iEvents.servicios.Patrocinador.InsertarPatrocinadorPosibleResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(iEvents.servicios.Patrocinador.InsertarPatrocinadorPosibleResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private iEvents.servicios.Patrocinador.InsertarPatrocinadorPosibleResponse wrapinsertarPatrocinadorPosible(){
                                iEvents.servicios.Patrocinador.InsertarPatrocinadorPosibleResponse wrappedElement = new iEvents.servicios.Patrocinador.InsertarPatrocinadorPosibleResponse();
                                return wrappedElement;
                         }
                    


        /**
        *  get the default envelope
        */
        private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory){
        return factory.getDefaultEnvelope();
        }


        private  java.lang.Object fromOM(
        org.apache.axiom.om.OMElement param,
        java.lang.Class type,
        java.util.Map extraNamespaces) throws org.apache.axis2.AxisFault{

        try {
        
                if (iEvents.servicios.Patrocinador.Contratar.class.equals(type)){
                
                        return iEvents.servicios.Patrocinador.Contratar.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (iEvents.servicios.Patrocinador.ContratarResponse.class.equals(type)){
                
                        return iEvents.servicios.Patrocinador.ContratarResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (iEvents.servicios.Patrocinador.EliminarPatrocinadorPosible.class.equals(type)){
                
                        return iEvents.servicios.Patrocinador.EliminarPatrocinadorPosible.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (iEvents.servicios.Patrocinador.EliminarPatrocinadorPosibleResponse.class.equals(type)){
                
                        return iEvents.servicios.Patrocinador.EliminarPatrocinadorPosibleResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (iEvents.servicios.Patrocinador.InsertarPatrocinadorPosible.class.equals(type)){
                
                        return iEvents.servicios.Patrocinador.InsertarPatrocinadorPosible.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (iEvents.servicios.Patrocinador.InsertarPatrocinadorPosibleResponse.class.equals(type)){
                
                        return iEvents.servicios.Patrocinador.InsertarPatrocinadorPosibleResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (iEvents.servicios.Patrocinador.ObtenerPatrocinadoresPosibles.class.equals(type)){
                
                        return iEvents.servicios.Patrocinador.ObtenerPatrocinadoresPosibles.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (iEvents.servicios.Patrocinador.ObtenerPatrocinadoresPosiblesResponse.class.equals(type)){
                
                        return iEvents.servicios.Patrocinador.ObtenerPatrocinadoresPosiblesResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
        } catch (java.lang.Exception e) {
        throw org.apache.axis2.AxisFault.makeFault(e);
        }
           return null;
        }



    

        /**
        *  A utility method that copies the namepaces from the SOAPEnvelope
        */
        private java.util.Map getEnvelopeNamespaces(org.apache.axiom.soap.SOAPEnvelope env){
        java.util.Map returnMap = new java.util.HashMap();
        java.util.Iterator namespaceIterator = env.getAllDeclaredNamespaces();
        while (namespaceIterator.hasNext()) {
        org.apache.axiom.om.OMNamespace ns = (org.apache.axiom.om.OMNamespace) namespaceIterator.next();
        returnMap.put(ns.getPrefix(),ns.getNamespaceURI());
        }
        return returnMap;
        }

        private org.apache.axis2.AxisFault createAxisFault(java.lang.Exception e) {
        org.apache.axis2.AxisFault f;
        Throwable cause = e.getCause();
        if (cause != null) {
            f = new org.apache.axis2.AxisFault(e.getMessage(), cause);
        } else {
            f = new org.apache.axis2.AxisFault(e.getMessage());
        }

        return f;
    }

        }//end of class
    