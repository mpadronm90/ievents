
/**
 * ClienteSkeleton.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.3  Built on : Jun 27, 2015 (11:17:49 BST)
 */
package iEvents.skeleton;

import java.sql.DriverManager;
import java.sql.ResultSet;

import iEvents.servicios.Cliente.Buscar;
import iEvents.servicios.Cliente.BuscarResponse;
import iEvents.servicios.Cliente.CrearCliente;
import iEvents.servicios.Cliente.CrearClienteResponse;
import iEvents.servicios.Contrato.*;

/**
 * ClienteSkeleton java skeleton for the axisService
 */
public class ClienteSkeleton {

	/**
	 * Recibe un identificador de cliente y devuelve si �ste existe en la base de datos o no
	 * 
	 * @param buscar
	 * @return buscarResponse
	 */

	public BuscarResponse buscar(Buscar buscar) {
		BuscarResponse devolver = new BuscarResponse();

		devolver.setOut(false);

		try {
			// Conectamos a la bd
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			java.sql.Connection con = DriverManager.getConnection("jdbc:mysql://localhost/ievents", "root", "");

			java.sql.Statement st = con.createStatement();

			// Buscamos el id del cliente
			String sql = "SELECT idCliente FROM cliente WHERE email like '" + buscar.getEmail() + "'";

			ResultSet rs = st.executeQuery(sql);

			if (rs.next()) { // Si se ha encontrado devolvemos true
				devolver.setOut(true);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return devolver;
	}

	/**
	 * Inserta un nuevo cliente en la bd con los datos solicitados y devuelve el
	 * id del cliente recien creado, o -1 en caso de error
	 * 
	 * @param crear
	 * @return crearResponse
	 */
	public CrearClienteResponse crearCliente(CrearCliente crear) {

		CrearClienteResponse devolver = new CrearClienteResponse();

		devolver.setOut(false);

		try {
			// Conectamos a la bd
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			java.sql.Connection con = DriverManager.getConnection("jdbc:mysql://localhost/ievents", "root", "");

			java.sql.Statement st = con.createStatement();

			// Buscamos el �ltimo id insertado para sumarle 1
			String sql = "SELECT idCliente FROM cliente ORDER BY idCliente DESC LIMIT 0,1";

			ResultSet rs = st.executeQuery(sql);

			int idClienteNuevo = 0;
			if (rs.next()) {
				idClienteNuevo = rs.getInt("idCliente");
			}
			
			idClienteNuevo++; //Aumentamos en uno el �ltimo id para poner el siguiente n�mero
			
			// Preparamos el insert con todos sus datos
			sql = "INSERT INTO cliente VALUES(" + idClienteNuevo + ",'" + crear.getNombre() + "','"
					+ crear.getApellido1() + "','" + crear.getApellido2() + "','" + crear.getEmail() + "','"
					+ crear.getDni() + "')";

			if (st.executeUpdate(sql) > 0) { // Si se ha creado correctamente,
												// devolvemos su Id
				devolver.setOut(true);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return devolver;
	}

}
