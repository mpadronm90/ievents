
/**
 * EventoSkeleton.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.3  Built on : Jun 27, 2015 (11:17:49 BST)
 */
package iEvents.skeleton;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.util.Calendar;

import iEvents.servicios.Evento.*;

/**
 * EventoSkeleton java skeleton for the axisService
 */
public class EventoSkeleton {

	/**
	 * Auto generated method signature
	 * 
	 * @param listarServiciosEvento
	 * @return listarServiciosEventoResponse
	 */
	public ListarServiciosEventoResponse listarServiciosEvento(ListarServiciosEvento listarServiciosEvento) {
		ListarServiciosEventoResponse devolver = new ListarServiciosEventoResponse();

		int idEvento = listarServiciosEvento.getIdEvento();

		try {
			// Conectamos a la bd
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			java.sql.Connection con = DriverManager.getConnection("jdbc:mysql://localhost/ievents", "root", "");

			java.sql.Statement st = con.createStatement();

			// Buscamos los servicios que hay que contratar para el evento
			String sql = "SELECT idServicio, descripcion, nombre FROM servicio WHERE idServicio IN (SELECT Servicio_idServicio FROM `servicio_has_contrato` WHERE Contrato_idContrato IN (SELECT Contrato_idContrato FROM evento WHERE idEvento = '"
					+ idEvento + "'))";
			ResultSet rs = st.executeQuery(sql);

			while (rs.next()) { // Para cada servicio
				// Recogemos los valores de la db
				TypeServicio tmp = new TypeServicio();
				tmp.setIdServicio(rs.getInt("idServicio"));
				tmp.setDescripcion(rs.getString("descripcion"));
				tmp.setNombre(rs.getString("nombre"));

				// A�adimos el patrocinado al array
				devolver.addOut(tmp);
			}

			con.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return devolver;
	}

	/**
	 * Auto generated method signature
	 * 
	 * @param disminuirAforo
	 * @return disminuirAforoResponse
	 */
	public DisminuirAforoResponse disminuirAforo(DisminuirAforo disminuirAforo) {
		DisminuirAforoResponse devolver = new DisminuirAforoResponse();

		// Cogemos los par�metros para trabajar m�s comodamente
		int idEvento = disminuirAforo.getIdEvento();
		int cantidad = disminuirAforo.getCantidad();

		// Suponemos que hay error como mensaje de salida
		// Si no es as� lo cambiamos despu�s del update
		devolver.setOut("No se ha podido reducir el aforo en " + cantidad + "' para el evento " + idEvento
				+ ", por favor, contacte con el administrador.");
		try {
			// Conectamos a la bd
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			java.sql.Connection con = DriverManager.getConnection("jdbc:mysql://localhost/ievents", "root", "");

			java.sql.Statement st = con.createStatement();

			// Buscamos el aforo actual
			String sql = "SELECT aforo FROM evento WHERE idEvento='" + idEvento + "'";
			ResultSet rs = st.executeQuery(sql);

			if (rs.next()) {
				int aforoActual = rs.getInt("aforo");
				// Restamos al aforo actual las unidades
				int aforoNuevo = aforoActual - cantidad;

				// Actualizamos en la db
				sql = "UPDATE evento SET aforo='" + aforoNuevo + "' WHERE idEvento='" + idEvento + "'";
				if (st.executeUpdate(sql) > 0) { // Si se ha modificado la fila
					devolver.setOut("El aforo del evento " + idEvento + " se ha disminuido en " + cantidad
							+ " plazas y actualmente est� en " + aforoNuevo + " plazas.");
				}
			}

			con.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return devolver;
	}

	/**
	 * Auto generated method signature
	 * 
	 * @param comprobarAforo
	 * @return comprobarAforoResponse
	 */
	public ComprobarAforoResponse comprobarAforo(ComprobarAforo comprobarAforo) {
		ComprobarAforoResponse devolver = new ComprobarAforoResponse();
		int idEvento = comprobarAforo.getIdEvento();
		int cantidad = comprobarAforo.getCantidad();
		// Inicializamos la variable de salida por si hay error con la db
		devolver.setOut(false);
		try {
			// Conectamos a la bd
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			java.sql.Connection con = DriverManager.getConnection("jdbc:mysql://localhost/ievents", "root", "");

			java.sql.Statement st = con.createStatement();

			// Buscamos el aforo disponible
			String sql = "SELECT aforo FROM evento WHERE idEvento='" + idEvento + "'";
			ResultSet rs = st.executeQuery(sql);
			if (rs.next()) { // Movemos el cursor del resultset a la primera
								// fila
				int aforoDisponible = rs.getInt("aforo");
				// Asignamos el resultado de si la cantidad de entradas
				// solicitada menor o igual que el aforo disponible
				devolver.setOut(cantidad <= aforoDisponible);
			}
			con.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return devolver;
	}

	/**
	 * Auto generated method signature
	 * 
	 * @param cambiarEstado
	 * @return cambiarEstadoResponse
	 */
	public CambiarEstadoResponse cambiarEstado(CambiarEstado cambiarEstado) {
		CambiarEstadoResponse devolver = new CambiarEstadoResponse();
		// Suponemos que hay error como mensaje de salida
		// Si no es as� lo cambiamos despu�s del update
		devolver.setOut(false);

		int idEvento = cambiarEstado.getIdEvento();
		int nuevoEstado = cambiarEstado.getNuevoEstado().getEstado();
		try {
			// Conectamos a la bd
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			java.sql.Connection con = DriverManager.getConnection("jdbc:mysql://localhost/ievents", "root", "");

			java.sql.Statement st = con.createStatement();

			// Actualizamos el estado del evento
			String sql = "UPDATE evento SET EstadoEvento_idEstadoEvento='" + nuevoEstado + "' WHERE idEvento='"
					+ idEvento + "'";
			if (st.executeUpdate(sql) > 0) { // Si se ha modificado la fila
				devolver.setOut(true);
			}
			con.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return devolver;
	}

	/**
	 * Auto generated method signature
	 * 
	 * @param crearEvento
	 * @return crearEventoResponse
	 */
	public CrearEventoResponse crearEvento(CrearEvento evento) {
		// Creamos el objeto que devolveremos
		CrearEventoResponse devolver = new CrearEventoResponse();

		// Guardamos los par�metros para trabajar m�s comodamente
		int idEvento = 1;
		int idContrato = evento.getIdContrato();
		double precioEntrada = 0;
		int aforo = 0;
		int idEstadoEvento = 1;
		int idEstadoContrato = 0;
		java.sql.Date fechaInicioEvento = null;
		java.sql.Date fechaFinEvento = null;
		boolean insertCorrecto = false;

		try {
			// Conectamos a la bd
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			java.sql.Connection con = DriverManager.getConnection("jdbc:mysql://localhost/ievents", "root", "");

			java.sql.Statement st = con.createStatement();

			// Buscamos los datos del contrato especificado
			String sql = "SELECT fechaInicioEvento, fechaFinEvento, Aforo, EstadoContrato_idEstadoContrato FROM contrato WHERE idContrato = "
					+ idContrato;
			System.out.println(sql);
			ResultSet rs = st.executeQuery(sql);
			if (rs.next()) {
				fechaInicioEvento = rs.getDate("fechaInicioEvento");
				fechaFinEvento = rs.getDate("fechaFinEvento");
				aforo = rs.getInt("Aforo");
				idEstadoContrato = rs.getInt("EstadoContrato_idEstadoContrato");
			}

			if (idEstadoContrato != 3) { // S�lo permitimos crear el contrato si
											// �ste est� en estado aceptado
				devolver.setOut("No se ha creado el evento. Solo se pueden crear eventos a partir de contratos aceptados.");
				return devolver;
			} else {
				// Buscamos el �ltimo id que asignamos a un evento para asignar
				// el
				// siguiente
				sql = ("SELECT idEvento FROM evento order by idEvento DESC limit 0,1");
				rs = st.executeQuery(sql);
				int nextId = 0;
				if (rs.next()) {
					nextId = rs.getInt("idEvento"); // Cogemos el id y le
													// sumamos 1
				}

				idEvento = ++nextId;

				// Preparamos el insert
				sql = "INSERT INTO evento VALUES ('" + nextId + "','" + precioEntrada + "','" + aforo + "','"
						+ idContrato + "','" + idEstadoEvento + "','" + fechaInicioEvento + "','" + fechaFinEvento
						+ "')";
				System.out.println(sql);
				// Ejecutamos la consulta y comprobamos su resultado
				if (st.executeUpdate(sql) > 0) { // Si es mayor que 0 significa
					insertCorrecto = true; // que se ha insertado correctamente
				}

			}
			con.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		// Si se ha insertado correctamente el evento en la bd
		if (insertCorrecto) {
			devolver.setOut(
					"El evento del contrato " + idContrato + " se ha creado correctamente con id:" + idEvento + ".");
		} else {
			devolver.setOut("Ocurri�n un error creando el evento a partir del contrato " + idContrato
					+ ". Por favor, contacte con su administrador");
		}

		return devolver;
	}

	/**
	 * Auto generated method signature
	 * 
	 * @param limpiarPatrocinadoresPosibles
	 * @return limpiarPatrocinadoresPosiblesResponse
	 */
	public LimpiarPatrocinadoresPosiblesResponse limpiarPatrocinadoresPosibles(
			LimpiarPatrocinadoresPosibles limpiarPatrocinadoresPosibles) {
		LimpiarPatrocinadoresPosiblesResponse devolver = new LimpiarPatrocinadoresPosiblesResponse();

		// Suponemos que hay error como mensaje de salida
		// Si no es as� lo cambiamos despu�s del update
		devolver.setResult(false);
		try {
			// Conectamos a la bd
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			java.sql.Connection con = DriverManager.getConnection("jdbc:mysql://localhost/ievents", "root", "");

			java.sql.Statement st = con.createStatement();
			String sql = "DELETE FROM contrato_has_patrocinador WHERE Contrato_idContrato = "
					+ limpiarPatrocinadoresPosibles.getIdContrato();
			if (st.executeUpdate(sql) > 0) { // Si se ha eliminado
				devolver.setResult(true);
			}
			con.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return devolver;
	}

	/**
	 * Auto generated method signature
	 * 
	 * @param fijarPrecioEntradas
	 * @return fijarPrecioEntradasResponse
	 */
	public FijarPrecioEntradasResponse fijarPrecioEntradas(FijarPrecioEntradas fijarPrecioEntradas) {
		FijarPrecioEntradasResponse devolver = new FijarPrecioEntradasResponse();

		// Cogemos los par�metros para trabajar m�s comodamente
		int idEvento = fijarPrecioEntradas.getIdEvento();
		double nuevoPrecio = fijarPrecioEntradas.getPrecio();

		// Suponemos que hay error como mensaje de salida
		// Si no es as� lo cambiamos despu�s del update
		devolver.setOut("No se ha podido cambiar el precio del evento " + idEvento
				+ ", por favor, contacte con el administrador.");

		try {
			// Conectamos a la bd
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			java.sql.Connection con = DriverManager.getConnection("jdbc:mysql://localhost/ievents", "root", "");

			java.sql.Statement st = con.createStatement();

			// Buscamos el �ltimo id que asignamos a un evento para asignar el
			// siguiente
			String sql = "UPDATE evento SET precioEntrada='" + nuevoPrecio + "' WHERE idEvento='" + idEvento + "'";
			if (st.executeUpdate(sql) > 0) { // Si se ha modificado la fila
				devolver.setOut("Las entradas para el evento " + idEvento + " cuestan " + nuevoPrecio);
			}
			con.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return devolver;
	}

	public ListarEventosAbiertosResponse listarEventosAbiertos(ListarEventosAbiertos wrappedParam) {

		ListarEventosAbiertosResponse devolver = new ListarEventosAbiertosResponse();
		try {
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			java.sql.Connection con = DriverManager.getConnection("jdbc:mysql://localhost/ievents", "root", "");

			java.sql.Statement st = con.createStatement();

			String sql = ("SELECT evento.idEvento, " + "evento.fechaInicio, " + "evento.fechaCierre, "
					+ "evento.precioEntrada, " + "evento.aforo, " + "emplazamiento.Direccion, "
					+ "contrato.descripcion " + "FROM evento "
					+ "left join contrato on contrato.idContrato = evento.Contrato_idContrato "
					+ "left join emplazamiento on emplazamiento.idEmplazamiento = contrato.Emplazamiento_idEmplazamiento "
					+ "where EstadoEvento_idEstadoEvento = 2");
			ResultSet rs = st.executeQuery(sql);
			rs = st.executeQuery(sql);

			while (rs.next()) {

				EventoType evento = new EventoType();
				evento.setIdEvento(rs.getInt("idEvento"));
				evento.setFechaIni(rs.getDate("fechaInicio"));
				evento.setFechaFin(rs.getDate("fechaCierre"));
				evento.setPrecioEntrada(rs.getDouble("precioEntrada"));
				evento.setAforo(rs.getInt("aforo"));
				evento.setDireccionEmplazamiento(rs.getString("Direccion"));
				evento.setDescripcion(rs.getString("descripcion"));

				devolver.addEvento(evento);
			}
			con.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return devolver;
	}

	/**
	 * Auto generated method signature
	 * 
	 * @param datosEvento
	 * @return datosEventoResponse
	 */
	public DatosEventoResponse datosEvento(DatosEvento entrada) {
		
		DatosEventoResponse devolver = new DatosEventoResponse();
		try {
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			java.sql.Connection con = DriverManager.getConnection("jdbc:mysql://localhost/ievents", "root", "");

			java.sql.Statement st = con.createStatement();
			
			String sql = ("SELECT ev.idEvento, ev.aforo, ev.Contrato_idContrato, ev.EstadoEvento_idEstadoEvento, ev.fechaInicio, ev.fechaCierre, co.fechaCreacionContrato, co.Cliente_idCliente, co.EstadoContrato_idEstadoContrato, co.Emplazamiento_idEmplazamiento,co.descripcion, ev.precioEntrada, co.urlImagen FROM evento ev INNER JOIN contrato co ON ev.Contrato_idContrato = co.idContrato WHERE idEvento = " + entrada.getIdEvento());
			ResultSet rs = st.executeQuery(sql);
			rs = st.executeQuery(sql);
			
			if(rs.next()){
				TypeEventDTO temp = new TypeEventDTO();
				
				temp.setIdEvento(rs.getInt("idEvento"));
				temp.setPrecioEntrada(rs.getDouble("precioEntrada"));
				temp.setContrato_idContrato(rs.getInt("Contrato_idContrato"));
				temp.setEstadoEvento_idEstadoEvento(rs.getInt("EstadoEvento_idEstadoEvento"));
				temp.setFechaInicio(rs.getDate("fechaInicio"));
				temp.setFechaFin(rs.getDate("fechaCierre"));
				temp.setFechaCreacionContrato(rs.getDate("fechaCreacionContrato"));
				temp.setCliente_idCliente(rs.getInt("Cliente_idCliente"));
				temp.setEstadoContrato_idEstadoContrato(rs.getInt("EstadoContrato_idEstadoContrato"));
				temp.setEmplazamiento_idEmplazamiento(rs.getInt("Emplazamiento_idEmplazamiento"));
				temp.setAforo(rs.getInt("aforo"));
				temp.setDescripcion(rs.getString("descripcion"));
				temp.setUrlImagen(rs.getString("urlImagen"));
				devolver.setOut(temp);
			}
			con.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return devolver;
	}

	public ObtenerAforoResponse obtenerAforo(ObtenerAforo obtenerAforo) {
		int idEvento=obtenerAforo.getIdEvento();
		ObtenerAforoResponse respuesta=new ObtenerAforoResponse();
		try {
			// Conectamos a la bd
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			java.sql.Connection con = DriverManager.getConnection("jdbc:mysql://localhost/ievents", "root", "");
		
			java.sql.Statement st = con.createStatement();
		
			// Buscamos los servicios que hay que contratar para el evento
			String sql = "SELECT aforo FROM evento WHERE idEvento="+idEvento;
			ResultSet rs = st.executeQuery(sql);
		
			if (rs.next()) { // Para cada servicio
				respuesta.setAforo(rs.getInt("aforo"));
			}
		
			con.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return respuesta;
	}

}