
/**
 * EventoMessageReceiverInOut.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.3  Built on : Jun 27, 2015 (11:17:49 BST)
 */
        package iEvents.skeleton;

        /**
        *  EventoMessageReceiverInOut message receiver
        */

        public class EventoMessageReceiverInOut extends org.apache.axis2.receivers.AbstractInOutMessageReceiver{


        public void invokeBusinessLogic(org.apache.axis2.context.MessageContext msgContext, org.apache.axis2.context.MessageContext newMsgContext)
        throws org.apache.axis2.AxisFault{

        try {

        // get the implementation class for the Web Service
        Object obj = getTheImplementationObject(msgContext);

        EventoSkeleton skel = (EventoSkeleton)obj;
        //Out Envelop
        org.apache.axiom.soap.SOAPEnvelope envelope = null;
        //Find the axisOperation that has been set by the Dispatch phase.
        org.apache.axis2.description.AxisOperation op = msgContext.getOperationContext().getAxisOperation();
        if (op == null) {
        throw new org.apache.axis2.AxisFault("Operation is not located, if this is doclit style the SOAP-ACTION should specified via the SOAP Action to use the RawXMLProvider");
        }

        java.lang.String methodName;
        if((op.getName() != null) && ((methodName = org.apache.axis2.util.JavaUtils.xmlNameToJavaIdentifier(op.getName().getLocalPart())) != null)){


        

            if("listarServiciosEvento".equals(methodName)){
                
                iEvents.servicios.Evento.ListarServiciosEventoResponse listarServiciosEventoResponse1 = null;
	                        iEvents.servicios.Evento.ListarServiciosEvento wrappedParam =
                                                             (iEvents.servicios.Evento.ListarServiciosEvento)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    iEvents.servicios.Evento.ListarServiciosEvento.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               listarServiciosEventoResponse1 =
                                                   
                                                   
                                                         skel.listarServiciosEvento(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), listarServiciosEventoResponse1, false, new javax.xml.namespace.QName("http://www.example.org/Evento/",
                                                    "listarServiciosEvento"));
                                    } else 

            if("disminuirAforo".equals(methodName)){
                
                iEvents.servicios.Evento.DisminuirAforoResponse disminuirAforoResponse3 = null;
	                        iEvents.servicios.Evento.DisminuirAforo wrappedParam =
                                                             (iEvents.servicios.Evento.DisminuirAforo)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    iEvents.servicios.Evento.DisminuirAforo.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               disminuirAforoResponse3 =
                                                   
                                                   
                                                         skel.disminuirAforo(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), disminuirAforoResponse3, false, new javax.xml.namespace.QName("http://www.example.org/Evento/",
                                                    "disminuirAforo"));
                                    } else 

            if("comprobarAforo".equals(methodName)){
                
                iEvents.servicios.Evento.ComprobarAforoResponse comprobarAforoResponse5 = null;
	                        iEvents.servicios.Evento.ComprobarAforo wrappedParam =
                                                             (iEvents.servicios.Evento.ComprobarAforo)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    iEvents.servicios.Evento.ComprobarAforo.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               comprobarAforoResponse5 =
                                                   
                                                   
                                                         skel.comprobarAforo(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), comprobarAforoResponse5, false, new javax.xml.namespace.QName("http://www.example.org/Evento/",
                                                    "comprobarAforo"));
                                    } else 

            if("datosEvento".equals(methodName)){
                
                iEvents.servicios.Evento.DatosEventoResponse datosEventoResponse7 = null;
	                        iEvents.servicios.Evento.DatosEvento wrappedParam =
                                                             (iEvents.servicios.Evento.DatosEvento)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    iEvents.servicios.Evento.DatosEvento.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               datosEventoResponse7 =
                                                   
                                                   
                                                         skel.datosEvento(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), datosEventoResponse7, false, new javax.xml.namespace.QName("http://www.example.org/Evento/",
                                                    "datosEvento"));
                                    } else 

            if("listarEventosAbiertos".equals(methodName)){
                
                iEvents.servicios.Evento.ListarEventosAbiertosResponse listarEventosAbiertosResponse9 = null;
	                        iEvents.servicios.Evento.ListarEventosAbiertos wrappedParam =
                                                             (iEvents.servicios.Evento.ListarEventosAbiertos)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    iEvents.servicios.Evento.ListarEventosAbiertos.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               listarEventosAbiertosResponse9 =
                                                   
                                                   
                                                         skel.listarEventosAbiertos(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), listarEventosAbiertosResponse9, false, new javax.xml.namespace.QName("http://www.example.org/Evento/",
                                                    "listarEventosAbiertos"));
                                    } else 

            if("cambiarEstado".equals(methodName)){
                
                iEvents.servicios.Evento.CambiarEstadoResponse cambiarEstadoResponse11 = null;
	                        iEvents.servicios.Evento.CambiarEstado wrappedParam =
                                                             (iEvents.servicios.Evento.CambiarEstado)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    iEvents.servicios.Evento.CambiarEstado.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               cambiarEstadoResponse11 =
                                                   
                                                   
                                                         skel.cambiarEstado(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), cambiarEstadoResponse11, false, new javax.xml.namespace.QName("http://www.example.org/Evento/",
                                                    "cambiarEstado"));
                                    } else 

            if("obtenerAforo".equals(methodName)){
                
                iEvents.servicios.Evento.ObtenerAforoResponse obtenerAforoResponse13 = null;
	                        iEvents.servicios.Evento.ObtenerAforo wrappedParam =
                                                             (iEvents.servicios.Evento.ObtenerAforo)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    iEvents.servicios.Evento.ObtenerAforo.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               obtenerAforoResponse13 =
                                                   
                                                   
                                                         skel.obtenerAforo(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), obtenerAforoResponse13, false, new javax.xml.namespace.QName("http://www.example.org/Evento/",
                                                    "obtenerAforo"));
                                    } else 

            if("crearEvento".equals(methodName)){
                
                iEvents.servicios.Evento.CrearEventoResponse crearEventoResponse15 = null;
	                        iEvents.servicios.Evento.CrearEvento wrappedParam =
                                                             (iEvents.servicios.Evento.CrearEvento)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    iEvents.servicios.Evento.CrearEvento.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               crearEventoResponse15 =
                                                   
                                                   
                                                         skel.crearEvento(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), crearEventoResponse15, false, new javax.xml.namespace.QName("http://www.example.org/Evento/",
                                                    "crearEvento"));
                                    } else 

            if("limpiarPatrocinadoresPosibles".equals(methodName)){
                
                iEvents.servicios.Evento.LimpiarPatrocinadoresPosiblesResponse limpiarPatrocinadoresPosiblesResponse17 = null;
	                        iEvents.servicios.Evento.LimpiarPatrocinadoresPosibles wrappedParam =
                                                             (iEvents.servicios.Evento.LimpiarPatrocinadoresPosibles)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    iEvents.servicios.Evento.LimpiarPatrocinadoresPosibles.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               limpiarPatrocinadoresPosiblesResponse17 =
                                                   
                                                   
                                                         skel.limpiarPatrocinadoresPosibles(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), limpiarPatrocinadoresPosiblesResponse17, false, new javax.xml.namespace.QName("http://www.example.org/Evento/",
                                                    "limpiarPatrocinadoresPosibles"));
                                    } else 

            if("fijarPrecioEntradas".equals(methodName)){
                
                iEvents.servicios.Evento.FijarPrecioEntradasResponse fijarPrecioEntradasResponse19 = null;
	                        iEvents.servicios.Evento.FijarPrecioEntradas wrappedParam =
                                                             (iEvents.servicios.Evento.FijarPrecioEntradas)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    iEvents.servicios.Evento.FijarPrecioEntradas.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               fijarPrecioEntradasResponse19 =
                                                   
                                                   
                                                         skel.fijarPrecioEntradas(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), fijarPrecioEntradasResponse19, false, new javax.xml.namespace.QName("http://www.example.org/Evento/",
                                                    "fijarPrecioEntradas"));
                                    
            } else {
              throw new java.lang.RuntimeException("method not found");
            }
        

        newMsgContext.setEnvelope(envelope);
        }
        }
        catch (java.lang.Exception e) {
        throw org.apache.axis2.AxisFault.makeFault(e);
        }
        }
        
        //
            private  org.apache.axiom.om.OMElement  toOM(iEvents.servicios.Evento.ListarServiciosEvento param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(iEvents.servicios.Evento.ListarServiciosEvento.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(iEvents.servicios.Evento.ListarServiciosEventoResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(iEvents.servicios.Evento.ListarServiciosEventoResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(iEvents.servicios.Evento.DisminuirAforo param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(iEvents.servicios.Evento.DisminuirAforo.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(iEvents.servicios.Evento.DisminuirAforoResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(iEvents.servicios.Evento.DisminuirAforoResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(iEvents.servicios.Evento.ComprobarAforo param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(iEvents.servicios.Evento.ComprobarAforo.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(iEvents.servicios.Evento.ComprobarAforoResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(iEvents.servicios.Evento.ComprobarAforoResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(iEvents.servicios.Evento.DatosEvento param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(iEvents.servicios.Evento.DatosEvento.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(iEvents.servicios.Evento.DatosEventoResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(iEvents.servicios.Evento.DatosEventoResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(iEvents.servicios.Evento.ListarEventosAbiertos param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(iEvents.servicios.Evento.ListarEventosAbiertos.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(iEvents.servicios.Evento.ListarEventosAbiertosResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(iEvents.servicios.Evento.ListarEventosAbiertosResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(iEvents.servicios.Evento.CambiarEstado param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(iEvents.servicios.Evento.CambiarEstado.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(iEvents.servicios.Evento.CambiarEstadoResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(iEvents.servicios.Evento.CambiarEstadoResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(iEvents.servicios.Evento.ObtenerAforo param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(iEvents.servicios.Evento.ObtenerAforo.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(iEvents.servicios.Evento.ObtenerAforoResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(iEvents.servicios.Evento.ObtenerAforoResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(iEvents.servicios.Evento.CrearEvento param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(iEvents.servicios.Evento.CrearEvento.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(iEvents.servicios.Evento.CrearEventoResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(iEvents.servicios.Evento.CrearEventoResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(iEvents.servicios.Evento.LimpiarPatrocinadoresPosibles param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(iEvents.servicios.Evento.LimpiarPatrocinadoresPosibles.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(iEvents.servicios.Evento.LimpiarPatrocinadoresPosiblesResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(iEvents.servicios.Evento.LimpiarPatrocinadoresPosiblesResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(iEvents.servicios.Evento.FijarPrecioEntradas param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(iEvents.servicios.Evento.FijarPrecioEntradas.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(iEvents.servicios.Evento.FijarPrecioEntradasResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(iEvents.servicios.Evento.FijarPrecioEntradasResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, iEvents.servicios.Evento.ListarServiciosEventoResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(iEvents.servicios.Evento.ListarServiciosEventoResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private iEvents.servicios.Evento.ListarServiciosEventoResponse wraplistarServiciosEvento(){
                                iEvents.servicios.Evento.ListarServiciosEventoResponse wrappedElement = new iEvents.servicios.Evento.ListarServiciosEventoResponse();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, iEvents.servicios.Evento.DisminuirAforoResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(iEvents.servicios.Evento.DisminuirAforoResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private iEvents.servicios.Evento.DisminuirAforoResponse wrapdisminuirAforo(){
                                iEvents.servicios.Evento.DisminuirAforoResponse wrappedElement = new iEvents.servicios.Evento.DisminuirAforoResponse();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, iEvents.servicios.Evento.ComprobarAforoResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(iEvents.servicios.Evento.ComprobarAforoResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private iEvents.servicios.Evento.ComprobarAforoResponse wrapcomprobarAforo(){
                                iEvents.servicios.Evento.ComprobarAforoResponse wrappedElement = new iEvents.servicios.Evento.ComprobarAforoResponse();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, iEvents.servicios.Evento.DatosEventoResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(iEvents.servicios.Evento.DatosEventoResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private iEvents.servicios.Evento.DatosEventoResponse wrapdatosEvento(){
                                iEvents.servicios.Evento.DatosEventoResponse wrappedElement = new iEvents.servicios.Evento.DatosEventoResponse();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, iEvents.servicios.Evento.ListarEventosAbiertosResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(iEvents.servicios.Evento.ListarEventosAbiertosResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private iEvents.servicios.Evento.ListarEventosAbiertosResponse wraplistarEventosAbiertos(){
                                iEvents.servicios.Evento.ListarEventosAbiertosResponse wrappedElement = new iEvents.servicios.Evento.ListarEventosAbiertosResponse();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, iEvents.servicios.Evento.CambiarEstadoResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(iEvents.servicios.Evento.CambiarEstadoResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private iEvents.servicios.Evento.CambiarEstadoResponse wrapcambiarEstado(){
                                iEvents.servicios.Evento.CambiarEstadoResponse wrappedElement = new iEvents.servicios.Evento.CambiarEstadoResponse();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, iEvents.servicios.Evento.ObtenerAforoResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(iEvents.servicios.Evento.ObtenerAforoResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private iEvents.servicios.Evento.ObtenerAforoResponse wrapobtenerAforo(){
                                iEvents.servicios.Evento.ObtenerAforoResponse wrappedElement = new iEvents.servicios.Evento.ObtenerAforoResponse();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, iEvents.servicios.Evento.CrearEventoResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(iEvents.servicios.Evento.CrearEventoResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private iEvents.servicios.Evento.CrearEventoResponse wrapcrearEvento(){
                                iEvents.servicios.Evento.CrearEventoResponse wrappedElement = new iEvents.servicios.Evento.CrearEventoResponse();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, iEvents.servicios.Evento.LimpiarPatrocinadoresPosiblesResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(iEvents.servicios.Evento.LimpiarPatrocinadoresPosiblesResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private iEvents.servicios.Evento.LimpiarPatrocinadoresPosiblesResponse wraplimpiarPatrocinadoresPosibles(){
                                iEvents.servicios.Evento.LimpiarPatrocinadoresPosiblesResponse wrappedElement = new iEvents.servicios.Evento.LimpiarPatrocinadoresPosiblesResponse();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, iEvents.servicios.Evento.FijarPrecioEntradasResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(iEvents.servicios.Evento.FijarPrecioEntradasResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private iEvents.servicios.Evento.FijarPrecioEntradasResponse wrapfijarPrecioEntradas(){
                                iEvents.servicios.Evento.FijarPrecioEntradasResponse wrappedElement = new iEvents.servicios.Evento.FijarPrecioEntradasResponse();
                                return wrappedElement;
                         }
                    


        /**
        *  get the default envelope
        */
        private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory){
        return factory.getDefaultEnvelope();
        }


        private  java.lang.Object fromOM(
        org.apache.axiom.om.OMElement param,
        java.lang.Class type,
        java.util.Map extraNamespaces) throws org.apache.axis2.AxisFault{

        try {
        
                if (iEvents.servicios.Evento.CambiarEstado.class.equals(type)){
                
                        return iEvents.servicios.Evento.CambiarEstado.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (iEvents.servicios.Evento.CambiarEstadoResponse.class.equals(type)){
                
                        return iEvents.servicios.Evento.CambiarEstadoResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (iEvents.servicios.Evento.ComprobarAforo.class.equals(type)){
                
                        return iEvents.servicios.Evento.ComprobarAforo.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (iEvents.servicios.Evento.ComprobarAforoResponse.class.equals(type)){
                
                        return iEvents.servicios.Evento.ComprobarAforoResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (iEvents.servicios.Evento.CrearEvento.class.equals(type)){
                
                        return iEvents.servicios.Evento.CrearEvento.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (iEvents.servicios.Evento.CrearEventoResponse.class.equals(type)){
                
                        return iEvents.servicios.Evento.CrearEventoResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (iEvents.servicios.Evento.DatosEvento.class.equals(type)){
                
                        return iEvents.servicios.Evento.DatosEvento.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (iEvents.servicios.Evento.DatosEventoResponse.class.equals(type)){
                
                        return iEvents.servicios.Evento.DatosEventoResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (iEvents.servicios.Evento.DisminuirAforo.class.equals(type)){
                
                        return iEvents.servicios.Evento.DisminuirAforo.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (iEvents.servicios.Evento.DisminuirAforoResponse.class.equals(type)){
                
                        return iEvents.servicios.Evento.DisminuirAforoResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (iEvents.servicios.Evento.FijarPrecioEntradas.class.equals(type)){
                
                        return iEvents.servicios.Evento.FijarPrecioEntradas.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (iEvents.servicios.Evento.FijarPrecioEntradasResponse.class.equals(type)){
                
                        return iEvents.servicios.Evento.FijarPrecioEntradasResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (iEvents.servicios.Evento.LimpiarPatrocinadoresPosibles.class.equals(type)){
                
                        return iEvents.servicios.Evento.LimpiarPatrocinadoresPosibles.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (iEvents.servicios.Evento.LimpiarPatrocinadoresPosiblesResponse.class.equals(type)){
                
                        return iEvents.servicios.Evento.LimpiarPatrocinadoresPosiblesResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (iEvents.servicios.Evento.ListarEventosAbiertos.class.equals(type)){
                
                        return iEvents.servicios.Evento.ListarEventosAbiertos.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (iEvents.servicios.Evento.ListarEventosAbiertosResponse.class.equals(type)){
                
                        return iEvents.servicios.Evento.ListarEventosAbiertosResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (iEvents.servicios.Evento.ListarServiciosEvento.class.equals(type)){
                
                        return iEvents.servicios.Evento.ListarServiciosEvento.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (iEvents.servicios.Evento.ListarServiciosEventoResponse.class.equals(type)){
                
                        return iEvents.servicios.Evento.ListarServiciosEventoResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (iEvents.servicios.Evento.ObtenerAforo.class.equals(type)){
                
                        return iEvents.servicios.Evento.ObtenerAforo.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (iEvents.servicios.Evento.ObtenerAforoResponse.class.equals(type)){
                
                        return iEvents.servicios.Evento.ObtenerAforoResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
        } catch (java.lang.Exception e) {
        throw org.apache.axis2.AxisFault.makeFault(e);
        }
           return null;
        }



    

        /**
        *  A utility method that copies the namepaces from the SOAPEnvelope
        */
        private java.util.Map getEnvelopeNamespaces(org.apache.axiom.soap.SOAPEnvelope env){
        java.util.Map returnMap = new java.util.HashMap();
        java.util.Iterator namespaceIterator = env.getAllDeclaredNamespaces();
        while (namespaceIterator.hasNext()) {
        org.apache.axiom.om.OMNamespace ns = (org.apache.axiom.om.OMNamespace) namespaceIterator.next();
        returnMap.put(ns.getPrefix(),ns.getNamespaceURI());
        }
        return returnMap;
        }

        private org.apache.axis2.AxisFault createAxisFault(java.lang.Exception e) {
        org.apache.axis2.AxisFault f;
        Throwable cause = e.getCause();
        if (cause != null) {
            f = new org.apache.axis2.AxisFault(e.getMessage(), cause);
        } else {
            f = new org.apache.axis2.AxisFault(e.getMessage());
        }

        return f;
    }

        }//end of class
    