/**
 * EmpleadoSOAPImpl.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.example.www.empleado;

import java.sql.DriverManager;
import java.sql.ResultSet;

public class EmpleadoSOAPImpl implements org.example.www.empleado.Empleado_PortType{
    public boolean contratar(int evento_id, int contrato_id, int empleado_id) throws java.rmi.RemoteException {
   
    	boolean result = true;
    	
    	try {
			// Conectamos a la bd
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			java.sql.Connection con = DriverManager.getConnection("jdbc:mysql://localhost/ievents", "root", "");

			java.sql.Statement st = con.createStatement();

			// Buscamos evento
			String sql = "SELECT idEvento FROM evento WHERE idEvento=" + Integer.toString(evento_id);
			ResultSet rs = st.executeQuery(sql);
			if (!rs.next()) { // Si se ha encontrado devolvemos true
				result = false;
			}		
			
			// Buscamos el id del empleado
			sql = "SELECT idEmpleado FROM empleado WHERE idEmpleado=" + Integer.toString(empleado_id);
			rs = st.executeQuery(sql);
			if (!rs.next()) { // Si se ha encontrado devolvemos true
				result = false;
			}
			
			// Preparamos el insert con todos sus datos
			sql = "INSERT INTO empleado_has_evento VALUES(" + empleado_id + ",'" + evento_id + "','"
					+ contrato_id +  "')";

			int number = st.executeUpdate(sql);
			//System.out.println(Integer.toString(number));
			if (number == 0) { 												
				result = false;
			}

		} catch (Exception e) {
			result = false;			
			System.out.println(e.getMessage());
		}
    	
    	
    	return result;
    }

}
