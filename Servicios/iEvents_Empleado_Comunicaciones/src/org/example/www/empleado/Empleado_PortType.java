/**
 * Empleado_PortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.example.www.empleado;

public interface Empleado_PortType extends java.rmi.Remote {
    public boolean contratar(int evento_id, int contrato_id, int empleado_id) throws java.rmi.RemoteException;
}
