/**
 * EmpleadoSOAPSkeleton.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.example.www.empleado;

public class EmpleadoSOAPSkeleton implements org.example.www.empleado.Empleado_PortType, org.apache.axis.wsdl.Skeleton {
    private org.example.www.empleado.Empleado_PortType impl;
    private static java.util.Map _myOperations = new java.util.Hashtable();
    private static java.util.Collection _myOperationsList = new java.util.ArrayList();

    /**
    * Returns List of OperationDesc objects with this name
    */
    public static java.util.List getOperationDescByName(java.lang.String methodName) {
        return (java.util.List)_myOperations.get(methodName);
    }

    /**
    * Returns Collection of OperationDescs
    */
    public static java.util.Collection getOperationDescs() {
        return _myOperationsList;
    }

    static {
        org.apache.axis.description.OperationDesc _oper;
        org.apache.axis.description.FaultDesc _fault;
        org.apache.axis.description.ParameterDesc [] _params;
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "evento_id"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"), int.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "contrato_id"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"), int.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "empleado_id"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"), int.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("contratar", _params, new javax.xml.namespace.QName("", "resultado"));
        _oper.setReturnType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        _oper.setElementQName(new javax.xml.namespace.QName("http://www.example.org/empleado/", "contratar"));
        _oper.setSoapAction("http://www.example.org/empleado/contratar");
        _myOperationsList.add(_oper);
        if (_myOperations.get("contratar") == null) {
            _myOperations.put("contratar", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("contratar")).add(_oper);
    }

    public EmpleadoSOAPSkeleton() {
        this.impl = new org.example.www.empleado.EmpleadoSOAPImpl();
    }

    public EmpleadoSOAPSkeleton(org.example.www.empleado.Empleado_PortType impl) {
        this.impl = impl;
    }
    public boolean contratar(int evento_id, int contrato_id, int empleado_id) throws java.rmi.RemoteException
    {
        boolean ret = impl.contratar(evento_id, contrato_id, empleado_id);
        return ret;
    }

}
