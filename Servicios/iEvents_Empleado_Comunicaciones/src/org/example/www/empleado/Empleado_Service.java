/**
 * Empleado_Service.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.example.www.empleado;

public interface Empleado_Service extends javax.xml.rpc.Service {
    public java.lang.String getempleadoSOAPAddress();

    public org.example.www.empleado.Empleado_PortType getempleadoSOAP() throws javax.xml.rpc.ServiceException;

    public org.example.www.empleado.Empleado_PortType getempleadoSOAP(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
