/**
 * Comunicaciones_Service.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.example.www.comunicaciones;

public interface Comunicaciones_Service extends javax.xml.rpc.Service {
    public java.lang.String getComunicacionesSOAPAddress();

    public org.example.www.comunicaciones.Comunicaciones_PortType getComunicacionesSOAP() throws javax.xml.rpc.ServiceException;

    public org.example.www.comunicaciones.Comunicaciones_PortType getComunicacionesSOAP(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
