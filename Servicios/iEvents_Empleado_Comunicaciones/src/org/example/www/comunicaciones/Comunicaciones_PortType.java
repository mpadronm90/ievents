/**
 * Comunicaciones_PortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.example.www.comunicaciones;

public interface Comunicaciones_PortType extends java.rmi.Remote {
    public boolean enviarComunicacionACliente(int id_Cliente, java.lang.String explicacion, java.lang.Integer id_Contrato) throws java.rmi.RemoteException;
    public boolean enviarPropuetaAPatrocinador(int id_Patrocinador, java.util.Date fechaIni, java.util.Date fechaFin, int id_Evento) throws java.rmi.RemoteException;
}
