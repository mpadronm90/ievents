/**
 * ComunicacionesSOAPSkeleton.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.example.www.comunicaciones;

public class ComunicacionesSOAPSkeleton implements org.example.www.comunicaciones.Comunicaciones_PortType, org.apache.axis.wsdl.Skeleton {
    private org.example.www.comunicaciones.Comunicaciones_PortType impl;
    private static java.util.Map _myOperations = new java.util.Hashtable();
    private static java.util.Collection _myOperationsList = new java.util.ArrayList();

    /**
    * Returns List of OperationDesc objects with this name
    */
    public static java.util.List getOperationDescByName(java.lang.String methodName) {
        return (java.util.List)_myOperations.get(methodName);
    }

    /**
    * Returns Collection of OperationDescs
    */
    public static java.util.Collection getOperationDescs() {
        return _myOperationsList;
    }

    static {
        org.apache.axis.description.OperationDesc _oper;
        org.apache.axis.description.FaultDesc _fault;
        org.apache.axis.description.ParameterDesc [] _params;
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "id_Cliente"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"), int.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "explicacion"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "id_Contrato"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"), java.lang.Integer.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("enviarComunicacionACliente", _params, new javax.xml.namespace.QName("", "out"));
        _oper.setReturnType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        _oper.setElementQName(new javax.xml.namespace.QName("http://www.example.org/comunicaciones/", "enviarComunicacionACliente"));
        _oper.setSoapAction("http://www.example.org/comunicaciones/enviarComunicacionACliente");
        _myOperationsList.add(_oper);
        if (_myOperations.get("enviarComunicacionACliente") == null) {
            _myOperations.put("enviarComunicacionACliente", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("enviarComunicacionACliente")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "id_Patrocinador"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"), int.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "fechaIni"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "date"), java.util.Date.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "fechaFin"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "date"), java.util.Date.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "id_Evento"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"), int.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("enviarPropuetaAPatrocinador", _params, new javax.xml.namespace.QName("", "out"));
        _oper.setReturnType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        _oper.setElementQName(new javax.xml.namespace.QName("http://www.example.org/comunicaciones/", "enviarPropuetaAPatrocinador"));
        _oper.setSoapAction("http://www.example.org/comunicaciones/enviarPropuetaAPatrocinador");
        _myOperationsList.add(_oper);
        if (_myOperations.get("enviarPropuetaAPatrocinador") == null) {
            _myOperations.put("enviarPropuetaAPatrocinador", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("enviarPropuetaAPatrocinador")).add(_oper);
    }

    public ComunicacionesSOAPSkeleton() {
        this.impl = new org.example.www.comunicaciones.ComunicacionesSOAPImpl();
    }

    public ComunicacionesSOAPSkeleton(org.example.www.comunicaciones.Comunicaciones_PortType impl) {
        this.impl = impl;
    }
    public boolean enviarComunicacionACliente(int id_Cliente, java.lang.String explicacion, java.lang.Integer id_Contrato) throws java.rmi.RemoteException
    {
        boolean ret = impl.enviarComunicacionACliente(id_Cliente, explicacion, id_Contrato);
        return ret;
    }

    public boolean enviarPropuetaAPatrocinador(int id_Patrocinador, java.util.Date fechaIni, java.util.Date fechaFin, int id_Evento) throws java.rmi.RemoteException
    {
        boolean ret = impl.enviarPropuetaAPatrocinador(id_Patrocinador, fechaIni, fechaFin, id_Evento);
        return ret;
    }

}
