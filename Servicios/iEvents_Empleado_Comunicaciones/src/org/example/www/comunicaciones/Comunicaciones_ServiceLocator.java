/**
 * Comunicaciones_ServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.example.www.comunicaciones;

public class Comunicaciones_ServiceLocator extends org.apache.axis.client.Service implements org.example.www.comunicaciones.Comunicaciones_Service {

    public Comunicaciones_ServiceLocator() {
    }


    public Comunicaciones_ServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public Comunicaciones_ServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for ComunicacionesSOAP
    private java.lang.String ComunicacionesSOAP_address = "http://www.example.org/";

    public java.lang.String getComunicacionesSOAPAddress() {
        return ComunicacionesSOAP_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String ComunicacionesSOAPWSDDServiceName = "ComunicacionesSOAP";

    public java.lang.String getComunicacionesSOAPWSDDServiceName() {
        return ComunicacionesSOAPWSDDServiceName;
    }

    public void setComunicacionesSOAPWSDDServiceName(java.lang.String name) {
        ComunicacionesSOAPWSDDServiceName = name;
    }

    public org.example.www.comunicaciones.Comunicaciones_PortType getComunicacionesSOAP() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(ComunicacionesSOAP_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getComunicacionesSOAP(endpoint);
    }

    public org.example.www.comunicaciones.Comunicaciones_PortType getComunicacionesSOAP(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            org.example.www.comunicaciones.ComunicacionesSOAPStub _stub = new org.example.www.comunicaciones.ComunicacionesSOAPStub(portAddress, this);
            _stub.setPortName(getComunicacionesSOAPWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setComunicacionesSOAPEndpointAddress(java.lang.String address) {
        ComunicacionesSOAP_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (org.example.www.comunicaciones.Comunicaciones_PortType.class.isAssignableFrom(serviceEndpointInterface)) {
                org.example.www.comunicaciones.ComunicacionesSOAPStub _stub = new org.example.www.comunicaciones.ComunicacionesSOAPStub(new java.net.URL(ComunicacionesSOAP_address), this);
                _stub.setPortName(getComunicacionesSOAPWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("ComunicacionesSOAP".equals(inputPortName)) {
            return getComunicacionesSOAP();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://www.example.org/comunicaciones/", "Comunicaciones");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://www.example.org/comunicaciones/", "ComunicacionesSOAP"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("ComunicacionesSOAP".equals(portName)) {
            setComunicacionesSOAPEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
