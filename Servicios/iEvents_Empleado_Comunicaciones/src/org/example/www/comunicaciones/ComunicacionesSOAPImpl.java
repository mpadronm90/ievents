/**
 * ComunicacionesSOAPImpl.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.example.www.comunicaciones;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class ComunicacionesSOAPImpl implements org.example.www.comunicaciones.Comunicaciones_PortType{
    public boolean enviarComunicacionACliente(int id_Cliente, java.lang.String explicacion, java.lang.Integer id_Contrato) throws java.rmi.RemoteException {
        
    	// crear una conexion a db
    	// obtener el cliente y el contrato
    	// Si el cliente tiene mail enviar correo
    	String cliente_nombre = "";
    	String cliente_email = null;
    	String contrato_desc = null;
    	String textoContrato = "";
    	
    	try {
			// Conectamos a la bd
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			java.sql.Connection con = DriverManager.getConnection("jdbc:mysql://localhost/ievents", "root", "");

			java.sql.Statement st = con.createStatement();

			// Buscamos el nombre del cliente
			String sql = "SELECT nombre, email FROM cliente WHERE idCliente=" + Integer.toString(id_Cliente);
			ResultSet rs = st.executeQuery(sql);
			if (rs.next()) { // Si se ha encontrado devolvemos true
				cliente_nombre = rs.getString("nombre");
				cliente_email = rs.getString("email");
			}
			
			// Buscamos el id del cliente
			sql = "SELECT descripcion FROM contrato WHERE idContrato=" + Integer.toString(id_Contrato);
			rs = st.executeQuery(sql);
			if (rs.next()) { // Si se ha encontrado devolvemos true
				contrato_desc = rs.getString("descripcion");
			}

		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
		}
    	
    	if (cliente_email != null) {
    		
    	
    	
    	Properties props = new Properties();
    	// Nombre del host de correo, es smtp.gmail.com
    	props.setProperty("mail.smtp.host", "smtp.gmail.com");
    	// TLS si está disponible
    	props.setProperty("mail.smtp.starttls.enable", "true");
    	// Puerto de gmail para envio de correos
    	props.setProperty("mail.smtp.port","587");
    	// Nombre del usuario
    	props.setProperty("mail.smtp.user", "ievents.comunicaciones@gmail.com");
    	// Si requiere o no usuario y password para conectarse.
    	props.setProperty("mail.smtp.auth", "true");
    	
    	Session session = Session.getInstance(props, new javax.mail.Authenticator() {
    	    protected PasswordAuthentication getPasswordAuthentication() {
    	        return new PasswordAuthentication("ievents.comunicaciones","comunicaciones");
    	    }
    	});    	
    	
    	session.setDebug(true);
    	
    	MimeMessage message = new MimeMessage(session);
    	
    	try {
    		
			message.setFrom(new InternetAddress("ievents.comunicaciones@gmail.com"));
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(cliente_email));
			
			message.setSubject("Comunicación iEvents");
			
			if(contrato_desc != null) {
    			textoContrato = "Referente a "+ contrato_desc+ ".\n\n";
    		}
			
			message.setText("Hola " + cliente_nombre+",\n\n" +
							
						  	 textoContrato +
						     explicacion+".\n\nSaludos.");
			
			Transport t = session.getTransport("smtp");
			t.connect();
			t.sendMessage(message,message.getAllRecipients());
			t.close();
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println(e.getMessage());
			return false;
		}
    	
    	return true;
    	}
    	
    	return false;
    }

    public boolean enviarPropuetaAPatrocinador(int id_Patrocinador, java.util.Date fechaIni, java.util.Date fechaFin, int id_Evento) throws java.rmi.RemoteException {
        
    	// crear una patrocinador a db
    	// obtener el patrocinador y el contrato
    	// Si el cliente tiene mail enviar correo
    	String patrocinador_nombre = "";
    	String patrocinador_email = null;
    	String emplazamiento = null;
    	String evento_desc = "";    	
    	String textoEvento = "";
    	
    	try {
			// Conectamos a la bd
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			java.sql.Connection con = DriverManager.getConnection("jdbc:mysql://localhost/ievents", "root", "");

			java.sql.Statement st = con.createStatement();

			// Buscamos el nombre del patrocinador
			String sql = "SELECT nombre, email FROM patrocinador WHERE idPatrocinador=" + Integer.toString(id_Patrocinador);
			ResultSet rs = st.executeQuery(sql);
			if (rs.next()) { // Si se ha encontrado devolvemos true
				patrocinador_nombre = rs.getString("nombre");
				patrocinador_email = rs.getString("email");
			}
			
			// Buscamos algunos datos
			sql = "SELECT nombre FROM emplazamiento WHERE idEmplazamiento in (SELECT Emplazamiento_idEmplazamiento FROM contrato WHERE idContrato in (SELECT Contrato_idContrato FROM evento WHERE idEvento = "+Integer.toString(id_Evento)+"))";
			rs = st.executeQuery(sql);
			if (rs.next()) { // Si se ha encontrado devolvemos true
				emplazamiento = rs.getString("nombre");
			}
			sql = "SELECT descripcion, aforo FROM contrato WHERE idContrato in (SELECT Contrato_idContrato FROM evento WHERE idEvento = "+Integer.toString(id_Evento)+"))";
			rs = st.executeQuery(sql);
			if (rs.next()) { // Si se ha encontrado devolvemos true
				emplazamiento = rs.getString("nombre");
				evento_desc =  rs.getString("descripcion");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
    	
    	if (patrocinador_email != null) {
    	
    	
    	Properties props = new Properties();
    	// Nombre del host de correo, es smtp.gmail.com
    	props.setProperty("mail.smtp.host", "smtp.gmail.com");
    	// TLS si está disponible
    	props.setProperty("mail.smtp.starttls.enable", "true");
    	// Puerto de gmail para envio de correos
    	props.setProperty("mail.smtp.port","587");
    	// Nombre del usuario
    	props.setProperty("mail.smtp.user", "ievents.comunicaciones@gmail.com");
    	// Si requiere o no usuario y password para conectarse.
    	props.setProperty("mail.smtp.auth", "true");
    	
    	Session session = Session.getInstance(props, new javax.mail.Authenticator() {
    	    protected PasswordAuthentication getPasswordAuthentication() {
    	        return new PasswordAuthentication("ievents.comunicaciones","comunicaciones");
    	    }
    	});    
    	session.setDebug(true);
    	
    	MimeMessage message = new MimeMessage(session);
    	
    	try {
			message.setFrom(new InternetAddress("ievents.comunicaciones@gmail.com"));
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(patrocinador_email));
			
			message.setSubject("Comunicación iEvents");
			
			if(emplazamiento != null) {
				emplazamiento = "lugar por determinar";
    		}
			
			if(evento_desc != null) {
				evento_desc = "";
    		}
			
			message.setText("Hola " + patrocinador_nombre+",\n\n" +
							
						  	 "Nos gustaría colaborar con usted en el evento "+evento_desc+ " que realizaremos pronto en "+emplazamiento+". Si está interesado su patrocinio sería desde fecha inicio "+fechaIni+ " a fecha fin "+ fechaFin + "si está interesado comuníquese con nosotros.\n\nSaludos.");
						
			
			Transport t = session.getTransport("smtp");
			t.connect();
			t.sendMessage(message,message.getAllRecipients());
			t.close();
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println(e.getMessage());
			return false;
		}
    	
    	return true;
    	}
    	
    	return false;
    }
    
    

}
