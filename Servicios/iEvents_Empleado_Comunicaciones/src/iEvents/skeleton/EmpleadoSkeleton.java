
/**
 * EmpleadoSkeleton.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.3  Built on : Jun 27, 2015 (11:17:49 BST)
 */
package iEvents.skeleton;

import java.sql.Date;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.util.ArrayList;

import iEvents.servicios.*;

/**
 * EmpleadoSkeleton java skeleton for the axisService
 */
public class EmpleadoSkeleton {

	/**
	 * Auto generated method signature
	 * 
	 * @param listarEmpleadosDisponibles
	 * @return listarEmpleadosDisponiblesResponse
	 */

	public iEvents.servicios.ListarEmpleadosDisponiblesResponse listarEmpleadosDisponibles(
			iEvents.servicios.ListarEmpleadosDisponibles listarEmpleadosDisponibles) {
		ListarEmpleadosDisponiblesResponse devolver = new ListarEmpleadosDisponiblesResponse();
		ArrayList<Empleado> listaNoDisponibles = new ArrayList<Empleado>();
		ArrayList<Empleado> listaDisponibles = new ArrayList<Empleado>();
		int idEvento = listarEmpleadosDisponibles.getIdEvento();
		Date fechaIniEvento = null;
		Date fechaFinEvento = null;
		try {
			// Conectamos a la bd
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			java.sql.Connection con = DriverManager.getConnection("jdbc:mysql://localhost/ievents", "root", "");

			java.sql.Statement st = con.createStatement();

			// Buscamos evento
			String sql = "SELECT idEvento, fechaInicio, fechaCierre "
					+ "FROM evento WHERE idEvento = " + idEvento;
			ResultSet rs = st.executeQuery(sql);
			if (rs.next()) { // Si no se ha encontrado el evento
				fechaIniEvento = rs.getDate("fechaInicio");
				fechaFinEvento = rs.getDate("fechaCierre");
				
				//Obtenemos todos los empleados, y los que est�n contratados con sus fechas de contrataci�n
				sql = "SELECT idEmpleado, nombre, apellido1, apellido2, fechaInicio, fechaCierre FROM empleado as emp LEFT JOIN empleado_has_evento as ehe ON emp.idEmpleado=ehe.Empleado_idEmpleado LEFT JOIN evento as eve ON ehe.Evento_idEvento=eve.idEvento AND ehe.Evento_Contrato_idContrato=eve.Contrato_idContrato";
				ResultSet rs2 = st.executeQuery(sql);
				while(rs2.next()){
					Empleado tmp = new Empleado();
					tmp.setIdEmpleado(rs2.getInt("idEmpleado"));
					tmp.setNombre(rs2.getString("nombre"));
					tmp.setApellido1(rs2.getString("apellido1"));
					tmp.setApellido2(rs2.getString("apellido2"));
					Date fechaIniContrato = rs2.getDate("fechaInicio");
					Date fechaFinContrato = rs2.getDate("fechaCierre");
					
					//Mirar si el empleado esta disponible en las fechas del evento
					if((fechaIniContrato == null && fechaFinContrato == null) || //Si no se ha contratado nunca � est� libre en la fecha de contrato
						!(fechaIniEvento.compareTo(fechaIniContrato) <= 0 && fechaFinEvento.compareTo(fechaFinContrato) >= 0) && //Si los eventos son simult�neos
						!(fechaIniEvento.compareTo(fechaIniContrato) <= 0 && fechaFinEvento.compareTo(fechaIniContrato) >=0 && fechaFinEvento.compareTo(fechaFinContrato) <= 0) && //Si los eventos intersectan por arriba
						!(fechaIniEvento.compareTo(fechaIniContrato) >= 0 && fechaFinEvento.compareTo(fechaFinContrato) <= 0) && //Si el evento ocurre a la vez que el contrato (el periodo que esta contratado es simultaneo y mayor que el evento)
						!(fechaIniEvento.compareTo(fechaIniContrato) >= 0 && fechaIniEvento.compareTo(fechaFinContrato) <=0 && fechaFinEvento.compareTo(fechaFinContrato) >= 0)){ //Si los eventos intersectan por debajo
						listaDisponibles.add(tmp); //Si est� disponible lo a�adimos a la lista
					}
					else{
						listaNoDisponibles.add(tmp); //Si no esta disponible lo a�adimos a la lista
					}
				}
				
				//Limpiamos la lista de disponibles con los que no est�n disponibles
				//Esto se hace porque en empleado_has_evento est�n duplicados los empleados (porque
				// se pueden contratar para m�s de un evento) y al hacer las comparaciones no se comparan
				// por separado, por lo que quiz� en la primera comparaci�n est� disponible, pero en la segunda no
				// debido a esto hay que filtrar los disponibles al acabar
				for(int i=0; i < listaNoDisponibles.size(); i++){ 
					for(int j=0; j < listaDisponibles.size(); j++){
						if(listaNoDisponibles.get(i).getIdEmpleado() == listaDisponibles.get(j).getIdEmpleado()){
							listaDisponibles.remove(j);
						}
					}
				}

				for(int i=0; i < listaDisponibles.size(); i++){
					devolver.addOut(listaDisponibles.get(i));
				}
			}
			
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return devolver;

	}

	/**
	 * Contrata un empleado para un evento concreto.
	 * 
	 * @param contratar
	 * @return contratarResponse
	 */

	public ContratarResponse contratar(iEvents.servicios.Contratar contratar) {
		ContratarResponse devolver = new ContratarResponse();

		int evento_id = contratar.getEvento_id();
		int contrato_id = contratar.getContrato_id();
		int empleado_id = contratar.getEmpleado_id();
		devolver.setResultado(true);
		try {
			// Conectamos a la bd
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			java.sql.Connection con = DriverManager.getConnection("jdbc:mysql://localhost/ievents", "root", "");

			java.sql.Statement st = con.createStatement();

			// Buscamos evento
			String sql = "SELECT idEvento FROM evento WHERE idEvento=" + Integer.toString(evento_id);
			ResultSet rs = st.executeQuery(sql);
			if (!rs.next()) { // Si no se ha encontrado devolvemos false
				devolver.setResultado(false);
			}

			// Buscamos el id del empleado
			sql = "SELECT idEmpleado FROM empleado WHERE idEmpleado=" + Integer.toString(empleado_id);
			rs = st.executeQuery(sql);
			if (!rs.next()) { // Si no se ha encontrado devolvemos false
				devolver.setResultado(false);
			}

			// Preparamos el insert con todos sus datos
			sql = "INSERT INTO empleado_has_evento VALUES(" + empleado_id + ",'" + evento_id + "','" + contrato_id
					+ "')";

			int number = st.executeUpdate(sql);
			// System.out.println(Integer.toString(number));
			if (number == 0) {
				devolver.setResultado(false);
			}

		} catch (Exception e) {
			devolver.setResultado(false);
			System.out.println(e.getMessage());
		}

		return devolver;
	}

}
