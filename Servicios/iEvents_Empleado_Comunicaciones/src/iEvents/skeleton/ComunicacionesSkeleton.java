
/**
 * ComunicacionesSkeleton.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.3  Built on : Jun 27, 2015 (11:17:49 BST)
 */
package iEvents.skeleton;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.util.Date;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import iEvents.servicios.*;

/**
 * ComunicacionesSkeleton java skeleton for the axisService
 */
public class ComunicacionesSkeleton {

	/**
	 * Auto generated method signature
	 * 
	 * @param enviarComunicacionACliente
	 * @return enviarComunicacionAClienteResponse
	 */

	public EnviarComunicacionAClienteResponse enviarComunicacionACliente(
			EnviarComunicacionACliente enviarComunicacionACliente) {
		EnviarComunicacionAClienteResponse devolver = new EnviarComunicacionAClienteResponse();

		// crear una conexion a db
		// obtener el cliente y el contrato
		// Si el cliente tiene mail enviar correo
		int id_Cliente = enviarComunicacionACliente.getId_Cliente();
		String explicacion = enviarComunicacionACliente.getExplicacion();
		int id_Contrato = enviarComunicacionACliente.getId_Contrato();
		String cliente_nombre = "";
		String cliente_email = null;
		String contrato_desc = null;
		String textoContrato = "";

		try {
			// Conectamos a la bd
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			java.sql.Connection con = DriverManager.getConnection("jdbc:mysql://localhost/ievents", "root", "");

			java.sql.Statement st = con.createStatement();

			// Buscamos el nombre del cliente
			String sql = "SELECT nombre, email FROM cliente WHERE idCliente=" + Integer.toString(id_Cliente);
			ResultSet rs = st.executeQuery(sql);
			if (rs.next()) { // Si se ha encontrado devolvemos true
				cliente_nombre = rs.getString("nombre");
				cliente_email = rs.getString("email");
			}

			// Buscamos el id del cliente
			sql = "SELECT descripcion FROM contrato WHERE idContrato=" + Integer.toString(id_Contrato);
			rs = st.executeQuery(sql);
			if (rs.next()) { // Si se ha encontrado devolvemos true
				contrato_desc = rs.getString("descripcion");
			}

		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
		}

		if (cliente_email != null) {

			Properties props = new Properties();
			// Nombre del host de correo, es smtp.gmail.com
			props.setProperty("mail.smtp.host", "smtp.gmail.com");
			// TLS si est� disponible
			props.setProperty("mail.smtp.starttls.enable", "true");
			// Puerto de gmail para envio de correos
			props.setProperty("mail.smtp.port", "587");
			// Nombre del usuario
			props.setProperty("mail.smtp.user", "ievents.comunicaciones@gmail.com");
			// Si requiere o no usuario y password para conectarse.
			props.setProperty("mail.smtp.auth", "true");

			Session session = Session.getInstance(props, new javax.mail.Authenticator() {
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication("ievents.comunicaciones", "comunicaciones");
				}
			});

			session.setDebug(true);

			MimeMessage message = new MimeMessage(session);

			try {

				message.setFrom(new InternetAddress("ievents.comunicaciones@gmail.com"));
				message.addRecipient(Message.RecipientType.TO, new InternetAddress(cliente_email));

				message.setSubject("Comunicaci�n iEvents");

				if (contrato_desc != null) {
					textoContrato = "Referente a " + contrato_desc + ".\n\n";
				}

				message.setText("Hola " + cliente_nombre + ",\n\n" +

				textoContrato + explicacion + ".\n\nSaludos.");

				Transport t = session.getTransport("smtp");
				t.connect();
				t.sendMessage(message, message.getAllRecipients());
				t.close();

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				System.out.println(e.getMessage());
				devolver.setOut(false);
				return devolver;
			}
			devolver.setOut(true);
			return devolver;
		}
		devolver.setOut(false);
		return devolver;
	}

	/**
	 * Auto generated method signature
	 * 
	 * @param enviarPropuetaAPatrocinador
	 * @return enviarPropuetaAPatrocinadorResponse
	 */

	public EnviarPropuestaAPatrocinadorResponse enviarPropuestaAPatrocinador(
			EnviarPropuestaAPatrocinador enviarPropuetaAPatrocinador) {
		EnviarPropuestaAPatrocinadorResponse devolver = new EnviarPropuestaAPatrocinadorResponse();

		// crear una patrocinador a db
		// obtener el patrocinador y el contrato
		// Si el cliente tiene mail enviar correo
		int id_Patrocinador = enviarPropuetaAPatrocinador.getId_Patrocinador();
		Date fechaIni = enviarPropuetaAPatrocinador.getFechaIni();
		Date fechaFin = enviarPropuetaAPatrocinador.getFechaFin();
		int id_Evento = enviarPropuetaAPatrocinador.getId_Evento();
		String patrocinador_nombre = "";
		String patrocinador_email = null;
		String emplazamiento = null;
		String evento_desc = "";
		String textoEvento = "";

		try {
			// Conectamos a la bd
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			java.sql.Connection con = DriverManager.getConnection("jdbc:mysql://localhost/ievents", "root", "");

			java.sql.Statement st = con.createStatement();

			// Buscamos el nombre del patrocinador
			String sql = "SELECT nombre, email FROM patrocinador WHERE idPatrocinador="
					+ Integer.toString(id_Patrocinador);
			ResultSet rs = st.executeQuery(sql);
			if (rs.next()) { // Si se ha encontrado devolvemos true
				patrocinador_nombre = rs.getString("nombre");
				patrocinador_email = rs.getString("email");
			}

			// Buscamos algunos datos
			sql = "SELECT nombre FROM emplazamiento WHERE idEmplazamiento in (SELECT Emplazamiento_idEmplazamiento FROM contrato WHERE idContrato in (SELECT Contrato_idContrato FROM evento WHERE idEvento = "
					+ Integer.toString(id_Evento) + "))";
			rs = st.executeQuery(sql);
			if (rs.next()) { // Si se ha encontrado devolvemos true
				emplazamiento = rs.getString("nombre");
			}
			sql = "SELECT descripcion FROM contrato WHERE idContrato in (SELECT Contrato_idContrato FROM evento WHERE idEvento = "
					+ Integer.toString(id_Evento) + ")";
			rs = st.executeQuery(sql);
			if (rs.next()) { // Si se ha encontrado devolvemos true
				evento_desc = rs.getString("descripcion");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("Nombre Patrocinador:"+patrocinador_nombre+
							"\r\nEmail patrocinador: " +patrocinador_email+
							"\r\nEmplazamiento: "+emplazamiento+
							"\r\nDescripcion Evento: "+evento_desc+
							"\r\nTexto Evento:"+textoEvento);
		
		if (patrocinador_email != null) {

			Properties props = new Properties();
			// Nombre del host de correo, es smtp.gmail.com
			props.setProperty("mail.smtp.host", "smtp.gmail.com");
			// TLS si est� disponible
			props.setProperty("mail.smtp.starttls.enable", "true");
			// Puerto de gmail para envio de correos
			props.setProperty("mail.smtp.port", "587");
			// Nombre del usuario
			props.setProperty("mail.smtp.user", "ievents.comunicaciones@gmail.com");
			// Si requiere o no usuario y password para conectarse.
			props.setProperty("mail.smtp.auth", "true");

			Session session = Session.getInstance(props, new javax.mail.Authenticator() {
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication("ievents.comunicaciones", "comunicaciones");
				}
			});
			session.setDebug(true);

			MimeMessage message = new MimeMessage(session);

			try {
				message.setFrom(new InternetAddress("ievents.comunicaciones@gmail.com"));
				message.addRecipient(Message.RecipientType.TO, new InternetAddress(patrocinador_email));

				message.setSubject("Comunicaci�n iEvents");

				if (emplazamiento != null) {
					emplazamiento = "lugar por determinar";
				}

				if (evento_desc != null) {
					evento_desc = "";
				}

				message.setText("Hola " + patrocinador_nombre + ",\n\n" +

				"Nos gustar�a colaborar con usted en el evento " + evento_desc + " que realizaremos pronto en "
						+ emplazamiento + ". Si est� interesado su patrocinio ser�a desde fecha inicio " + fechaIni
						+ " a fecha fin " + fechaFin + "si est� interesado comun�quese con nosotros.\n\nSaludos.");

				Transport t = session.getTransport("smtp");
				t.connect();
				t.sendMessage(message, message.getAllRecipients());
				t.close();

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				System.out.println(e.getMessage());
				devolver.setOut(false);
				return devolver;
			}
			devolver.setOut(true);
			return devolver;
		}
		devolver.setOut(false);
		return devolver;
	}

}
