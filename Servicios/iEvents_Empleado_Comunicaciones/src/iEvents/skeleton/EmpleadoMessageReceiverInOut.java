
/**
 * EmpleadoMessageReceiverInOut.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.3  Built on : Jun 27, 2015 (11:17:49 BST)
 */
        package iEvents.skeleton;

        /**
        *  EmpleadoMessageReceiverInOut message receiver
        */

        public class EmpleadoMessageReceiverInOut extends org.apache.axis2.receivers.AbstractInOutMessageReceiver{


        public void invokeBusinessLogic(org.apache.axis2.context.MessageContext msgContext, org.apache.axis2.context.MessageContext newMsgContext)
        throws org.apache.axis2.AxisFault{

        try {

        // get the implementation class for the Web Service
        Object obj = getTheImplementationObject(msgContext);

        EmpleadoSkeleton skel = (EmpleadoSkeleton)obj;
        //Out Envelop
        org.apache.axiom.soap.SOAPEnvelope envelope = null;
        //Find the axisOperation that has been set by the Dispatch phase.
        org.apache.axis2.description.AxisOperation op = msgContext.getOperationContext().getAxisOperation();
        if (op == null) {
        throw new org.apache.axis2.AxisFault("Operation is not located, if this is doclit style the SOAP-ACTION should specified via the SOAP Action to use the RawXMLProvider");
        }

        java.lang.String methodName;
        if((op.getName() != null) && ((methodName = org.apache.axis2.util.JavaUtils.xmlNameToJavaIdentifier(op.getName().getLocalPart())) != null)){


        

            if("listarEmpleadosDisponibles".equals(methodName)){
                
                iEvents.servicios.ListarEmpleadosDisponiblesResponse listarEmpleadosDisponiblesResponse9 = null;
	                        iEvents.servicios.ListarEmpleadosDisponibles wrappedParam =
                                                             (iEvents.servicios.ListarEmpleadosDisponibles)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    iEvents.servicios.ListarEmpleadosDisponibles.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               listarEmpleadosDisponiblesResponse9 =
                                                   
                                                   
                                                         skel.listarEmpleadosDisponibles(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), listarEmpleadosDisponiblesResponse9, false, new javax.xml.namespace.QName("http://www.example.org/empleado/",
                                                    "listarEmpleadosDisponibles"));
                                    } else 

            if("contratar".equals(methodName)){
                
                iEvents.servicios.ContratarResponse contratarResponse11 = null;
	                        iEvents.servicios.Contratar wrappedParam =
                                                             (iEvents.servicios.Contratar)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    iEvents.servicios.Contratar.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               contratarResponse11 =
                                                   
                                                   
                                                         skel.contratar(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), contratarResponse11, false, new javax.xml.namespace.QName("http://www.example.org/empleado/",
                                                    "contratar"));
                                    
            } else {
              throw new java.lang.RuntimeException("method not found");
            }
        

        newMsgContext.setEnvelope(envelope);
        }
        }
        catch (java.lang.Exception e) {
        throw org.apache.axis2.AxisFault.makeFault(e);
        }
        }
        
        //
            private  org.apache.axiom.om.OMElement  toOM(iEvents.servicios.ListarEmpleadosDisponibles param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(iEvents.servicios.ListarEmpleadosDisponibles.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(iEvents.servicios.ListarEmpleadosDisponiblesResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(iEvents.servicios.ListarEmpleadosDisponiblesResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(iEvents.servicios.Contratar param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(iEvents.servicios.Contratar.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(iEvents.servicios.ContratarResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(iEvents.servicios.ContratarResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, iEvents.servicios.ListarEmpleadosDisponiblesResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(iEvents.servicios.ListarEmpleadosDisponiblesResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private iEvents.servicios.ListarEmpleadosDisponiblesResponse wraplistarEmpleadosDisponibles(){
                                iEvents.servicios.ListarEmpleadosDisponiblesResponse wrappedElement = new iEvents.servicios.ListarEmpleadosDisponiblesResponse();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, iEvents.servicios.ContratarResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(iEvents.servicios.ContratarResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private iEvents.servicios.ContratarResponse wrapcontratar(){
                                iEvents.servicios.ContratarResponse wrappedElement = new iEvents.servicios.ContratarResponse();
                                return wrappedElement;
                         }
                    


        /**
        *  get the default envelope
        */
        private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory){
        return factory.getDefaultEnvelope();
        }


        private  java.lang.Object fromOM(
        org.apache.axiom.om.OMElement param,
        java.lang.Class type,
        java.util.Map extraNamespaces) throws org.apache.axis2.AxisFault{

        try {
        
                if (iEvents.servicios.Contratar.class.equals(type)){
                
                        return iEvents.servicios.Contratar.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (iEvents.servicios.ContratarResponse.class.equals(type)){
                
                        return iEvents.servicios.ContratarResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (iEvents.servicios.ListarEmpleadosDisponibles.class.equals(type)){
                
                        return iEvents.servicios.ListarEmpleadosDisponibles.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (iEvents.servicios.ListarEmpleadosDisponiblesResponse.class.equals(type)){
                
                        return iEvents.servicios.ListarEmpleadosDisponiblesResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
        } catch (java.lang.Exception e) {
        throw org.apache.axis2.AxisFault.makeFault(e);
        }
           return null;
        }



    

        /**
        *  A utility method that copies the namepaces from the SOAPEnvelope
        */
        private java.util.Map getEnvelopeNamespaces(org.apache.axiom.soap.SOAPEnvelope env){
        java.util.Map returnMap = new java.util.HashMap();
        java.util.Iterator namespaceIterator = env.getAllDeclaredNamespaces();
        while (namespaceIterator.hasNext()) {
        org.apache.axiom.om.OMNamespace ns = (org.apache.axiom.om.OMNamespace) namespaceIterator.next();
        returnMap.put(ns.getPrefix(),ns.getNamespaceURI());
        }
        return returnMap;
        }

        private org.apache.axis2.AxisFault createAxisFault(java.lang.Exception e) {
        org.apache.axis2.AxisFault f;
        Throwable cause = e.getCause();
        if (cause != null) {
            f = new org.apache.axis2.AxisFault(e.getMessage(), cause);
        } else {
            f = new org.apache.axis2.AxisFault(e.getMessage());
        }

        return f;
    }

        }//end of class
    