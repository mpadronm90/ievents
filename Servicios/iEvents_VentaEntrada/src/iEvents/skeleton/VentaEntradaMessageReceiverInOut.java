
/**
 * VentaEntradaMessageReceiverInOut.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.3  Built on : Jun 27, 2015 (11:17:49 BST)
 */
        package iEvents.skeleton;

        /**
        *  VentaEntradaMessageReceiverInOut message receiver
        */

        public class VentaEntradaMessageReceiverInOut extends org.apache.axis2.receivers.AbstractInOutMessageReceiver{


        public void invokeBusinessLogic(org.apache.axis2.context.MessageContext msgContext, org.apache.axis2.context.MessageContext newMsgContext)
        throws org.apache.axis2.AxisFault{

        try {

        // get the implementation class for the Web Service
        Object obj = getTheImplementationObject(msgContext);

        VentaEntradaSkeleton skel = (VentaEntradaSkeleton)obj;
        //Out Envelop
        org.apache.axiom.soap.SOAPEnvelope envelope = null;
        //Find the axisOperation that has been set by the Dispatch phase.
        org.apache.axis2.description.AxisOperation op = msgContext.getOperationContext().getAxisOperation();
        if (op == null) {
        throw new org.apache.axis2.AxisFault("Operation is not located, if this is doclit style the SOAP-ACTION should specified via the SOAP Action to use the RawXMLProvider");
        }

        java.lang.String methodName;
        if((op.getName() != null) && ((methodName = org.apache.axis2.util.JavaUtils.xmlNameToJavaIdentifier(op.getName().getLocalPart())) != null)){


        

            if("ventaEntrada".equals(methodName)){
                
                iEvents.servicios.VentaEntradaResponse ventaEntradaResponse17 = null;
	                        iEvents.servicios.VentaEntrada wrappedParam =
                                                             (iEvents.servicios.VentaEntrada)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    iEvents.servicios.VentaEntrada.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               ventaEntradaResponse17 =
                                                   
                                                   
                                                         skel.ventaEntrada(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), ventaEntradaResponse17, false, new javax.xml.namespace.QName("http://localhost:9080/servicesVentaEntrada/",
                                                    "ventaEntrada"));
                                    } else 

            if("listarEntradasEvento".equals(methodName)){
                
                iEvents.servicios.ListarEntradasEventoResponse listarEntradasEventoResponse19 = null;
	                        iEvents.servicios.ListarEntradasEvento wrappedParam =
                                                             (iEvents.servicios.ListarEntradasEvento)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    iEvents.servicios.ListarEntradasEvento.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               listarEntradasEventoResponse19 =
                                                   
                                                   
                                                         skel.listarEntradasEvento(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), listarEntradasEventoResponse19, false, new javax.xml.namespace.QName("http://localhost:9080/servicesVentaEntrada/",
                                                    "listarEntradasEvento"));
                                    } else 

            if("enviarTicket".equals(methodName)){
                
                iEvents.servicios.EnviarTicketResponse enviarTicketResponse21 = null;
	                        iEvents.servicios.EnviarTicket wrappedParam =
                                                             (iEvents.servicios.EnviarTicket)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    iEvents.servicios.EnviarTicket.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               enviarTicketResponse21 =
                                                   
                                                   
                                                         skel.enviarTicket(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), enviarTicketResponse21, false, new javax.xml.namespace.QName("http://localhost:9080/servicesVentaEntrada/",
                                                    "enviarTicket"));
                                    } else 

            if("generarTicket".equals(methodName)){
                
                iEvents.servicios.GenerarTicketResponse generarTicketResponse23 = null;
	                        iEvents.servicios.GenerarTicket wrappedParam =
                                                             (iEvents.servicios.GenerarTicket)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    iEvents.servicios.GenerarTicket.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               generarTicketResponse23 =
                                                   
                                                   
                                                         skel.generarTicket(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), generarTicketResponse23, false, new javax.xml.namespace.QName("http://localhost:9080/servicesVentaEntrada/",
                                                    "generarTicket"));
                                    
            } else {
              throw new java.lang.RuntimeException("method not found");
            }
        

        newMsgContext.setEnvelope(envelope);
        }
        }
        catch (java.lang.Exception e) {
        throw org.apache.axis2.AxisFault.makeFault(e);
        }
        }
        
        //
            private  org.apache.axiom.om.OMElement  toOM(iEvents.servicios.VentaEntrada param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(iEvents.servicios.VentaEntrada.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(iEvents.servicios.VentaEntradaResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(iEvents.servicios.VentaEntradaResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(iEvents.servicios.ListarEntradasEvento param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(iEvents.servicios.ListarEntradasEvento.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(iEvents.servicios.ListarEntradasEventoResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(iEvents.servicios.ListarEntradasEventoResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(iEvents.servicios.EnviarTicket param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(iEvents.servicios.EnviarTicket.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(iEvents.servicios.EnviarTicketResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(iEvents.servicios.EnviarTicketResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(iEvents.servicios.GenerarTicket param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(iEvents.servicios.GenerarTicket.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(iEvents.servicios.GenerarTicketResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(iEvents.servicios.GenerarTicketResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, iEvents.servicios.VentaEntradaResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(iEvents.servicios.VentaEntradaResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private iEvents.servicios.VentaEntradaResponse wrapVentaEntrada(){
                                iEvents.servicios.VentaEntradaResponse wrappedElement = new iEvents.servicios.VentaEntradaResponse();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, iEvents.servicios.ListarEntradasEventoResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(iEvents.servicios.ListarEntradasEventoResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private iEvents.servicios.ListarEntradasEventoResponse wraplistarEntradasEvento(){
                                iEvents.servicios.ListarEntradasEventoResponse wrappedElement = new iEvents.servicios.ListarEntradasEventoResponse();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, iEvents.servicios.EnviarTicketResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(iEvents.servicios.EnviarTicketResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private iEvents.servicios.EnviarTicketResponse wrapenviarTicket(){
                                iEvents.servicios.EnviarTicketResponse wrappedElement = new iEvents.servicios.EnviarTicketResponse();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, iEvents.servicios.GenerarTicketResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(iEvents.servicios.GenerarTicketResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private iEvents.servicios.GenerarTicketResponse wrapgenerarTicket(){
                                iEvents.servicios.GenerarTicketResponse wrappedElement = new iEvents.servicios.GenerarTicketResponse();
                                return wrappedElement;
                         }
                    


        /**
        *  get the default envelope
        */
        private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory){
        return factory.getDefaultEnvelope();
        }


        private  java.lang.Object fromOM(
        org.apache.axiom.om.OMElement param,
        java.lang.Class type,
        java.util.Map extraNamespaces) throws org.apache.axis2.AxisFault{

        try {
        
                if (iEvents.servicios.EnviarTicket.class.equals(type)){
                
                        return iEvents.servicios.EnviarTicket.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (iEvents.servicios.EnviarTicketResponse.class.equals(type)){
                
                        return iEvents.servicios.EnviarTicketResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (iEvents.servicios.GenerarTicket.class.equals(type)){
                
                        return iEvents.servicios.GenerarTicket.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (iEvents.servicios.GenerarTicketResponse.class.equals(type)){
                
                        return iEvents.servicios.GenerarTicketResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (iEvents.servicios.ListarEntradasEvento.class.equals(type)){
                
                        return iEvents.servicios.ListarEntradasEvento.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (iEvents.servicios.ListarEntradasEventoResponse.class.equals(type)){
                
                        return iEvents.servicios.ListarEntradasEventoResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (iEvents.servicios.VentaEntrada.class.equals(type)){
                
                        return iEvents.servicios.VentaEntrada.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
                if (iEvents.servicios.VentaEntradaResponse.class.equals(type)){
                
                        return iEvents.servicios.VentaEntradaResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
            
        } catch (java.lang.Exception e) {
        throw org.apache.axis2.AxisFault.makeFault(e);
        }
           return null;
        }



    

        /**
        *  A utility method that copies the namepaces from the SOAPEnvelope
        */
        private java.util.Map getEnvelopeNamespaces(org.apache.axiom.soap.SOAPEnvelope env){
        java.util.Map returnMap = new java.util.HashMap();
        java.util.Iterator namespaceIterator = env.getAllDeclaredNamespaces();
        while (namespaceIterator.hasNext()) {
        org.apache.axiom.om.OMNamespace ns = (org.apache.axiom.om.OMNamespace) namespaceIterator.next();
        returnMap.put(ns.getPrefix(),ns.getNamespaceURI());
        }
        return returnMap;
        }

        private org.apache.axis2.AxisFault createAxisFault(java.lang.Exception e) {
        org.apache.axis2.AxisFault f;
        Throwable cause = e.getCause();
        if (cause != null) {
            f = new org.apache.axis2.AxisFault(e.getMessage(), cause);
        } else {
            f = new org.apache.axis2.AxisFault(e.getMessage());
        }

        return f;
    }

        }//end of class
    