package iEvents.skeleton;
import java.io.File;
import java.io.PrintWriter;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import iEvents.servicios.Entrada;
import iEvents.servicios.EnviarTicket;
import iEvents.servicios.EnviarTicketResponse;
import iEvents.servicios.GenerarTicket;
import iEvents.servicios.GenerarTicketResponse;
import iEvents.servicios.ListarEntradasEvento;
import iEvents.servicios.ListarEntradasEventoResponse;
import iEvents.servicios.VentaEntrada;
import iEvents.servicios.VentaEntradaResponse;

public class VentaEntradaSkeleton
{

    @SuppressWarnings("finally")
	public VentaEntradaResponse ventaEntrada
    (VentaEntrada ventaEntrada)
    {
    	VentaEntradaResponse respuesta = new VentaEntradaResponse();
    	try {
    		
    		int idVentaEntrada;
    		double precio;
    		Class.forName("com.mysql.jdbc.Driver");
            java.sql.Connection conexion_mysql = DriverManager.getConnection ("jdbc:mysql://localhost/ievents","root", "");
            Statement statement = conexion_mysql.createStatement();
            
    		
    		String consulta_id="SELECT idVentaEntrada FROM ventaentrada "+
    		"order by idVentaEntrada desc "+
    	    "LIMIT 1";
    		
    		ResultSet resultSet=statement.executeQuery(consulta_id);
    		if(resultSet.next()){
    			idVentaEntrada=resultSet.getInt("idVentaEntrada")+1;
    		}else{
    			idVentaEntrada=1;
    		}
    		String consulta_precio="select precioEntrada from evento where idEvento="+ventaEntrada.getEvento_idEvento();

    		resultSet=statement.executeQuery(consulta_precio);
    		if(resultSet.next()){
    			precio=resultSet.getDouble("precioEntrada");
    			
    		}else{
    			precio=0;
    		}
    		
	    	String consulta = "INSERT INTO ventaentrada (idVentaEntrada, "
										    			+ "precio, "
										    			+ "emailParticipante, "
										    			+ "Evento_idEvento, "
										    			+ "Evento_Contrato_idContrato) "
			    			+ " VALUES (" + idVentaEntrada + ", "
										  + precio + ", '"
										  + ventaEntrada.getEmail() + "', "
										  + ventaEntrada.getEvento_idEvento() + ", "
										  + ventaEntrada.getEvento_Contrato_idContrato()+ ")";
	    	System.out.println(consulta);
	    	respuesta.setOut(idVentaEntrada);

    		
            statement.executeUpdate(consulta);
            conexion_mysql.close();
    	}
    	catch(Exception e) {
    		respuesta.setOut(-1);
    		System.out.println("Error: " + e.toString());
    	}
    	finally {
    		return respuesta;
    	}
    }

    @SuppressWarnings("finally")
	public EnviarTicketResponse enviarTicket
    (EnviarTicket enviarTicket)
    {
    	EnviarTicketResponse respuesta = new EnviarTicketResponse();
    	try {
    		// Obtenemos el id de la entrada para localizar el fichero
    		Class.forName("com.mysql.jdbc.Driver");
            java.sql.Connection conexion_mysql = DriverManager.getConnection ("jdbc:mysql://localhost/ievents","root", "");
            Statement statement = conexion_mysql.createStatement();

            // Obtenemos ID de la entrada
            ResultSet rs = statement.executeQuery("SELECT idVentaEntrada, emailParticipante "
							            		+ "FROM ventaentrada "
							            		+ "WHERE idVentaEntrada = '" + enviarTicket.getIdEntrada() + "'");
    		int idVentaEntrada = 0;
    		String destinatarioCorreo = "";
    		if (rs.next()) {
    			idVentaEntrada = rs.getInt("idVentaEntrada");
    			destinatarioCorreo = rs.getString("emailParticipante");
    		}

    		// Obtenemos la descripcion del evento
    		rs = statement.executeQuery("SELECT descripcion "
    									+ "FROM contrato "
    									+ "WHERE idContrato = '" + enviarTicket.getIdContrato() + "'");
    		String descripcion = "";
    		if (rs.next()) {
    			descripcion = rs.getString("descripcion");
    		}

    		// Generamos el correo y usaremos el SMTP de Gmail
    		Properties props = new Properties();
    		props.put("mail.smtp.host", "smtp.gmail.com");
    		props.setProperty("mail.smtp.starttls.enable", "true");
    		props.setProperty("mail.smtp.port","587");
    		props.setProperty("mail.smtp.user", "ieventsua@gmail.com");
    		props.setProperty("mail.smtp.auth", "true");

    		Session session = Session.getDefaultInstance(props, null);
    		session.setDebug(true);

    		BodyPart texto = new MimeBodyPart();
    		texto.setText("Ticket generado por iEvents. Descargue el documento adjunto");

    		BodyPart adjunto = new MimeBodyPart();
    		adjunto.setDataHandler(new DataHandler(new FileDataSource("entrada" + idVentaEntrada +".txt")));
    		adjunto.setFileName("entrada" + idVentaEntrada +".txt");

    		MimeMultipart multiParte = new MimeMultipart();
    		multiParte.addBodyPart(texto);
    		multiParte.addBodyPart(adjunto);

    		MimeMessage message = new MimeMessage(session);
    		// Se rellena el From
    		message.setFrom(new InternetAddress("ieventsua@gmail.com"));
    		// Se rellenan los destinatarios
    		message.addRecipient(Message.RecipientType.TO, new InternetAddress(destinatarioCorreo));
    		// Se rellena el subject
    		message.setSubject("Su ticket del evento " + descripcion);
    		// Se mete el texto y la foto adjunta.
    		message.setContent(multiParte);

    		Transport t = session.getTransport("smtp");
    		t.connect("ieventsua@gmail.com","mtisua2016");
    		t.sendMessage(message,message.getAllRecipients());
    		t.close();

    		respuesta.setOut(true);
    	}
    	catch (Exception e) {
    		respuesta.setOut(false);
    		System.out.println("Error: " + e.toString());
    	}
    	finally {
        	return respuesta;
    	}
    }

	@SuppressWarnings("finally")
	public GenerarTicketResponse generarTicket
	(GenerarTicket generarTicket)
    {
    	GenerarTicketResponse respuesta = new GenerarTicketResponse();
    	try {
    		Class.forName("com.mysql.jdbc.Driver");
            java.sql.Connection conexion_mysql = DriverManager.getConnection ("jdbc:mysql://localhost/ievents","root", "");
            Statement statement = conexion_mysql.createStatement();

            // Obtenemos id de ventaEntrada, precio y email del ticket
            ResultSet rs = statement.executeQuery("SELECT idVentaEntrada, "
							            		+ "precio, "
							            		+ "emailParticipante "
							            		+ "FROM ventaentrada "
							            		+ "WHERE idVentaEntrada = '" + generarTicket.getIdEntrada() + "'");

            float precio = 0.0f;
            int idVentaEntrada = 0;
            String email = "";

            if (rs.next()) {
            	precio = rs.getFloat("precio");
            	idVentaEntrada = rs.getInt("idVentaEntrada");
            	email = rs.getString("emailParticipante");
            }

    		// Obtenemos descripci�n del evento
            String descripcion = "";
            rs = statement.executeQuery("SELECT descripcion "
            							+ "FROM contrato "
            							+ "WHERE idContrato = '" + generarTicket.getIdContrato() + "'");

            if (rs.next()) {
            	descripcion = rs.getString("descripcion");
            }

            // Obtenemos las fechas de inicio y final del evento
            rs = statement.executeQuery("SELECT fechaInicio, fechaCierre "
				            		+ "FROM evento "
				            		+ "WHERE idEvento = '" + generarTicket.getIdEvento()+ "'");

            java.util.Date f_inicio = new java.util.Date();
            java.util.Date f_cierre = new java.util.Date();

            if (rs.next()) {
            	f_inicio = rs.getDate("fechaInicio");
            	f_cierre = rs.getDate("fechaCierre");
            }

            // Generamos el ticket

            String textoFichero = "Ticket de acceso para el evento " + descripcion + "\n"
            + "Email: " + email + "\n"
            + "Fecha de inicio: " + f_inicio.toString() + "\n"
            + "Fecha de cierre: " + f_cierre.toString() + "\n"
            + "N� de serie: " + idVentaEntrada + "\n"
            + "PVP (IVA incluido): " + precio + " �\n"
            + "�Gracias por su compra\n"
            + "------ Powered by iEvents ------";

            File archivo = new File("entrada" + idVentaEntrada + ".txt");
            PrintWriter  pw = new PrintWriter  (archivo);
            pw.println(textoFichero);
            pw.close();

    		respuesta.setOut(true);
    	}
    	catch (Exception e) {
    		respuesta.setOut(false);
    		System.out.println("Error: " + e.toString());
    	}
    	finally {
        	return respuesta;
    	}
    }

	@SuppressWarnings("finally")
	public ListarEntradasEventoResponse listarEntradasEvento(ListarEntradasEvento listarEntradasEvento) {
    	
		ListarEntradasEventoResponse dev = new ListarEntradasEventoResponse();
		try {
	    	String consulta = "SELECT idVentaEntrada FROM ventaentrada WHERE Evento_idEvento = " + listarEntradasEvento.getIn();
	    	System.out.println(consulta);
	    	dev.setOut(null);

    		Class.forName("com.mysql.jdbc.Driver");
            java.sql.Connection conexion_mysql = DriverManager.getConnection ("jdbc:mysql://localhost/ievents","root", "");
            Statement statement = conexion_mysql.createStatement();
            ResultSet rs = statement.executeQuery(consulta);
            
            while( rs.next() ){
            	
            	Entrada entrada = new Entrada();
            	entrada.setIdEntrada(rs.getInt("idVentaEntrada"));
            	
            	dev.addOut(entrada);
            }
            	
            conexion_mysql.close();
            
    	}
    	catch(Exception e) {
    		System.out.println("Error: " + e.toString());
    	}
    	finally {
    		return dev;
    	}
	}

}
