
/**
 * ServicioSkeleton.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.3  Built on : Jun 27, 2015 (11:17:49 BST)
 */
package iEvents.skeleton;

import iEvents.servicios.*;

import java.util.Date;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 *  ServicioSkeleton java skeleton for the axisService
 */
public class ServiciosSkeleton{
 
    /**
     * Reserva, asociando el servicio al Contrato, un servicio
     * 
     * @param reservar Recibimos la id del servicio y del contrato.
	 * @return reservarResponse devuelve true si todo va bien y false si no.
     */
	@SuppressWarnings("finally")
	public ReservarResponse reservar ( Reservar reservar )
    {
		ReservarResponse response = new ReservarResponse();
		response.setResult(true);
		try{
			Class.forName("com.mysql.jdbc.Driver");
			java.sql.Connection conexion = DriverManager.getConnection("jdbc:mysql://localhost/iEvents", "root", "");
			
			Statement statement = conexion.createStatement();
			
			//Insertamos en la tabla la nueva relaci�n.
			response.setResult(statement.execute("INSERT INTO servicio_has_contrato (Contrato_idContrato, Servicio_idServicio)"
					+ "VALUES (" + reservar.getIdContrato() + ", " + reservar.getIdServicio() + ")"));
			
			conexion.close();
			response.setResult(true);
			
		} catch( Exception e ) {
			System.out.println("Error: "+e.toString());
			response.setResult(false);
			
		}finally{
			return response;
		}
	}
 
    /**
     * Comprueba que un servicio sea disponible para unas fechas
     * 
	 * @param comprobarDisponibilidad 
	 * @return comprobarDisponibilidadResponse 
     */
	@SuppressWarnings("finally")
	public ComprobarDisponibilidadResponse comprobarDisponibilidad ( ComprobarDisponibilidad comprobarDisponibilidad )
	{
		ComprobarDisponibilidadResponse response = new ComprobarDisponibilidadResponse();
		response.setResult(false);
		Boolean disponible = true;
		
		try{
			Class.forName("com.mysql.jdbc.Driver");
			java.sql.Connection conexion = DriverManager.getConnection("jdbc:mysql://localhost/iEvents", "root", "");
			
			Statement statement = conexion.createStatement();
			
			//Comprobamos que sea disponible el servicio
			//Fechas de todos los contratos relacionado con el servicio a comprobar
			String consulta = "SELECT fechaInicioEvento, fechaFinEvento "
							+ "FROM contrato "
							+ "WHERE idContrato IN "
								+ "(SELECT Contrato_idContrato "
								+ "FROM servicio_has_contrato "
								+ "WHERE Servicio_idServicio=" + comprobarDisponibilidad.getIdServicio() + ")";
			ResultSet rs = statement.executeQuery(consulta);
			

			Date fechaInicioServicio = comprobarDisponibilidad.getFechaInicio();
			Date fechaFinServicio = comprobarDisponibilidad.getFechaFin();

			while( rs.next() && disponible){
				
				Date fechaInicioEvento = rs.getDate("fechaInicioEvento");
				Date fechaFinEvento = rs.getDate("fechaFinEvento");
				
				//Esto se hace porque en cliente nos introduce horas (en concreto 1:00:00) y nosotros lo tenemos en 0:00:00 en BDD
				fechaInicioServicio.setHours(0);
				fechaFinServicio.setHours(0);
				
				//Mirar si el empleado esta disponible en las fechas del evento
				if((fechaInicioEvento.compareTo(fechaInicioServicio) <= 0 && fechaFinEvento.compareTo(fechaFinServicio) >= 0) || //Si los eventos son simult�neos
					(fechaInicioEvento.compareTo(fechaInicioServicio) <= 0 && fechaInicioEvento.compareTo(fechaFinServicio) >=0 && fechaFinEvento.compareTo(fechaFinServicio) <= 0) || //Si los eventos intersectan por arriba
					(fechaInicioEvento.compareTo(fechaInicioServicio) >= 0 && fechaFinEvento.compareTo(fechaFinServicio) <= 0) || //Si el evento ocurre a la vez que el contrato (el periodo que esta contratado es simultaneo y mayor que el evento)
					(fechaInicioEvento.compareTo(fechaInicioServicio) >= 0 && fechaFinEvento.compareTo(fechaInicioServicio) <=0 && fechaFinEvento.compareTo(fechaFinServicio) >= 0)){ //Si los eventos intersectan por debajo
					disponible = false;
				}
			}
			
			//Si lo est� devolvemos true
			if( disponible ){
				response.setResult(true);
			}
			
			conexion.close();
			
		} catch( Exception e ) {
			System.out.println("Error: "+e.toString());
			
		}finally{
			return response;
		}
	}
 
}
