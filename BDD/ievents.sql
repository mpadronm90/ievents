-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 24-04-2016 a las 18:55:42
-- Versión del servidor: 5.5.24-log
-- Versión de PHP: 5.4.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `ievents`
--

CREATE DATABASE IF NOT EXISTS ievents;
USE ievents;
-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente`
--

CREATE TABLE IF NOT EXISTS `cliente` (
  `idCliente` int(11) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `apellido1` varchar(45) NOT NULL,
  `apellido2` varchar(45) DEFAULT NULL,
  `email` varchar(45) NOT NULL,
  `dni` varchar(45) NOT NULL,
  PRIMARY KEY (`idCliente`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `cliente`
--

INSERT INTO `cliente` (`idCliente`, `nombre`, `apellido1`, `apellido2`, `email`, `dni`) VALUES
(1, 'Manuel', 'García', 'Sánchez', 'manuel.gS@gmail.com', '11111111A'),
(2, 'María', 'Martinez', 'Cano', 'maría.mc@gmail.com', '22222222B'),
(3, 'Michel', 'Padrón', 'Morales', 'michelpadronmorales@gmail.com', '22222222B'),
(4, 'Pascual', '', NULL, 'pascual.maestreserver@gmail.com', '1231231'),
(5, 'Antonio', 'Active', 'MQ', 'antonio@active.mq', '123456789R'),
(6, 'Jose', 'Martinez', 'Varela', 'varela@eresmas.com', '12345634F'),
(7, 'Pedrito', 'Paya', 'Ramirez', 'pedril@pedro.com', '222222222E');
-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contrato`
--

CREATE TABLE IF NOT EXISTS `contrato` (
  `idContrato` int(11) NOT NULL,
  `fechaCreacionContrato` datetime NOT NULL,
  `fechaInicioEvento` date NOT NULL,
  `fechaFinEvento` date NOT NULL,
  `descripcion` varchar(45) DEFAULT NULL,
  `Cliente_idCliente` int(11) NOT NULL,
  `EstadoContrato_idEstadoContrato` int(11) NOT NULL,
  `Emplazamiento_idEmplazamiento` int(11) DEFAULT NULL,
  `Aforo` int(11) NOT NULL,  
  `urlImagen` varchar(500) NULL,
  PRIMARY KEY (`idContrato`),
  KEY `fk_Contrato_Cliente_idx` (`Cliente_idCliente`),
  KEY `fk_Contrato_EstadoContrato1_idx` (`EstadoContrato_idEstadoContrato`),
  KEY `fk_Contrato_Emplazamiento1_idx` (`Emplazamiento_idEmplazamiento`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `contrato`
--

INSERT INTO `contrato` (`idContrato`, `fechaCreacionContrato`, `fechaInicioEvento`, `fechaFinEvento`, `descripcion`, `Cliente_idCliente`, `EstadoContrato_idEstadoContrato`, `Emplazamiento_idEmplazamiento`, `Aforo`, `urlImagen`) VALUES
(1, '2016-04-08 00:00:00', '2016-05-18', '2016-05-20', 'Feria de Coches BMW-2016', 3, 3, 3, 300, 'http://blog.caranddriver.com/wp-content/uploads/2016/02/BMW-M2-MotoGP-safety-car-PLACEMENT.jpg'),
(2, '2016-05-08 00:00:00', '2016-10-22', '2016-10-20', 'Feria de comics PascualComic', 1, 3, NULL, 1000, 'http://modogeeks.com/wp-content/uploads/2015/10/dc-comics.jpg'),
(3, '2016-05-08 00:00:00', '2016-05-09', '2016-05-10', 'Evento loco', 4, 4, NULL, 100000000, 'https://1.bp.blogspot.com/-UUOHv_3y2UA/TyGBmNV_hxI/AAAAAAAAAc8/a68Ua0KgRfA/s1600/loco.png'),
(4, '2016-05-09 00:00:00', '2016-05-20', '2016-05-21', 'Ferie de Manga', 4, 2, NULL, 1000, 'https://i.ytimg.com/vi/GaWcwfxDQoY/maxresdefault.jpg'),
(5, '2016-05-09 00:00:00', '2016-10-22', '2016-10-20', 'ExpoComic de los 80', 4, 2, 2, 1000, 'http://www.sinembargo.mx/wp-content/uploads/2013/05/ku-medium.jpg'),
(6, '2016-05-12 00:00:00', '2016-10-22', '2016-10-20', 'ExpoComic de los 90', 4, 3, 10, 1000, 'https://i.ytimg.com/vi/NluQVihfAh8/hqdefault.jpg'),
(7, '2016-05-14 00:00:00', '2016-06-07', '2016-06-08', 'Exposición Equina', 4, 3, 10, 1000, 'http://pasofinotv.com/wp-content/uploads/2014/02/onix-afiche.jpg'),
(8, '2016-05-15 00:00:00', '2016-05-23', '2016-05-27', 'Tomorrowland', 7, 4, 11, 200, 'http://youredm.youredm1.netdna-cdn.com/wp-content/uploads/2015/07/new-tomorrowland-pic-yay.jpg'),
(9, '2016-05-15 00:00:00', '2016-09-23', '2016-09-27', 'Good Food', 6, 4, 12, 400, 'http://www.chicagonow.com/count-gregulas-crypt/files/2013/03/GFF_Banner.jpg'),
(10, '2016-05-15 00:00:00', '2016-06-24', '2016-06-24', 'Alicante Summer Festival', 1, 4, 13, 200, 'http://images1.browardpalmbeach.com/imager/u/745xauto/6931728/tortuga_003.jpg'),
(11, '2016-05-15 00:00:00', '2016-08-24', '2016-08-24', 'Jack Daniel''s', 4, 4, 14, 200, 'https://vivegourmet.files.wordpress.com/2012/02/1jack_daniels.jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contrato_has_patrocinador`
--

CREATE TABLE IF NOT EXISTS `contrato_has_patrocinador` (
  `Contrato_idContrato` int(11) NOT NULL,
  `Patrocinador_idPatrocinador` int(11) NOT NULL,
  PRIMARY KEY (`Contrato_idContrato`,`Patrocinador_idPatrocinador`),
  KEY `fk_Contrato_has_Patrocinador_Patrocinador1_idx` (`Patrocinador_idPatrocinador`),
  KEY `fk_Contrato_has_Patrocinador_Contrato1_idx` (`Contrato_idContrato`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `contrato_has_patrocinador`
--

INSERT INTO `contrato_has_patrocinador` (`Contrato_idContrato`, `Patrocinador_idPatrocinador`) VALUES
(1, 2),
(1, 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `emplazamiento`
--

CREATE TABLE IF NOT EXISTS `emplazamiento` (
  `idEmplazamiento` int(11) NOT NULL,
  `Nombre` varchar(40) NOT NULL,
  `Direccion` varchar(100) NOT NULL,
  `descripcion` varchar(45) DEFAULT NULL,
  `aforo` int(11) DEFAULT NULL,
  PRIMARY KEY (`idEmplazamiento`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `emplazamiento`
--

INSERT INTO `emplazamiento` (`idEmplazamiento`, `Nombre`, `Direccion`, `descripcion`, `aforo`) VALUES
(1, 'Local del casal de la juventud', 'c/ Mayor Nº 4 (Petrer)', NULL, 200),
(2, 'Local mediano 1', 'C/ Magallanes Nº 145 (Alicante)', NULL, 2000),
(3, 'Local Mediano 2', 'C/Pio XII Nº 8 (Elche)', NULL, 2500),
(4, 'Local de convenciones', 'Avda. Juan Carlos I (Alicante)', NULL, 10000),
(5, 'Local grande 1', 'Poligono atalayas (Alicante)', NULL, 40000),
(6, 'IFA', 'IFA (Alicante)', NULL, 10000),
(7, 'Edificio Museo', 'C/ La Palma Nº9 (Elda)', NULL, 400),
(8, 'Edificio Convenciones pequeño', 'C/Alfonso X (Elche)', NULL, 500),
(9, 'Estadio Pepico Amat', 'Elda (Alicante)', NULL, 5000),
(10, 'Complejo VistaHermosa', 'Avda. Denia (Alicante)', NULL, 1500),
(11, 'Belgic Stadium', 'Bruselas, Belgica', NULL, 10000),
(12, 'Hungry Site', 'Gran Via Nº3, Alicante', NULL, 2000),
(13, 'Puerto Alicante', 'Puerto Deportivo de Alicante', NULL, 1000),
(14, 'Jack Daniel''s House Siete', 'Gran Via 12, Madrid', NULL, 300);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empleado`
--

CREATE TABLE IF NOT EXISTS `empleado` (
  `idEmpleado` int(11) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `apellido1` varchar(45) NOT NULL,
  `apellido2` varchar(45) DEFAULT NULL,
  `fechaAlta` datetime DEFAULT NULL,
  `Voluntario` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`idEmpleado`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `empleado`
--

INSERT INTO `empleado` (`idEmpleado`, `nombre`, `apellido1`, `apellido2`, `fechaAlta`, `Voluntario`) VALUES
(1, 'Manuel ', 'Santana', NULL, '2016-04-16 10:57:29', 1),
(2, 'Juan ', 'Hernandez', NULL, '2016-04-16 11:01:05', 0),
(3, 'Ana ', 'Calatrava', NULL, '2016-04-16 11:01:05', 0),
(4, 'Jose ', 'Canseco', NULL, '2016-04-16 11:01:05', 0),
(5, 'Rubeola ', 'Aguasderio', NULL, '2016-04-16 11:01:05', 1),
(6, 'Pedro ', 'De Entrerios', NULL, '2016-04-16 11:01:05', 0),
(7, 'Jose Juan ', 'Martinez', NULL, '2016-04-16 11:01:05', 1),
(8, 'Manuela ', 'Castañeda', NULL, '2016-04-16 11:01:05', 0),
(9, 'Eloisa ', 'Santamaría', NULL, '2016-04-16 11:01:05', 0),
(10, 'Jacinta ', 'Pascual', 'Montesdeoca', '2016-04-16 11:01:05', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empleado_has_evento`
--

CREATE TABLE IF NOT EXISTS `ievents`.`empleado_has_evento` (
  `Empleado_idEmpleado` int(11) NOT NULL,
  `Evento_idEvento` int(11) NOT NULL,
  `Evento_Contrato_idContrato` int(11) NOT NULL,
  PRIMARY KEY (`Empleado_idEmpleado`,`Evento_idEvento`,`Evento_Contrato_idContrato`),
  KEY `fk_Empleado_has_Evento_Evento1_idx` (`Evento_idEvento`,`Evento_Contrato_idContrato`),
  KEY `fk_Empleado_has_Evento_Empleado1_idx` (`Empleado_idEmpleado`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `empleado_has_evento`
--

INSERT INTO `empleado_has_evento` (`Empleado_idEmpleado`, `Evento_idEvento`, `Evento_Contrato_idContrato`) VALUES
(1, 3, 5),
(3, 1, 1),
(4, 1, 1),
(4, 3, 5),
(5, 1, 1),
(6, 1, 1),
(10, 3, 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estadocontrato`
--

CREATE TABLE IF NOT EXISTS `ievents`.`estadocontrato` (
  `idEstadoContrato` int(11) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `descripcion` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idEstadoContrato`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `estadocontrato`
--

INSERT INTO `estadocontrato` (`idEstadoContrato`, `nombre`, `descripcion`) VALUES
(1, 'Propuesta', 'Propuesta recibida por el cliente'),
(2, 'Pactando', 'Esperando comunicaciones del cliente'),
(3, 'Aceptado', 'Contrato aceptado'),
(4, 'No aceptado', 'Contrato no aceptado'),
(5, 'No realizado', 'Contrato rechazado');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estadoevento`
--

CREATE TABLE IF NOT EXISTS `ievents`.`estadoevento` (
  `idEstadoEvento` int(11) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `descripcion` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idEstadoEvento`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `estadoevento`
--

INSERT INTO `estadoevento` (`idEstadoEvento`, `nombre`, `descripcion`) VALUES
(1, 'Creado', 'Evento a partir de un contrato'),
(2, 'Abierto', 'Evento abirto al publico'),
(3, 'Cerrado', 'Evento finalizado');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `evento`
--

CREATE TABLE IF NOT EXISTS `ievents`.`evento` (
  `idEvento` int(11) NOT NULL,
  `precioEntrada` double NULL,
  `aforo` int(11) NOT NULL,
  `Contrato_idContrato` int(11) NOT NULL,
  `EstadoEvento_idEstadoEvento` int(11) NOT NULL,
  `fechaInicio` date NOT NULL,
  `fechaCierre` date NOT NULL,
  PRIMARY KEY (`idEvento`,`Contrato_idContrato`),
  KEY `fk_Evento_Contrato1_idx` (`Contrato_idContrato`),
  KEY `fk_Evento_EstadoEvento1_idx` (`EstadoEvento_idEstadoEvento`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `evento`
--

INSERT INTO `evento` (`idEvento`, `precioEntrada`, `aforo`, `Contrato_idContrato`, `EstadoEvento_idEstadoEvento`, `fechaInicio`, `fechaCierre`) VALUES
(1, 20, 294, 1, 3, '2016-05-18', '2016-05-20'),
(2, 11, 1000, 1, 3, '2016-10-20', '2016-10-22'),
(3, 15, 999, 5, 2, '2016-10-22', '2016-10-20'),
(4, 5, 1000, 6, 3, '2016-10-22', '2016-10-20'),
(5, 5, 998, 7, 2, '2016-06-07', '2016-06-08'),
(6, 99, 200, 8, 2, '2016-05-23', '2016-05-27'),
(7, 39, 400, 9, 2, '2016-09-23', '2016-09-27'),
(8, 29, 200, 10, 2, '2016-06-24', '2016-06-24'),
(9, 29, 200, 11, 2, '2016-08-24', '2016-08-24');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `evento_has_patrocinador`
--

CREATE TABLE IF NOT EXISTS `ievents`.`evento_has_patrocinador` (
  `Evento_idEvento` int(11) NOT NULL,
  `Evento_Contrato_idContrato` int(11) NOT NULL,
  `Patrocinador_idPatrocinador` int(11) NOT NULL,
  PRIMARY KEY (`Evento_idEvento`,`Evento_Contrato_idContrato`,`Patrocinador_idPatrocinador`),
  KEY `fk_Evento_has_Patrocinador_Patrocinador1_idx` (`Patrocinador_idPatrocinador`),
  KEY `fk_Evento_has_Patrocinador_Evento1_idx` (`Evento_idEvento`,`Evento_Contrato_idContrato`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `evento_has_patrocinador`
--

INSERT INTO `evento_has_patrocinador` (`Evento_idEvento`, `Evento_Contrato_idContrato`, `Patrocinador_idPatrocinador`) VALUES
(1, 1, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `patrocinador`
--

CREATE TABLE IF NOT EXISTS `ievents`.`patrocinador` (
  `idPatrocinador` int(11) NOT NULL,
  `nombre` varchar(45) DEFAULT NULL,
  `Contacto` varchar(100) DEFAULT NULL,
  `email` varchar(40) NOT NULL,
  PRIMARY KEY (`idPatrocinador`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `patrocinador`
--

INSERT INTO `patrocinador` (`idPatrocinador`, `nombre`, `Contacto`, `email`) VALUES
(1, 'Ateneo Comics', 'Manuel Alberto Sanchez Hernandez', ''),
(2, 'IFA', 'Jose Juan Martinez Estrada', ''),
(3, 'Nike', 'Efren Sullivan', ''),
(4, 'Concesionario Pablo Motor', 'Ana Garcia', ''),
(5, 'Esperanza Consultores', 'Estela Esperanza Ubilla Motos', ''),
(6, 'Seat SL', 'Jose Joaquin Laura Pozo', ''),
(7, 'Muebles Pina', 'manoli Pina Pina', ''),
(8, 'Coches Antiguos Hnos Martinez ', 'Manuel MArtinez Soriano', 'michelpadronmorales@gmail.com'),
(9, 'Chocolates Valor', 'Marta Galiana Ferrandiz', ''),
(10, 'Panadería Boix', 'Carmen', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `servicio`
--

CREATE TABLE IF NOT EXISTS `ievents`.`servicio` (
  `idServicio` int(11) NOT NULL,
  `descripcion` varchar(45) DEFAULT NULL,
  `Nombre` varchar(30) NOT NULL,
  PRIMARY KEY (`idServicio`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `servicio`
--

INSERT INTO `servicio` (`idServicio`, `descripcion`, `Nombre`) VALUES
(1, 'Actuación del último disco', 'Shakira'),
(2, 'Firma de discos', 'Shakira '),
(3, 'Flashdance', 'Grupo danza MNLi S.L.'),
(4, 'Puesto de comida japonesa', 'Sushi Moki´s'),
(5, 'Comida para llevar de todo tipo', 'FastFood 4 All '),
(6, 'Exposición de coches antiguos', 'Coches Antiguos Manuel'),
(7, 'Animación con personajes', 'Cosplay Events'),
(8, 'Azafatas para tus convenciones', 'Azafatas Motor'),
(9, 'Oculus Rift en tu salón', 'Asociación ACME'),
(10, 'Talleres de todo tipo', 'Asociación PJ');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `servicio_has_contrato`
--

CREATE TABLE IF NOT EXISTS `ievents`.`servicio_has_contrato` (
  `Servicio_idServicio` int(11) NOT NULL,
  `Contrato_idContrato` int(11) NOT NULL,
  PRIMARY KEY (`Servicio_idServicio`,`Contrato_idContrato`),
  KEY `fk_Servicio_has_Contrato_Contrato1_idx` (`Contrato_idContrato`),
  KEY `fk_Servicio_has_Contrato_Servicio1_idx` (`Servicio_idServicio`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `servicio_has_contrato`
--

INSERT INTO `servicio_has_contrato` (`Servicio_idServicio`, `Contrato_idContrato`) VALUES
(1, 1),
(1, 5),
(2, 1),
(4, 4),
(7, 5),
(8, 5),
(9, 6),
(10, 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ventaentrada`
--

CREATE TABLE IF NOT EXISTS `ievents`.`ventaentrada` (
  `idVentaEntrada` int(11) NOT NULL,
  `precio` double NOT NULL,
  `emailParticipante` varchar(100) NOT NULL,
  `Evento_idEvento` int(11) NOT NULL,
  `Evento_Contrato_idContrato` int(11) NOT NULL,
  PRIMARY KEY (`idVentaEntrada`,`Evento_idEvento`,`Evento_Contrato_idContrato`),
  KEY `fk_VentaEntrada_Evento1_idx` (`Evento_idEvento`,`Evento_Contrato_idContrato`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `ventaentrada`
--

INSERT INTO `ventaentrada` (`idVentaEntrada`, `precio`, `emailParticipante`, `Evento_idEvento`, `Evento_Contrato_idContrato`) VALUES
(1, 20, 'crisjg10@gmail.com', 1, 1),
(2, 20, 'crisjg10@gmail.com', 1, 1),
(3, 5, 'crisjg10@gmail.com', 4, 6),
(4, 5, 'crisjg10@gmail.com', 4, 6),
(5, 5, 'crisjg10@gmail.com', 4, 6),
(6, 5, 'crisjg10@gmail.com', 4, 6),
(7, 5, 'thaniasc88@gmail.com', 4, 6),
(8, 20, 'thaniasc88@gmail.com', 1, 1),
(9, 20, 'thaniasc88@gmail.com', 1, 1),
(10, 20, 'thaniasc88@gmail.com', 1, 1),
(11, 15, 'thaniasc88@gmail.com', 3, 5);

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `contrato`
--
ALTER TABLE `contrato`
  ADD CONSTRAINT `fk_Contrato_Cliente` FOREIGN KEY (`Cliente_idCliente`) REFERENCES `cliente` (`idCliente`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Contrato_Emplazamiento1` FOREIGN KEY (`Emplazamiento_idEmplazamiento`) REFERENCES `emplazamiento` (`idEmplazamiento`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Contrato_EstadoContrato1` FOREIGN KEY (`EstadoContrato_idEstadoContrato`) REFERENCES `estadocontrato` (`idEstadoContrato`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `contrato_has_patrocinador`
--
ALTER TABLE `contrato_has_patrocinador`
  ADD CONSTRAINT `fk_Contrato_has_Patrocinador_Contrato1` FOREIGN KEY (`Contrato_idContrato`) REFERENCES `contrato` (`idContrato`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Contrato_has_Patrocinador_Patrocinador1` FOREIGN KEY (`Patrocinador_idPatrocinador`) REFERENCES `patrocinador` (`idPatrocinador`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `empleado_has_evento`
--
ALTER TABLE `empleado_has_evento`
  ADD CONSTRAINT `fk_Empleado_has_Evento_Empleado1` FOREIGN KEY (`Empleado_idEmpleado`) REFERENCES `empleado` (`idEmpleado`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Empleado_has_Evento_Evento1` FOREIGN KEY (`Evento_idEvento`, `Evento_Contrato_idContrato`) REFERENCES `evento` (`idEvento`, `Contrato_idContrato`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `evento`
--
ALTER TABLE `evento`
  ADD CONSTRAINT `fk_Evento_Contrato1` FOREIGN KEY (`Contrato_idContrato`) REFERENCES `contrato` (`idContrato`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Evento_EstadoEvento1` FOREIGN KEY (`EstadoEvento_idEstadoEvento`) REFERENCES `estadoevento` (`idEstadoEvento`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `evento_has_patrocinador`
--
ALTER TABLE `evento_has_patrocinador`
  ADD CONSTRAINT `fk_Evento_has_Patrocinador_Evento1` FOREIGN KEY (`Evento_idEvento`, `Evento_Contrato_idContrato`) REFERENCES `evento` (`idEvento`, `Contrato_idContrato`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Evento_has_Patrocinador_Patrocinador1` FOREIGN KEY (`Patrocinador_idPatrocinador`) REFERENCES `patrocinador` (`idPatrocinador`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `servicio_has_contrato`
--
ALTER TABLE `servicio_has_contrato`
  ADD CONSTRAINT `fk_Servicio_has_Contrato_Contrato1` FOREIGN KEY (`Contrato_idContrato`) REFERENCES `contrato` (`idContrato`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Servicio_has_Contrato_Servicio1` FOREIGN KEY (`Servicio_idServicio`) REFERENCES `servicio` (`idServicio`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `ventaentrada`
--
ALTER TABLE `ventaentrada`
  ADD CONSTRAINT `fk_VentaEntrada_Evento1` FOREIGN KEY (`Evento_idEvento`, `Evento_Contrato_idContrato`) REFERENCES `evento` (`idEvento`, `Contrato_idContrato`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
